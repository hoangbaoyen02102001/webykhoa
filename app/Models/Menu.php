<?php 

require_once('app/Models/Model.php');

class Menu extends Model
{
    protected $table = 'menus';

    protected $fillable = ['id', 'title','description', 'created_at'];

    public function all()
    {
        $sql = "SELECT * FROM menus";
        return $this->getAll($sql);
    }

}