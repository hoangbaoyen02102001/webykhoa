<?php 

require_once('app/Models/Model.php');

class Blog extends Model
{
    protected $table = 'blogs';

    protected $fillable = ['id', 'title','thumbnail', 'category_id', 'description', 'content', 'active' , 'created_at', 'updated_at'];

    public function first($id)
    {
        $sql = "SELECT * FROM blogs WHERE id = '$id'";
        return $this->getFirst($sql);
    }

    public function all()
    {
        $sql = "SELECT blogs.*, categories.title as 'category_title', categories.id as 'category_id' FROM blogs inner join categories on categories.id = blogs.category_id";
        if (isset($_GET['keyword'])) {
            $sql .= " where categories.title like '%{$_GET['keyword']}%'";
        }
        return $this->getAll($sql);
    }

    public function updateStatus($data)
    {
        $updated_at = date('Y-m-d H:i:s');
        $sql = "UPDATE blogs SET active='{$data['active']}' ,`updated_at`='$updated_at' WHERE `id`='{$data['id']}'";
        return $this->dbConnection->query($sql);
    }

}