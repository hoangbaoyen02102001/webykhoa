<?php 

require_once('app/Models/Model.php');

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['id', 'title','description', 'active','menu_id','created_at', 'updated_at'];

    
    public function all()
    {
        $sql = "SELECT categories.*, menus.title as 'menu_title', menus.id as 'menu_id' FROM categories inner join menus on menus.id = categories.menu_id";
        if (isset($_GET['keyword'])) {
            $sql .= " where categories.title like '%{$_GET['keyword']}%'";
        }
        return $this->getAll($sql);
    }

    public function updateStatus($data)
    {
        $updated_at = date('Y-m-d H:i:s');
        $sql = "UPDATE categories SET active='{$data['active']}' ,`updated_at`='$updated_at' WHERE `id`='{$data['id']}'";
        return $this->dbConnection->query($sql);
    }

}