<?php

require_once('app/Requests/BaseRequest.php');
require_once('app/Models/User.php');
class AuthUserRequest extends BaseRequest
{
    public function validateRegister($data)
    {
        if (empty($data['name'])) {
            $this->errors['name'] = "Họ và tên không được để trống";
        }

        if (empty($data['email'])) {
            $this->errors['email'] = "Email không được để trống";
        }

        $user = new User();
        if ($user->checkEmail($data)) {
            $this->errors['email'] = "Email đã được đăng ký, vui lòng nhập email khác";
        }


        if (empty($data['password'])) {
            $this->errors['password'] = "Password không được để trống";
        }

        if (empty($data['password_confirmation'])) {
            $this->errors['password_confirmation'] = "Password nhập lại không được để trống";
        }

        if (!empty($data['password']) && !empty($data['password_confirmation']) && $data['password'] != $data['password_confirmation']) {
            $this->errors['password_confirmation'] = "Password nhập lại không khớp";
        }

        return $this->errors;
    }

    public function validateLogin($data)
    {
        if (empty($data['email'])) {
            $this->errors['email'] = "Email không được để trống";
        }

        if (empty($data['password'])) {
            $this->errors['password'] = "Password không được để trống";
        }

        return $this->errors;
    }
}
