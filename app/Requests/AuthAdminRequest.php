<?php

require_once('app/Requests/BaseRequest.php');

class AuthAdminRequest extends BaseRequest
{
    public function validateRegister($data)
    {
        if (empty($data['email'])) {
            $this->errors['email'] = "Email không được để trống";
        }

        if (empty($data['password'])) {
            $this->errors['password'] = "Password không được để trống";
        }

        if (empty($data['password_confirmation'])) {
            $this->errors['password_confirmation'] = "Password nhập lại không được để trống";
        }

        return $this->errors;
    }

    public function validateLogin($data)
    {
        if (empty($data['email'])) {
            $this->errors['email'] = "Email không được để trống";
        }

        if (empty($data['password'])) {
            $this->errors['password'] = "Password không được để trống";
        }

        return $this->errors;
    }
}
