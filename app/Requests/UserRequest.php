<?php 
require_once('app/Requests/BaseRequest.php');
require_once('app/Models/User.php');

class UserRequest extends BaseRequest 
{
    public function validateCreate($data) {
        if ($data['password'] != $data['password_confirmation']) {
            $this->errors['password_confirmation'] = 'Nhập lại mật khẩu không chính xác';
        }
        $user_model = new User();
        $emailExists = $user_model->checkEmail($data);
        if ($emailExists) {
            $this->errors['email'] = 'Email đã được sử dụng';
        }
        return $this->errors;
    }

    public function validateUpdate($data) {
        if ($data['password'] != $data['password_confirmation']) {
            $this->errors['password_confirmation'] = 'Nhập lại mật khẩu không chính xác';
        }
        return $this->errors;
    }
}
