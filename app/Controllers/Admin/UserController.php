<?php

require_once('app/Controllers/Admin/BackendController.php');
require_once('app/Middlewares/AdminMiddleware.php');
require_once('app/Models/User.php');
require_once('app/Requests/UserRequest.php');
require_once('core/Flash.php');
require_once('core/Auth.php');

class UserController extends BackendController
{
    private $middleware;
    public function __construct()
    {
        $this->middleware = new AdminMiddleware();
        $this->middleware->handle();
    }

    public function index()
    {
        $user_model = new User();
        $users = $user_model->all();
        return $this->view('user/index.php', ['users' => $users]);
    }

    public function create()
    {
        return $this->view('user/create.php');
    }

    public function update()
    {
        if (isset($_GET['id'])) {
            $user_model = new User();
            $user = $user_model->find($_GET['id']);
            if ($user) {
                return $this->view("user/update.php", ['user' => $user]);
            }
        }
        redirect('admin/user');
    }

    public function handleCreate()
    {
        $request = new UserRequest();
        $errors = $request->validateCreate($_POST);
        if (empty($errors)) {
            $user_model = new User();
            $data = $_POST;
            $data['password'] = md5($data['password']);
            if ($user_model->create($data)) {
                redirect('admin/user');
            }
        }
        return $this->view('user/create.php', ['errors' => $errors]);
    }

    public function handleUpdate()
    {
        if (isset($_GET['id'])) {
            $request = new UserRequest();
            $errors = $request->validateUpdate($_POST);
            $user_model = new User();
            $user = $user_model->find($_GET['id']);
            if (empty($errors)) {
                $name = $_POST['name'];
                $password = md5($_POST['password']);
                if ($user_model->update(['name' => $name,'password' => $password], $_GET['id'])) {
                    redirect('admin/user');
                }
            } 
            return $this->view('user/update.php', ['user'=>$user, 'errors' => $errors]);
        }
        redirect('admin/user');
    }

    public function delete()
    {
        if (isset($_GET['id'])) {
            if (Auth::getUser('admin')['id'] == $_GET['id']) {
                redirect('admin/user');
            } else {
                $user_model = new User();
                if ($user_model->delete($_GET['id'])) {
                    redirect('admin/user');
                }
            }
        };
    }
}
