<?php

require_once('app/Controllers/Admin/BackendController.php');
require_once('app/Middlewares/AdminMiddleware.php');
require_once('app/Models/Category.php');
require_once('app/Models/Menu.php');
require_once('app/Requests/CategoryRequest.php');
require_once('core/Flash.php');
require_once('core/Auth.php');

class CategoryController extends BackendController
{
    private $middleware;
    public function __construct()
    {
        $this->middleware = new AdminMiddleware();
        $this->middleware->handle();
    }

    public function index()
    {
        $category_model = new Category();
        $menu_model = new Menu();
        $categories = $category_model->all();
        $menus = $menu_model->all();
        return $this->view('category/index.php', ['categories' => $categories, 'menus' => $menus]);
    }

    public function handleCreate()
    {
        $request = new CategoryRequest();
        $errors = $request->validateCreate($_POST);
        if (empty($errors)) {
            $active = isset($_POST['active']) ? 1 : 0;
            $category_model = new Category();
            $data = array_merge($_POST, ['active' => $active]);
            if ($category_model->create($data)) {
                return redirect('admin/category');
            }
        }
        $errors['old_data'] = $_POST;
        Flash::set('createCategoryErrors', $errors);
        return redirect('admin/category');
    }

    public function handleUpdate()
    {
        $request = new CategoryRequest();
        $errors = $request->validateUpdate($_POST);
        if (empty($errors)) {
            $active = isset($_POST['active']) ? 1 : 0;
            $category_model = new Category();
            $data = array_merge($_POST, ['active' => $active]);
            if ($category_model->update($data, $data['id'])) {
                return redirect('admin/category');
            }
        }
        $errors['old_data'] = $_POST;
        Flash::set('createCategoryErrors', $errors);
        return redirect('admin/category');
    }

    public function status()
    {
        $request = new CategoryRequest();
        $errors = $request->validateStatus($_GET);
        if (empty($errors)) {
            $category_model = new Category();
            $category_model->updateStatus($_GET);
        }
        return redirect('admin/category');
    }

    public function delete()
    {
        if (isset($_GET['id'])) {
            $category_model = new Category();
            $category_model->delete($_GET['id']);
        }
        return redirect('admin/category');
    }
}
