<?php 

require('app/Controllers/Admin/BackendController.php');
require_once('app/Requests/AuthAdminRequest.php');
require_once('app/Models/User.php');
require_once('core/Flash.php');
require_once('core/Auth.php');
class AuthController extends BackendController
{
    public function login() 
    {   
        if (Auth::loggedIn('admin')) {
            return redirect('admin/user');
        }
        return $this->view('user/login.php');
    }

    public function handleLogin() 
    {
        $request = new AuthAdminRequest();
        $errors = $request->validateLogin($_POST);
        if (empty($errors)) {
            $user = new User();
            $foundUser = $user->first($_POST);
            $remember = isset($_POST['remember']) ? true : false;
            if ($foundUser) {
                Auth::setUser('admin', $foundUser, $remember);
                return redirect('admin/user');
            }
            Flash::set('error', 'Đăng nhập thất bại');
            return redirect('admin/auth/login');
        }
        Flash::set('errors', $errors);
        return back();
    }
    public function logout()
    {
        Auth::logout('admin');
        return redirect('admin/auth/login');
    }

}