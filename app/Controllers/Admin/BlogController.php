<?php

require_once('app/Controllers/Admin/BackendController.php');
require_once('app/Middlewares/AdminMiddleware.php');
require_once('app/Models/Blog.php');
require_once('app/Models/Category.php');
require_once('app/Requests/BlogRequest.php');
require_once('core/Flash.php');
require_once('core/Storage.php');
require_once('core/Auth.php');

class BlogController extends BackendController
{
    private $middleware;
    public function __construct()
    {
        $this->middleware = new AdminMiddleware();
        $this->middleware->handle();
    }

    public function index()
    {
        $blog_model = new Blog();
        $blogs = $blog_model->all();
        return $this->view('blog/index.php', ['blogs' => $blogs]);
    }

    public function create()
    {
        $category_model = new Category();
        $categories = $category_model->all();
        return $this->view('blog/create.php', ['categories' => $categories]);
    }

    public function update()
    {   
        if (isset($_GET['id'])) {
            $category_model = new Category();
            $categories = $category_model->all();
            $blog_model = new Blog();
            $blog = $blog_model->first($_GET['id']);
            return $this->view('blog/update.php', ['blog' => $blog, 'categories' => $categories]);
        } else {
            return redirect('admin/blog');
        }
    }

    public function handleCreate()
    {
        $request = new BlogRequest();
        $errors = $request->validateCreate($_POST);
        if (empty($errors)) {
            $file = $_FILES['thumbnail'];
            if ($file['error'] == 0) {
                $image_name = $file['name'];
                Storage::upload('blogs', $file);
                $active = isset($_POST['active']) ? 1 : 0;
                $blog_model = new Blog();
                $data = array_merge($_POST, ['active' => $active, 'thumbnail' => $image_name]);
                if ($blog_model->create($data)) {
                    return redirect('admin/blog');
                }
            } else {
                $errors['old_data'] = $_POST;
                $errors['thumbnail'] = 'Ảnh không được để trống hoặc bị sai định dạng';
                Flash::set('createBlogErrors', $errors);
                return back();
            }
        }
        $errors['old_data'] = $_POST;
        Flash::set('createBlogErrors', $errors);
        return back();
    }

    public function handleUpdate()
    {
        $request = new BlogRequest();
        $errors = $request->validateUpdate($_POST);
        if (empty($errors)) {
            $id = $_POST['id'];
            $file = $_FILES['thumbnail'];
            $blog_model = new Blog();
            $blog = $blog_model->first($id);
            if ($file['error'] == 0) {
                Storage::upload('blogs', $file);
                unlink('public/storage/blogs/'.$blog['thumbnail']);
                $image_name = $file['name'];
            } else {
                $image_name = $blog['thumbnail'];
            }
            $active = isset($_POST['active']) ? 1 : 0;
        
            $data = array_merge($_POST, ['active' => $active, 'thumbnail' => $image_name, 'updated_at' => date('Y-m-d H:i:s') ]);
            if ($blog_model->update($data, $id)) {
                return redirect('admin/blog');
            }
        }
        $errors['old_data'] = $_POST;
        Flash::set('updateBlogErrors', $errors);
        return back();
    }

    public function status()
    {
        $request = new BlogRequest();
        $errors = $request->validateStatus($_GET);
        if (empty($errors)) {
            $blog_model = new Blog();
            $blog_model->updateStatus($_GET);
        }
        return redirect('admin/blog');
    }

    public function delete()
    {
        if (isset($_GET['id'])) {
            $blog_model = new Blog();
            $blog_model->delete($_GET['id']);
        }
        return redirect('admin/blog');
    }
}
