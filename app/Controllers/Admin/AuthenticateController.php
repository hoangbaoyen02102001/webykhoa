<?php 

require('app/Controllers/Admin/BackendController.php');
require_once('app/Requests/AuthAdminRequest.php');
require_once('app/Models/User.php');
require_once('core/Flash.php');
require_once('core/Auth.php');
class AuthenticateController extends BackendController
{
    public function login() 
    {   
        if (Auth::loggedIn('admin')) {
            return redirect('admin/user');
        }
        return $this->view('pages/user/login.php');
    }

    public function handleLogin() 
    {
        $request = new AuthAdminRequest();
        $errors = $request->validateLogin($_POST);
        if (empty($errors)) {
            $user = new User();
            $foundUser = $user->first($_POST);
            $remember = isset($_POST['remember']) ? true : false;
            if ($foundUser) {
                Auth::setUser('admin', $foundUser, $remember);
                return redirect('admin/user');
            }
            Flash::set('error', 'Đăng nhập thất bại');
            return redirect('admin/authenticate/login');
        }
        return $this->view('pages/user/login.php', ['errors' => $errors]);
    }
    public function logout()
    {
        Auth::logout('admin');
        return redirect('admin/authenticate/login');
    }

}