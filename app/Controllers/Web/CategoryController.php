<?php

require_once('app/Controllers/Web/WebController.php');
// require_once('app/Models/Category.php');

class CategoryController extends WebController
{

    public function index()
    {
        return $this->view('category/index.php');
    }

    public function create()
    {
        return $this->view('category/create.php');
    }

    // public function store()
    // {
    //     $category = new Category();
    //     $category->create($_POST);
    // }

    // public function delete()
    // {
    //     $category = new Category();
    //     $category->delete($_GET['id']);
    //     return redirect('homepage/index');
    // }
}
