<?php
require_once('app/Controllers/Web/WebController.php');
require_once('app/Models/User.php');
require_once('app/Requests/AuthUserRequest.php');
require_once('core/Flash.php');
require_once('core/Auth.php');

class AuthController extends WebController
{

    public function handleRegister()
    {
        $authRequest = new AuthUserRequest();
        $errors = $authRequest->validateRegister($_POST);
        if (count($errors) == 0) {
            $user = new User();
            $_POST['password'] = md5($_POST['password']);
            $createdUser = $user->create(array_merge($_POST, ['role' => 1]));
            if ($createdUser) {
                Auth::setUser('user', $createdUser);
                return redirect('category');
            }
        }
        $errors['old_data'] = $_POST;
        Flash::set('registerErrors', $errors);
        return redirect('category');
    }

    public function handleLogin()
    {
        $authRequest = new AuthUserRequest();
        $errors = $authRequest->validateLogin($_POST);
        if (empty($errors)) {
            $user = new User();
            $foundUser = $user->first($_POST);
            $remember = isset($_POST['remember']) ? true : false;
            if ($foundUser) {
                Auth::setUser('user', $foundUser, $remember);
                return redirect('category');
            } else {
                $errors['another'] = 'Đăng nhập thất bại';
            }
            Flash::set('loginErrors', $errors);
            return redirect('category');
        }
        Flash::set('loginErrors', $errors);
        return redirect('category');
    }

    public function logout()
    {
        Auth::logout('user');
        return redirect('category');
    }
}
