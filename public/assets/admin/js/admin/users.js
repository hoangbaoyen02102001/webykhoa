$((function() {
    $("#role").select2({
        minimumResultsForSearch: 1 / 0,
        width: "100%"
    }), $("#status").select2({
        minimumResultsForSearch: 1 / 0,
        width: "100%"
    }), $("#birthday").datepicker({
        format: "yyyy-mm-dd"
    })
}));