! function(o) {
    "use strict";
    o(window).on("load", (function() {
        o("#preloader-active").delay(450).fadeOut("slow"), o("body").delay(450).css({
            overflow: "visible"
        })
    })), o(document).ready((function() {
        var s, e, i, t;
        o('[data-toggle="tooltip"]').tooltip(), o("#off-canvas-toggle").on("click", (function() {
            o("body").toggleClass("canvas-opened")
        })), o(".dark-mark").on("click", (function() {
            o("body").removeClass("canvas-opened")
        })), o(".off-canvas-close").on("click", (function() {
            o("body").removeClass("canvas-opened")
        })), o(".sub-mega-menu .nav-pills > a").on("mouseover", (function(s) {
            o(this).tab("show")
        })), o.scrollUp({
            scrollName: "scrollUp",
            topDistance: "300",
            topSpeed: 300,
            animation: "fade",
            animationInSpeed: 200,
            animationOutSpeed: 200,
            scrollText: '<i class="ti-arrow-up"></i>',
            activeOverlay: !1
        }), o(window).on("scroll", (function() {
            o(window).scrollTop() < 0 ? o(".header-sticky ").removeClass("sticky-bar") : o(".header-sticky").addClass("sticky-bar")
        })), o(".sticky-sidebar").theiaStickySidebar(), o(".post-carausel-1-items").slick({
            dots: !1,
            infinite: !0,
            speed: 1e3,
            arrows: !0,
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: !0,
            loop: !0,
            adaptiveHeight: !0,
            cssEase: "linear",
            prevArrow: '<button type="button" class="slick-prev"><i class="flaticon-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="flaticon-right"></i></button>',
            appendArrows: ".post-carausel-1-arrow",
            centerPadding: 50,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6,
                    infinite: !0,
                    dots: !1
                }
            }, {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }]
        }), o(".post-carausel-2").slick({
            dots: !0,
            infinite: !0,
            speed: 1e3,
            arrows: !1,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: !1,
            loop: !0,
            adaptiveHeight: !0,
            cssEase: "linear",
            centerPadding: 50,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: !0,
                    dots: !1
                }
            }, {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        }), o(".post-carausel-3").slick({
            dots: !0,
            infinite: !0,
            speed: 1e3,
            arrows: !1,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: !0,
            loop: !0,
            adaptiveHeight: !0,
            cssEase: "linear",
            centerPadding: 50,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: !0,
                    dots: !1
                }
            }, {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        }), o(".featured-slider-2-items").slick({
            fade: !0,
            asNavFor: ".featured-slider-2-nav",
            arrows: !0,
            prevArrow: '<button type="button" class="slick-prev"><i class="flaticon-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="flaticon-right"></i></button>',
            appendArrows: ".arrow-cover"
        }), o(".featured-slider-2-nav").slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: ".featured-slider-2-items",
            dots: !1,
            arrows: !1,
            centerMode: !0,
            focusOnSelect: !0,
            centerPadding: 0,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }]
        }), (s = o("ul#navigation")).length && s.slicknav({
            label: "",
            prependTo: ".mobile_menu",
            closedSymbol: "&rsaquo;",
            openedSymbol: "-"
        }), i = o(document).height(), t = o(window).height(), o(window).on("scroll", (function() {
            e = o(window).scrollTop() / (i - t) * 100, o(".scroll-progress").width(e + "%")
        })), o(".search-icon").on("click", (function() {
            o(this).closest("form").submit()
        })), o(".search-icon-mobile").on("click", (function() {
            o(".search-form").addClass("search-mobile"), o(".slicknav_btn").hide()
        })), o(".search-close-icon").on("click", (function() {
            o(".search-form").removeClass("search-mobile"), o(".slicknav_btn").show()
        })), o("#ajax-login").click((function(s) {
            var e = route("ajax-login");
            o.ajax({
                url: e,
                method: "POST",
                data: {
                    email: o("#email").val(),
                    password: o("#password").val()
                },
                success: function(s) {
                    "success" === s.status ? location.reload() : o(".js-login-error").show()
                },
                error: function(s) {
                    if (422 === s.status) {
                        var e = s.responseJSON.errors;
                        e.email && o("#email-error").html(e.email[0]), e.password && o("#password-error").html(e.password[0])
                    }
                    console.log("Error ", s)
                }
            })
        })), o("#email").change((function(s) {
            o("#email-error").html("")
        })), o("#password").change((function(s) {
            o("#password-error").html("")
        }))
    }))
}(jQuery);