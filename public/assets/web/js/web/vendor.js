! function(t, e) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : t.Popper = e()
}(this, (function() {
    "use strict";

    function t(t) {
        return t && "[object Function]" === {}.toString.call(t)
    }

    function e(t, e) {
        if (1 !== t.nodeType) return [];
        var i = getComputedStyle(t, null);
        return e ? i[e] : i
    }

    function i(t) {
        return "HTML" === t.nodeName ? t : t.parentNode || t.host
    }

    function n(t) {
        if (!t) return document.body;
        switch (t.nodeName) {
            case "HTML":
            case "BODY":
                return t.ownerDocument.body;
            case "#document":
                return t.body
        }
        var o = e(t),
            s = o.overflow,
            r = o.overflowX,
            a = o.overflowY;
        return /(auto|scroll)/.test(s + a + r) ? t : n(i(t))
    }

    function o(t) {
        var i = t && t.offsetParent,
            n = i && i.nodeName;
        return n && "BODY" !== n && "HTML" !== n ? -1 !== ["TD", "TABLE"].indexOf(i.nodeName) && "static" === e(i, "position") ? o(i) : i : t ? t.ownerDocument.documentElement : document.documentElement
    }

    function s(t) {
        return null === t.parentNode ? t : s(t.parentNode)
    }

    function r(t, e) {
        if (!(t && t.nodeType && e && e.nodeType)) return document.documentElement;
        var i = t.compareDocumentPosition(e) & Node.DOCUMENT_POSITION_FOLLOWING,
            n = i ? t : e,
            a = i ? e : t,
            l = document.createRange();
        l.setStart(n, 0), l.setEnd(a, 0);
        var d = l.commonAncestorContainer;
        if (t !== d && e !== d || n.contains(a)) return function(t) {
            var e = t.nodeName;
            return "BODY" !== e && ("HTML" === e || o(t.firstElementChild) === t)
        }(d) ? d : o(d);
        var c = s(t);
        return c.host ? r(c.host, e) : r(t, s(e).host)
    }

    function a(t) {
        var e = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "top",
            i = "top" === e ? "scrollTop" : "scrollLeft",
            n = t.nodeName;
        if ("BODY" === n || "HTML" === n) {
            var o = t.ownerDocument.documentElement,
                s = t.ownerDocument.scrollingElement || o;
            return s[i]
        }
        return t[i]
    }

    function l(t, e) {
        var i = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
            n = a(e, "top"),
            o = a(e, "left"),
            s = i ? -1 : 1;
        return t.top += n * s, t.bottom += n * s, t.left += o * s, t.right += o * s, t
    }

    function d(t, e) {
        var i = "x" === e ? "Left" : "Top",
            n = "Left" == i ? "Right" : "Bottom";
        return parseFloat(t["border" + i + "Width"], 10) + parseFloat(t["border" + n + "Width"], 10)
    }

    function c(t, e, i, n) {
        return R(e["offset" + t], e["scroll" + t], i["client" + t], i["offset" + t], i["scroll" + t], V() ? i["offset" + t] + n["margin" + ("Height" === t ? "Top" : "Left")] + n["margin" + ("Height" === t ? "Bottom" : "Right")] : 0)
    }

    function p() {
        var t = document.body,
            e = document.documentElement,
            i = V() && getComputedStyle(e);
        return {
            height: c("Height", t, e, i),
            width: c("Width", t, e, i)
        }
    }

    function u(t) {
        return J({}, t, {
            right: t.left + t.width,
            bottom: t.top + t.height
        })
    }

    function h(t) {
        var i = {};
        if (V()) try {
            i = t.getBoundingClientRect();
            var n = a(t, "top"),
                o = a(t, "left");
            i.top += n, i.left += o, i.bottom += n, i.right += o
        } catch (t) {} else i = t.getBoundingClientRect();
        var s = {
                left: i.left,
                top: i.top,
                width: i.right - i.left,
                height: i.bottom - i.top
            },
            r = "HTML" === t.nodeName ? p() : {},
            l = r.width || t.clientWidth || s.right - s.left,
            c = r.height || t.clientHeight || s.bottom - s.top,
            h = t.offsetWidth - l,
            f = t.offsetHeight - c;
        if (h || f) {
            var g = e(t);
            h -= d(g, "x"), f -= d(g, "y"), s.width -= h, s.height -= f
        }
        return u(s)
    }

    function f(t, i) {
        var o = V(),
            s = "HTML" === i.nodeName,
            r = h(t),
            a = h(i),
            d = n(t),
            c = e(i),
            p = parseFloat(c.borderTopWidth, 10),
            f = parseFloat(c.borderLeftWidth, 10),
            g = u({
                top: r.top - a.top - p,
                left: r.left - a.left - f,
                width: r.width,
                height: r.height
            });
        if (g.marginTop = 0, g.marginLeft = 0, !o && s) {
            var m = parseFloat(c.marginTop, 10),
                v = parseFloat(c.marginLeft, 10);
            g.top -= p - m, g.bottom -= p - m, g.left -= f - v, g.right -= f - v, g.marginTop = m, g.marginLeft = v
        }
        return (o ? i.contains(d) : i === d && "BODY" !== d.nodeName) && (g = l(g, i)), g
    }

    function g(t) {
        var e = t.ownerDocument.documentElement,
            i = f(t, e),
            n = R(e.clientWidth, window.innerWidth || 0),
            o = R(e.clientHeight, window.innerHeight || 0),
            s = a(e),
            r = a(e, "left");
        return u({
            top: s - i.top + i.marginTop,
            left: r - i.left + i.marginLeft,
            width: n,
            height: o
        })
    }

    function m(t) {
        var n = t.nodeName;
        return "BODY" !== n && "HTML" !== n && ("fixed" === e(t, "position") || m(i(t)))
    }

    function v(t, e, o, s) {
        var a = {
                top: 0,
                left: 0
            },
            l = r(t, e);
        if ("viewport" === s) a = g(l);
        else {
            var d;
            "scrollParent" === s ? "BODY" === (d = n(i(e))).nodeName && (d = t.ownerDocument.documentElement) : d = "window" === s ? t.ownerDocument.documentElement : s;
            var c = f(d, l);
            if ("HTML" !== d.nodeName || m(l)) a = c;
            else {
                var u = p(),
                    h = u.height,
                    v = u.width;
                a.top += c.top - c.marginTop, a.bottom = h + c.top, a.left += c.left - c.marginLeft, a.right = v + c.left
            }
        }
        return a.left += o, a.top += o, a.right -= o, a.bottom -= o, a
    }

    function y(t) {
        return t.width * t.height
    }

    function _(t, e, i, n, o) {
        var s = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
        if (-1 === t.indexOf("auto")) return t;
        var r = v(i, n, s, o),
            a = {
                top: {
                    width: r.width,
                    height: e.top - r.top
                },
                right: {
                    width: r.right - e.right,
                    height: r.height
                },
                bottom: {
                    width: r.width,
                    height: r.bottom - e.bottom
                },
                left: {
                    width: e.left - r.left,
                    height: r.height
                }
            },
            l = Object.keys(a).map((function(t) {
                return J({
                    key: t
                }, a[t], {
                    area: y(a[t])
                })
            })).sort((function(t, e) {
                return e.area - t.area
            })),
            d = l.filter((function(t) {
                var e = t.width,
                    n = t.height;
                return e >= i.clientWidth && n >= i.clientHeight
            })),
            c = 0 < d.length ? d[0].key : l[0].key,
            p = t.split("-")[1];
        return c + (p ? "-" + p : "")
    }

    function b(t, e, i) {
        return f(i, r(e, i))
    }

    function w(t) {
        var e = getComputedStyle(t),
            i = parseFloat(e.marginTop) + parseFloat(e.marginBottom),
            n = parseFloat(e.marginLeft) + parseFloat(e.marginRight);
        return {
            width: t.offsetWidth + n,
            height: t.offsetHeight + i
        }
    }

    function k(t) {
        var e = {
            left: "right",
            right: "left",
            bottom: "top",
            top: "bottom"
        };
        return t.replace(/left|right|bottom|top/g, (function(t) {
            return e[t]
        }))
    }

    function T(t, e, i) {
        i = i.split("-")[0];
        var n = w(t),
            o = {
                width: n.width,
                height: n.height
            },
            s = -1 !== ["right", "left"].indexOf(i),
            r = s ? "top" : "left",
            a = s ? "left" : "top",
            l = s ? "height" : "width",
            d = s ? "width" : "height";
        return o[r] = e[r] + e[l] / 2 - n[l] / 2, o[a] = i === a ? e[a] - n[d] : e[k(a)], o
    }

    function S(t, e) {
        return Array.prototype.find ? t.find(e) : t.filter(e)[0]
    }

    function C(e, i, n) {
        var o = void 0 === n ? e : e.slice(0, function(t, e, i) {
            if (Array.prototype.findIndex) return t.findIndex((function(t) {
                return t[e] === i
            }));
            var n = S(t, (function(t) {
                return t[e] === i
            }));
            return t.indexOf(n)
        }(e, "name", n));
        return o.forEach((function(e) {
            e.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");
            var n = e.function || e.fn;
            e.enabled && t(n) && (i.offsets.popper = u(i.offsets.popper), i.offsets.reference = u(i.offsets.reference), i = n(i, e))
        })), i
    }

    function E() {
        if (!this.state.isDestroyed) {
            var t = {
                instance: this,
                styles: {},
                arrowStyles: {},
                attributes: {},
                flipped: !1,
                offsets: {}
            };
            t.offsets.reference = b(this.state, this.popper, this.reference), t.placement = _(this.options.placement, t.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), t.originalPlacement = t.placement, t.offsets.popper = T(this.popper, t.offsets.reference, t.placement), t.offsets.popper.position = "absolute", t = C(this.modifiers, t), this.state.isCreated ? this.options.onUpdate(t) : (this.state.isCreated = !0, this.options.onCreate(t))
        }
    }

    function A(t, e) {
        return t.some((function(t) {
            var i = t.name;
            return t.enabled && i === e
        }))
    }

    function x(t) {
        for (var e = [!1, "ms", "Webkit", "Moz", "O"], i = t.charAt(0).toUpperCase() + t.slice(1), n = 0; n < e.length - 1; n++) {
            var o = e[n],
                s = o ? "" + o + i : t;
            if (void 0 !== document.body.style[s]) return s
        }
        return null
    }

    function I() {
        return this.state.isDestroyed = !0, A(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), this.popper.style.left = "", this.popper.style.position = "", this.popper.style.top = "", this.popper.style[x("transform")] = ""), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this
    }

    function D(t) {
        var e = t.ownerDocument;
        return e ? e.defaultView : window
    }

    function O(t, e, i, o) {
        var s = "BODY" === t.nodeName,
            r = s ? t.ownerDocument.defaultView : t;
        r.addEventListener(e, i, {
            passive: !0
        }), s || O(n(r.parentNode), e, i, o), o.push(r)
    }

    function $(t, e, i, o) {
        i.updateBound = o, D(t).addEventListener("resize", i.updateBound, {
            passive: !0
        });
        var s = n(t);
        return O(s, "scroll", i.updateBound, i.scrollParents), i.scrollElement = s, i.eventsEnabled = !0, i
    }

    function N() {
        this.state.eventsEnabled || (this.state = $(this.reference, this.options, this.state, this.scheduleUpdate))
    }

    function P() {
        this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = function(t, e) {
            return D(t).removeEventListener("resize", e.updateBound), e.scrollParents.forEach((function(t) {
                t.removeEventListener("scroll", e.updateBound)
            })), e.updateBound = null, e.scrollParents = [], e.scrollElement = null, e.eventsEnabled = !1, e
        }(this.reference, this.state))
    }

    function H(t) {
        return "" !== t && !isNaN(parseFloat(t)) && isFinite(t)
    }

    function L(t, e) {
        Object.keys(e).forEach((function(i) {
            var n = ""; - 1 !== ["width", "height", "top", "right", "bottom", "left"].indexOf(i) && H(e[i]) && (n = "px"), t.style[i] = e[i] + n
        }))
    }

    function j(t, e, i) {
        var n = S(t, (function(t) {
                return t.name === e
            })),
            o = !!n && t.some((function(t) {
                return t.name === i && t.enabled && t.order < n.order
            }));
        if (!o) {
            var s = "`" + e + "`";
            console.warn("`" + i + "` modifier is required by " + s + " modifier in order to work, be sure to include it before " + s + "!")
        }
        return o
    }

    function M(t) {
        var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
            i = et.indexOf(t),
            n = et.slice(i + 1).concat(et.slice(0, i));
        return e ? n.reverse() : n
    }

    function W(t, e, i, n) {
        var o = [0, 0],
            s = -1 !== ["right", "left"].indexOf(n),
            r = t.split(/(\+|\-)/).map((function(t) {
                return t.trim()
            })),
            a = r.indexOf(S(r, (function(t) {
                return -1 !== t.search(/,|\s/)
            })));
        r[a] && -1 === r[a].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");
        var l = /\s*,\s*|\s+/,
            d = -1 === a ? [r] : [r.slice(0, a).concat([r[a].split(l)[0]]), [r[a].split(l)[1]].concat(r.slice(a + 1))];
        return d = d.map((function(t, n) {
            var o = (1 === n ? !s : s) ? "height" : "width",
                r = !1;
            return t.reduce((function(t, e) {
                return "" === t[t.length - 1] && -1 !== ["+", "-"].indexOf(e) ? (t[t.length - 1] = e, r = !0, t) : r ? (t[t.length - 1] += e, r = !1, t) : t.concat(e)
            }), []).map((function(t) {
                return function(t, e, i, n) {
                    var o = t.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
                        s = +o[1],
                        r = o[2];
                    if (!s) return t;
                    if (0 === r.indexOf("%")) {
                        return u("%p" === r ? i : n)[e] / 100 * s
                    }
                    return "vh" === r || "vw" === r ? ("vh" === r ? R(document.documentElement.clientHeight, window.innerHeight || 0) : R(document.documentElement.clientWidth, window.innerWidth || 0)) / 100 * s : s
                }(t, o, e, i)
            }))
        })), d.forEach((function(t, e) {
            t.forEach((function(i, n) {
                H(i) && (o[e] += i * ("-" === t[n - 1] ? -1 : 1))
            }))
        })), o
    }
    for (var z = Math.min, U = Math.floor, R = Math.max, B = "undefined" != typeof window && "undefined" != typeof document, F = ["Edge", "Trident", "Firefox"], q = 0, Q = 0; Q < F.length; Q += 1)
        if (B && 0 <= navigator.userAgent.indexOf(F[Q])) {
            q = 1;
            break
        }
    var K, Y = B && window.Promise ? function(t) {
            var e = !1;
            return function() {
                e || (e = !0, window.Promise.resolve().then((function() {
                    e = !1, t()
                })))
            }
        } : function(t) {
            var e = !1;
            return function() {
                e || (e = !0, setTimeout((function() {
                    e = !1, t()
                }), q))
            }
        },
        V = function() {
            return null == K && (K = -1 !== navigator.appVersion.indexOf("MSIE 10")), K
        },
        X = function(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        },
        G = function() {
            function t(t, e) {
                for (var i, n = 0; n < e.length; n++)(i = e[n]).enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
            }
            return function(e, i, n) {
                return i && t(e.prototype, i), n && t(e, n), e
            }
        }(),
        Z = function(t, e, i) {
            return e in t ? Object.defineProperty(t, e, {
                value: i,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : t[e] = i, t
        },
        J = Object.assign || function(t) {
            for (var e, i = 1; i < arguments.length; i++)
                for (var n in e = arguments[i]) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
            return t
        },
        tt = ["auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start"],
        et = tt.slice(3),
        it = "flip",
        nt = "clockwise",
        ot = "counterclockwise",
        st = function() {
            function e(i, n) {
                var o = this,
                    s = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
                X(this, e), this.scheduleUpdate = function() {
                    return requestAnimationFrame(o.update)
                }, this.update = Y(this.update.bind(this)), this.options = J({}, e.Defaults, s), this.state = {
                    isDestroyed: !1,
                    isCreated: !1,
                    scrollParents: []
                }, this.reference = i && i.jquery ? i[0] : i, this.popper = n && n.jquery ? n[0] : n, this.options.modifiers = {}, Object.keys(J({}, e.Defaults.modifiers, s.modifiers)).forEach((function(t) {
                    o.options.modifiers[t] = J({}, e.Defaults.modifiers[t] || {}, s.modifiers ? s.modifiers[t] : {})
                })), this.modifiers = Object.keys(this.options.modifiers).map((function(t) {
                    return J({
                        name: t
                    }, o.options.modifiers[t])
                })).sort((function(t, e) {
                    return t.order - e.order
                })), this.modifiers.forEach((function(e) {
                    e.enabled && t(e.onLoad) && e.onLoad(o.reference, o.popper, o.options, e, o.state)
                })), this.update();
                var r = this.options.eventsEnabled;
                r && this.enableEventListeners(), this.state.eventsEnabled = r
            }
            return G(e, [{
                key: "update",
                value: function() {
                    return E.call(this)
                }
            }, {
                key: "destroy",
                value: function() {
                    return I.call(this)
                }
            }, {
                key: "enableEventListeners",
                value: function() {
                    return N.call(this)
                }
            }, {
                key: "disableEventListeners",
                value: function() {
                    return P.call(this)
                }
            }]), e
        }();
    return st.Utils = ("undefined" == typeof window ? global : window).PopperUtils, st.placements = tt, st.Defaults = {
        placement: "bottom",
        eventsEnabled: !0,
        removeOnDestroy: !1,
        onCreate: function() {},
        onUpdate: function() {},
        modifiers: {
            shift: {
                order: 100,
                enabled: !0,
                fn: function(t) {
                    var e = t.placement,
                        i = e.split("-")[0],
                        n = e.split("-")[1];
                    if (n) {
                        var o = t.offsets,
                            s = o.reference,
                            r = o.popper,
                            a = -1 !== ["bottom", "top"].indexOf(i),
                            l = a ? "left" : "top",
                            d = a ? "width" : "height",
                            c = {
                                start: Z({}, l, s[l]),
                                end: Z({}, l, s[l] + s[d] - r[d])
                            };
                        t.offsets.popper = J({}, r, c[n])
                    }
                    return t
                }
            },
            offset: {
                order: 200,
                enabled: !0,
                fn: function(t, e) {
                    var i, n = e.offset,
                        o = t.placement,
                        s = t.offsets,
                        r = s.popper,
                        a = s.reference,
                        l = o.split("-")[0];
                    return i = H(+n) ? [+n, 0] : W(n, r, a, l), "left" === l ? (r.top += i[0], r.left -= i[1]) : "right" === l ? (r.top += i[0], r.left += i[1]) : "top" === l ? (r.left += i[0], r.top -= i[1]) : "bottom" === l && (r.left += i[0], r.top += i[1]), t.popper = r, t
                },
                offset: 0
            },
            preventOverflow: {
                order: 300,
                enabled: !0,
                fn: function(t, e) {
                    var i = e.boundariesElement || o(t.instance.popper);
                    t.instance.reference === i && (i = o(i));
                    var n = v(t.instance.popper, t.instance.reference, e.padding, i);
                    e.boundaries = n;
                    var s = e.priority,
                        r = t.offsets.popper,
                        a = {
                            primary: function(t) {
                                var i = r[t];
                                return r[t] < n[t] && !e.escapeWithReference && (i = R(r[t], n[t])), Z({}, t, i)
                            },
                            secondary: function(t) {
                                var i = "right" === t ? "left" : "top",
                                    o = r[i];
                                return r[t] > n[t] && !e.escapeWithReference && (o = z(r[i], n[t] - ("right" === t ? r.width : r.height))), Z({}, i, o)
                            }
                        };
                    return s.forEach((function(t) {
                        var e = -1 === ["left", "top"].indexOf(t) ? "secondary" : "primary";
                        r = J({}, r, a[e](t))
                    })), t.offsets.popper = r, t
                },
                priority: ["left", "right", "top", "bottom"],
                padding: 5,
                boundariesElement: "scrollParent"
            },
            keepTogether: {
                order: 400,
                enabled: !0,
                fn: function(t) {
                    var e = t.offsets,
                        i = e.popper,
                        n = e.reference,
                        o = t.placement.split("-")[0],
                        s = U,
                        r = -1 !== ["top", "bottom"].indexOf(o),
                        a = r ? "right" : "bottom",
                        l = r ? "left" : "top",
                        d = r ? "width" : "height";
                    return i[a] < s(n[l]) && (t.offsets.popper[l] = s(n[l]) - i[d]), i[l] > s(n[a]) && (t.offsets.popper[l] = s(n[a])), t
                }
            },
            arrow: {
                order: 500,
                enabled: !0,
                fn: function(t, i) {
                    var n;
                    if (!j(t.instance.modifiers, "arrow", "keepTogether")) return t;
                    var o = i.element;
                    if ("string" == typeof o) {
                        if (!(o = t.instance.popper.querySelector(o))) return t
                    } else if (!t.instance.popper.contains(o)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), t;
                    var s = t.placement.split("-")[0],
                        r = t.offsets,
                        a = r.popper,
                        l = r.reference,
                        d = -1 !== ["left", "right"].indexOf(s),
                        c = d ? "height" : "width",
                        p = d ? "Top" : "Left",
                        h = p.toLowerCase(),
                        f = d ? "left" : "top",
                        g = d ? "bottom" : "right",
                        m = w(o)[c];
                    l[g] - m < a[h] && (t.offsets.popper[h] -= a[h] - (l[g] - m)), l[h] + m > a[g] && (t.offsets.popper[h] += l[h] + m - a[g]), t.offsets.popper = u(t.offsets.popper);
                    var v = l[h] + l[c] / 2 - m / 2,
                        y = e(t.instance.popper),
                        _ = parseFloat(y["margin" + p], 10),
                        b = parseFloat(y["border" + p + "Width"], 10),
                        k = v - t.offsets.popper[h] - _ - b;
                    return k = R(z(a[c] - m, k), 0), t.arrowElement = o, t.offsets.arrow = (Z(n = {}, h, Math.round(k)), Z(n, f, ""), n), t
                },
                element: "[x-arrow]"
            },
            flip: {
                order: 600,
                enabled: !0,
                fn: function(t, e) {
                    if (A(t.instance.modifiers, "inner")) return t;
                    if (t.flipped && t.placement === t.originalPlacement) return t;
                    var i = v(t.instance.popper, t.instance.reference, e.padding, e.boundariesElement),
                        n = t.placement.split("-")[0],
                        o = k(n),
                        s = t.placement.split("-")[1] || "",
                        r = [];
                    switch (e.behavior) {
                        case it:
                            r = [n, o];
                            break;
                        case nt:
                            r = M(n);
                            break;
                        case ot:
                            r = M(n, !0);
                            break;
                        default:
                            r = e.behavior
                    }
                    return r.forEach((function(a, l) {
                        if (n !== a || r.length === l + 1) return t;
                        n = t.placement.split("-")[0], o = k(n);
                        var d = t.offsets.popper,
                            c = t.offsets.reference,
                            p = U,
                            u = "left" === n && p(d.right) > p(c.left) || "right" === n && p(d.left) < p(c.right) || "top" === n && p(d.bottom) > p(c.top) || "bottom" === n && p(d.top) < p(c.bottom),
                            h = p(d.left) < p(i.left),
                            f = p(d.right) > p(i.right),
                            g = p(d.top) < p(i.top),
                            m = p(d.bottom) > p(i.bottom),
                            v = "left" === n && h || "right" === n && f || "top" === n && g || "bottom" === n && m,
                            y = -1 !== ["top", "bottom"].indexOf(n),
                            _ = !!e.flipVariations && (y && "start" === s && h || y && "end" === s && f || !y && "start" === s && g || !y && "end" === s && m);
                        (u || v || _) && (t.flipped = !0, (u || v) && (n = r[l + 1]), _ && (s = function(t) {
                            return "end" === t ? "start" : "start" === t ? "end" : t
                        }(s)), t.placement = n + (s ? "-" + s : ""), t.offsets.popper = J({}, t.offsets.popper, T(t.instance.popper, t.offsets.reference, t.placement)), t = C(t.instance.modifiers, t, "flip"))
                    })), t
                },
                behavior: "flip",
                padding: 5,
                boundariesElement: "viewport"
            },
            inner: {
                order: 700,
                enabled: !1,
                fn: function(t) {
                    var e = t.placement,
                        i = e.split("-")[0],
                        n = t.offsets,
                        o = n.popper,
                        s = n.reference,
                        r = -1 !== ["left", "right"].indexOf(i),
                        a = -1 === ["top", "left"].indexOf(i);
                    return o[r ? "left" : "top"] = s[i] - (a ? o[r ? "width" : "height"] : 0), t.placement = k(e), t.offsets.popper = u(o), t
                }
            },
            hide: {
                order: 800,
                enabled: !0,
                fn: function(t) {
                    if (!j(t.instance.modifiers, "hide", "preventOverflow")) return t;
                    var e = t.offsets.reference,
                        i = S(t.instance.modifiers, (function(t) {
                            return "preventOverflow" === t.name
                        })).boundaries;
                    if (e.bottom < i.top || e.left > i.right || e.top > i.bottom || e.right < i.left) {
                        if (!0 === t.hide) return t;
                        t.hide = !0, t.attributes["x-out-of-boundaries"] = ""
                    } else {
                        if (!1 === t.hide) return t;
                        t.hide = !1, t.attributes["x-out-of-boundaries"] = !1
                    }
                    return t
                }
            },
            computeStyle: {
                order: 850,
                enabled: !0,
                fn: function(t, e) {
                    var i = e.x,
                        n = e.y,
                        s = t.offsets.popper,
                        r = S(t.instance.modifiers, (function(t) {
                            return "applyStyle" === t.name
                        })).gpuAcceleration;
                    void 0 !== r && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");
                    var a, l, d = void 0 === r ? e.gpuAcceleration : r,
                        c = h(o(t.instance.popper)),
                        p = {
                            position: s.position
                        },
                        u = {
                            left: U(s.left),
                            top: U(s.top),
                            bottom: U(s.bottom),
                            right: U(s.right)
                        },
                        f = "bottom" === i ? "top" : "bottom",
                        g = "right" === n ? "left" : "right",
                        m = x("transform");
                    if (l = "bottom" == f ? -c.height + u.bottom : u.top, a = "right" == g ? -c.width + u.right : u.left, d && m) p[m] = "translate3d(" + a + "px, " + l + "px, 0)", p[f] = 0, p[g] = 0, p.willChange = "transform";
                    else {
                        var v = "bottom" == f ? -1 : 1,
                            y = "right" == g ? -1 : 1;
                        p[f] = l * v, p[g] = a * y, p.willChange = f + ", " + g
                    }
                    var _ = {
                        "x-placement": t.placement
                    };
                    return t.attributes = J({}, _, t.attributes), t.styles = J({}, p, t.styles), t.arrowStyles = J({}, t.offsets.arrow, t.arrowStyles), t
                },
                gpuAcceleration: !0,
                x: "bottom",
                y: "right"
            },
            applyStyle: {
                order: 900,
                enabled: !0,
                fn: function(t) {
                    return L(t.instance.popper, t.styles),
                        function(t, e) {
                            Object.keys(e).forEach((function(i) {
                                !1 === e[i] ? t.removeAttribute(i) : t.setAttribute(i, e[i])
                            }))
                        }(t.instance.popper, t.attributes), t.arrowElement && Object.keys(t.arrowStyles).length && L(t.arrowElement, t.arrowStyles), t
                },
                onLoad: function(t, e, i, n, o) {
                    var s = b(0, e, t),
                        r = _(i.placement, s, e, t, i.modifiers.flip.boundariesElement, i.modifiers.flip.padding);
                    return e.setAttribute("x-placement", r), L(e, {
                        position: "absolute"
                    }), i
                },
                gpuAcceleration: void 0
            }
        }
    }, st
})),
function(t, e) {
    "object" == typeof exports && "undefined" != typeof module ? e(exports, require("jquery"), require("popper.js")) : "function" == typeof define && define.amd ? define(["exports", "jquery", "popper.js"], e) : e(t.bootstrap = {}, t.jQuery, t.Popper)
}(this, (function(t, e, i) {
    "use strict";

    function n(t, e) {
        for (var i = 0; i < e.length; i++) {
            var n = e[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n)
        }
    }

    function o(t, e, i) {
        return e && n(t.prototype, e), i && n(t, i), t
    }

    function s() {
        return (s = Object.assign || function(t) {
            for (var e = 1; e < arguments.length; e++) {
                var i = arguments[e];
                for (var n in i) Object.prototype.hasOwnProperty.call(i, n) && (t[n] = i[n])
            }
            return t
        }).apply(this, arguments)
    }
    e = e && e.hasOwnProperty("default") ? e.default : e, i = i && i.hasOwnProperty("default") ? i.default : i;
    var r, a, l, d, c, p, u, h, f, g, m, v, y, _, b, w, k, T, S = function(t) {
            var e = !1;
            var i = {
                TRANSITION_END: "bsTransitionEnd",
                getUID: function(t) {
                    do {
                        t += ~~(1e6 * Math.random())
                    } while (document.getElementById(t));
                    return t
                },
                getSelectorFromElement: function(e) {
                    var i, n = e.getAttribute("data-target");
                    n && "#" !== n || (n = e.getAttribute("href") || ""), "#" === n.charAt(0) && (i = n, n = i = "function" == typeof t.escapeSelector ? t.escapeSelector(i).substr(1) : i.replace(/(:|\.|\[|\]|,|=|@)/g, "\\$1"));
                    try {
                        return t(document).find(n).length > 0 ? n : null
                    } catch (t) {
                        return null
                    }
                },
                reflow: function(t) {
                    return t.offsetHeight
                },
                triggerTransitionEnd: function(i) {
                    t(i).trigger(e.end)
                },
                supportsTransitionEnd: function() {
                    return Boolean(e)
                },
                isElement: function(t) {
                    return (t[0] || t).nodeType
                },
                typeCheckConfig: function(t, e, n) {
                    for (var o in n)
                        if (Object.prototype.hasOwnProperty.call(n, o)) {
                            var s = n[o],
                                r = e[o],
                                a = r && i.isElement(r) ? "element" : (l = r, {}.toString.call(l).match(/\s([a-zA-Z]+)/)[1].toLowerCase());
                            if (!new RegExp(s).test(a)) throw new Error(t.toUpperCase() + ': Option "' + o + '" provided type "' + a + '" but expected type "' + s + '".')
                        }
                    var l
                }
            };
            return e = ("undefined" == typeof window || !window.QUnit) && {
                end: "transitionend"
            }, t.fn.emulateTransitionEnd = function(e) {
                var n = this,
                    o = !1;
                return t(this).one(i.TRANSITION_END, (function() {
                    o = !0
                })), setTimeout((function() {
                    o || i.triggerTransitionEnd(n)
                }), e), this
            }, i.supportsTransitionEnd() && (t.event.special[i.TRANSITION_END] = {
                bindType: e.end,
                delegateType: e.end,
                handle: function(e) {
                    if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
                }
            }), i
        }(e),
        C = (a = "alert", d = "." + (l = "bs.alert"), c = (r = e).fn[a], p = {
            CLOSE: "close" + d,
            CLOSED: "closed" + d,
            CLICK_DATA_API: "click" + d + ".data-api"
        }, "alert", "fade", "show", u = function() {
            function t(t) {
                this._element = t
            }
            var e = t.prototype;
            return e.close = function(t) {
                t = t || this._element;
                var e = this._getRootElement(t);
                this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e)
            }, e.dispose = function() {
                r.removeData(this._element, l), this._element = null
            }, e._getRootElement = function(t) {
                var e = S.getSelectorFromElement(t),
                    i = !1;
                return e && (i = r(e)[0]), i || (i = r(t).closest(".alert")[0]), i
            }, e._triggerCloseEvent = function(t) {
                var e = r.Event(p.CLOSE);
                return r(t).trigger(e), e
            }, e._removeElement = function(t) {
                var e = this;
                r(t).removeClass("show"), S.supportsTransitionEnd() && r(t).hasClass("fade") ? r(t).one(S.TRANSITION_END, (function(i) {
                    return e._destroyElement(t, i)
                })).emulateTransitionEnd(150) : this._destroyElement(t)
            }, e._destroyElement = function(t) {
                r(t).detach().trigger(p.CLOSED).remove()
            }, t._jQueryInterface = function(e) {
                return this.each((function() {
                    var i = r(this),
                        n = i.data(l);
                    n || (n = new t(this), i.data(l, n)), "close" === e && n[e](this)
                }))
            }, t._handleDismiss = function(t) {
                return function(e) {
                    e && e.preventDefault(), t.close(this)
                }
            }, o(t, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0"
                }
            }]), t
        }(), r(document).on(p.CLICK_DATA_API, '[data-dismiss="alert"]', u._handleDismiss(new u)), r.fn[a] = u._jQueryInterface, r.fn[a].Constructor = u, r.fn[a].noConflict = function() {
            return r.fn[a] = c, u._jQueryInterface
        }, u),
        E = (f = "button", m = "." + (g = "bs.button"), v = ".data-api", y = (h = e).fn[f], _ = "active", "btn", "focus", b = '[data-toggle^="button"]', '[data-toggle="buttons"]', "input", ".active", w = ".btn", k = {
            CLICK_DATA_API: "click" + m + v,
            FOCUS_BLUR_DATA_API: "focus" + m + v + " blur" + m + v
        }, T = function() {
            function t(t) {
                this._element = t
            }
            var e = t.prototype;
            return e.toggle = function() {
                var t = !0,
                    e = !0,
                    i = h(this._element).closest('[data-toggle="buttons"]')[0];
                if (i) {
                    var n = h(this._element).find("input")[0];
                    if (n) {
                        if ("radio" === n.type)
                            if (n.checked && h(this._element).hasClass(_)) t = !1;
                            else {
                                var o = h(i).find(".active")[0];
                                o && h(o).removeClass(_)
                            }
                        if (t) {
                            if (n.hasAttribute("disabled") || i.hasAttribute("disabled") || n.classList.contains("disabled") || i.classList.contains("disabled")) return;
                            n.checked = !h(this._element).hasClass(_), h(n).trigger("change")
                        }
                        n.focus(), e = !1
                    }
                }
                e && this._element.setAttribute("aria-pressed", !h(this._element).hasClass(_)), t && h(this._element).toggleClass(_)
            }, e.dispose = function() {
                h.removeData(this._element, g), this._element = null
            }, t._jQueryInterface = function(e) {
                return this.each((function() {
                    var i = h(this).data(g);
                    i || (i = new t(this), h(this).data(g, i)), "toggle" === e && i[e]()
                }))
            }, o(t, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0"
                }
            }]), t
        }(), h(document).on(k.CLICK_DATA_API, b, (function(t) {
            t.preventDefault();
            var e = t.target;
            h(e).hasClass("btn") || (e = h(e).closest(w)), T._jQueryInterface.call(h(e), "toggle")
        })).on(k.FOCUS_BLUR_DATA_API, b, (function(t) {
            var e = h(t.target).closest(w)[0];
            h(e).toggleClass("focus", /^focus(in)?$/.test(t.type))
        })), h.fn[f] = T._jQueryInterface, h.fn[f].Constructor = T, h.fn[f].noConflict = function() {
            return h.fn[f] = y, T._jQueryInterface
        }, T),
        A = function(t) {
            var e = "carousel",
                i = "bs.carousel",
                n = "." + i,
                r = t.fn[e],
                a = {
                    interval: 5e3,
                    keyboard: !0,
                    slide: !1,
                    pause: "hover",
                    wrap: !0
                },
                l = {
                    interval: "(number|boolean)",
                    keyboard: "boolean",
                    slide: "(boolean|string)",
                    pause: "(string|boolean)",
                    wrap: "boolean"
                },
                d = "next",
                c = "prev",
                p = {
                    SLIDE: "slide" + n,
                    SLID: "slid" + n,
                    KEYDOWN: "keydown" + n,
                    MOUSEENTER: "mouseenter" + n,
                    MOUSELEAVE: "mouseleave" + n,
                    TOUCHEND: "touchend" + n,
                    LOAD_DATA_API: "load" + n + ".data-api",
                    CLICK_DATA_API: "click" + n + ".data-api"
                },
                u = "active",
                h = ".active",
                f = ".active.carousel-item",
                g = ".carousel-item",
                m = ".carousel-item-next, .carousel-item-prev",
                v = ".carousel-indicators",
                y = "[data-slide], [data-slide-to]",
                _ = '[data-ride="carousel"]',
                b = function() {
                    function r(e, i) {
                        this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this._config = this._getConfig(i), this._element = t(e)[0], this._indicatorsElement = t(this._element).find(v)[0], this._addEventListeners()
                    }
                    var y = r.prototype;
                    return y.next = function() {
                        this._isSliding || this._slide(d)
                    }, y.nextWhenVisible = function() {
                        !document.hidden && t(this._element).is(":visible") && "hidden" !== t(this._element).css("visibility") && this.next()
                    }, y.prev = function() {
                        this._isSliding || this._slide(c)
                    }, y.pause = function(e) {
                        e || (this._isPaused = !0), t(this._element).find(m)[0] && S.supportsTransitionEnd() && (S.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null
                    }, y.cycle = function(t) {
                        t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
                    }, y.to = function(e) {
                        var i = this;
                        this._activeElement = t(this._element).find(f)[0];
                        var n = this._getItemIndex(this._activeElement);
                        if (!(e > this._items.length - 1 || e < 0))
                            if (this._isSliding) t(this._element).one(p.SLID, (function() {
                                return i.to(e)
                            }));
                            else {
                                if (n === e) return this.pause(), void this.cycle();
                                var o = e > n ? d : c;
                                this._slide(o, this._items[e])
                            }
                    }, y.dispose = function() {
                        t(this._element).off(n), t.removeData(this._element, i), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null
                    }, y._getConfig = function(t) {
                        return t = s({}, a, t), S.typeCheckConfig(e, t, l), t
                    }, y._addEventListeners = function() {
                        var e = this;
                        this._config.keyboard && t(this._element).on(p.KEYDOWN, (function(t) {
                            return e._keydown(t)
                        })), "hover" === this._config.pause && (t(this._element).on(p.MOUSEENTER, (function(t) {
                            return e.pause(t)
                        })).on(p.MOUSELEAVE, (function(t) {
                            return e.cycle(t)
                        })), "ontouchstart" in document.documentElement && t(this._element).on(p.TOUCHEND, (function() {
                            e.pause(), e.touchTimeout && clearTimeout(e.touchTimeout), e.touchTimeout = setTimeout((function(t) {
                                return e.cycle(t)
                            }), 500 + e._config.interval)
                        })))
                    }, y._keydown = function(t) {
                        if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {
                            case 37:
                                t.preventDefault(), this.prev();
                                break;
                            case 39:
                                t.preventDefault(), this.next()
                        }
                    }, y._getItemIndex = function(e) {
                        return this._items = t.makeArray(t(e).parent().find(g)), this._items.indexOf(e)
                    }, y._getItemByDirection = function(t, e) {
                        var i = t === d,
                            n = t === c,
                            o = this._getItemIndex(e),
                            s = this._items.length - 1;
                        if ((n && 0 === o || i && o === s) && !this._config.wrap) return e;
                        var r = (o + (t === c ? -1 : 1)) % this._items.length;
                        return -1 === r ? this._items[this._items.length - 1] : this._items[r]
                    }, y._triggerSlideEvent = function(e, i) {
                        var n = this._getItemIndex(e),
                            o = this._getItemIndex(t(this._element).find(f)[0]),
                            s = t.Event(p.SLIDE, {
                                relatedTarget: e,
                                direction: i,
                                from: o,
                                to: n
                            });
                        return t(this._element).trigger(s), s
                    }, y._setActiveIndicatorElement = function(e) {
                        if (this._indicatorsElement) {
                            t(this._indicatorsElement).find(h).removeClass(u);
                            var i = this._indicatorsElement.children[this._getItemIndex(e)];
                            i && t(i).addClass(u)
                        }
                    }, y._slide = function(e, i) {
                        var n, o, s, r = this,
                            a = t(this._element).find(f)[0],
                            l = this._getItemIndex(a),
                            c = i || a && this._getItemByDirection(e, a),
                            h = this._getItemIndex(c),
                            g = Boolean(this._interval);
                        if (e === d ? (n = "carousel-item-left", o = "carousel-item-next", s = "left") : (n = "carousel-item-right", o = "carousel-item-prev", s = "right"), c && t(c).hasClass(u)) this._isSliding = !1;
                        else if (!this._triggerSlideEvent(c, s).isDefaultPrevented() && a && c) {
                            this._isSliding = !0, g && this.pause(), this._setActiveIndicatorElement(c);
                            var m = t.Event(p.SLID, {
                                relatedTarget: c,
                                direction: s,
                                from: l,
                                to: h
                            });
                            S.supportsTransitionEnd() && t(this._element).hasClass("slide") ? (t(c).addClass(o), S.reflow(c), t(a).addClass(n), t(c).addClass(n), t(a).one(S.TRANSITION_END, (function() {
                                t(c).removeClass(n + " " + o).addClass(u), t(a).removeClass(u + " " + o + " " + n), r._isSliding = !1, setTimeout((function() {
                                    return t(r._element).trigger(m)
                                }), 0)
                            })).emulateTransitionEnd(600)) : (t(a).removeClass(u), t(c).addClass(u), this._isSliding = !1, t(this._element).trigger(m)), g && this.cycle()
                        }
                    }, r._jQueryInterface = function(e) {
                        return this.each((function() {
                            var n = t(this).data(i),
                                o = s({}, a, t(this).data());
                            "object" == typeof e && (o = s({}, o, e));
                            var l = "string" == typeof e ? e : o.slide;
                            if (n || (n = new r(this, o), t(this).data(i, n)), "number" == typeof e) n.to(e);
                            else if ("string" == typeof l) {
                                if (void 0 === n[l]) throw new TypeError('No method named "' + l + '"');
                                n[l]()
                            } else o.interval && (n.pause(), n.cycle())
                        }))
                    }, r._dataApiClickHandler = function(e) {
                        var n = S.getSelectorFromElement(this);
                        if (n) {
                            var o = t(n)[0];
                            if (o && t(o).hasClass("carousel")) {
                                var a = s({}, t(o).data(), t(this).data()),
                                    l = this.getAttribute("data-slide-to");
                                l && (a.interval = !1), r._jQueryInterface.call(t(o), a), l && t(o).data(i).to(l), e.preventDefault()
                            }
                        }
                    }, o(r, null, [{
                        key: "VERSION",
                        get: function() {
                            return "4.0.0"
                        }
                    }, {
                        key: "Default",
                        get: function() {
                            return a
                        }
                    }]), r
                }();
            return t(document).on(p.CLICK_DATA_API, y, b._dataApiClickHandler), t(window).on(p.LOAD_DATA_API, (function() {
                t(_).each((function() {
                    var e = t(this);
                    b._jQueryInterface.call(e, e.data())
                }))
            })), t.fn[e] = b._jQueryInterface, t.fn[e].Constructor = b, t.fn[e].noConflict = function() {
                return t.fn[e] = r, b._jQueryInterface
            }, b
        }(e),
        x = function(t) {
            var e = "collapse",
                i = "bs.collapse",
                n = "." + i,
                r = t.fn[e],
                a = {
                    toggle: !0,
                    parent: ""
                },
                l = {
                    toggle: "boolean",
                    parent: "(string|element)"
                },
                d = {
                    SHOW: "show" + n,
                    SHOWN: "shown" + n,
                    HIDE: "hide" + n,
                    HIDDEN: "hidden" + n,
                    CLICK_DATA_API: "click" + n + ".data-api"
                },
                c = "show",
                p = "collapse",
                u = "collapsing",
                h = "collapsed",
                f = "width",
                g = ".show, .collapsing",
                m = '[data-toggle="collapse"]',
                v = function() {
                    function n(e, i) {
                        this._isTransitioning = !1, this._element = e, this._config = this._getConfig(i), this._triggerArray = t.makeArray(t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'));
                        for (var n = t(m), o = 0; o < n.length; o++) {
                            var s = n[o],
                                r = S.getSelectorFromElement(s);
                            null !== r && t(r).filter(e).length > 0 && (this._selector = r, this._triggerArray.push(s))
                        }
                        this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle()
                    }
                    var r = n.prototype;
                    return r.toggle = function() {
                        t(this._element).hasClass(c) ? this.hide() : this.show()
                    }, r.show = function() {
                        var e, o, s = this;
                        if (!(this._isTransitioning || t(this._element).hasClass(c) || (this._parent && 0 === (e = t.makeArray(t(this._parent).find(g).filter('[data-parent="' + this._config.parent + '"]'))).length && (e = null), e && (o = t(e).not(this._selector).data(i)) && o._isTransitioning))) {
                            var r = t.Event(d.SHOW);
                            if (t(this._element).trigger(r), !r.isDefaultPrevented()) {
                                e && (n._jQueryInterface.call(t(e).not(this._selector), "hide"), o || t(e).data(i, null));
                                var a = this._getDimension();
                                t(this._element).removeClass(p).addClass(u), this._element.style[a] = 0, this._triggerArray.length > 0 && t(this._triggerArray).removeClass(h).attr("aria-expanded", !0), this.setTransitioning(!0);
                                var l = function() {
                                    t(s._element).removeClass(u).addClass(p).addClass(c), s._element.style[a] = "", s.setTransitioning(!1), t(s._element).trigger(d.SHOWN)
                                };
                                if (S.supportsTransitionEnd()) {
                                    var f = "scroll" + (a[0].toUpperCase() + a.slice(1));
                                    t(this._element).one(S.TRANSITION_END, l).emulateTransitionEnd(600), this._element.style[a] = this._element[f] + "px"
                                } else l()
                            }
                        }
                    }, r.hide = function() {
                        var e = this;
                        if (!this._isTransitioning && t(this._element).hasClass(c)) {
                            var i = t.Event(d.HIDE);
                            if (t(this._element).trigger(i), !i.isDefaultPrevented()) {
                                var n = this._getDimension();
                                if (this._element.style[n] = this._element.getBoundingClientRect()[n] + "px", S.reflow(this._element), t(this._element).addClass(u).removeClass(p).removeClass(c), this._triggerArray.length > 0)
                                    for (var o = 0; o < this._triggerArray.length; o++) {
                                        var s = this._triggerArray[o],
                                            r = S.getSelectorFromElement(s);
                                        null !== r && (t(r).hasClass(c) || t(s).addClass(h).attr("aria-expanded", !1))
                                    }
                                this.setTransitioning(!0);
                                var a = function() {
                                    e.setTransitioning(!1), t(e._element).removeClass(u).addClass(p).trigger(d.HIDDEN)
                                };
                                this._element.style[n] = "", S.supportsTransitionEnd() ? t(this._element).one(S.TRANSITION_END, a).emulateTransitionEnd(600) : a()
                            }
                        }
                    }, r.setTransitioning = function(t) {
                        this._isTransitioning = t
                    }, r.dispose = function() {
                        t.removeData(this._element, i), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null
                    }, r._getConfig = function(t) {
                        return (t = s({}, a, t)).toggle = Boolean(t.toggle), S.typeCheckConfig(e, t, l), t
                    }, r._getDimension = function() {
                        return t(this._element).hasClass(f) ? f : "height"
                    }, r._getParent = function() {
                        var e = this,
                            i = null;
                        S.isElement(this._config.parent) ? (i = this._config.parent, void 0 !== this._config.parent.jquery && (i = this._config.parent[0])) : i = t(this._config.parent)[0];
                        var o = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';
                        return t(i).find(o).each((function(t, i) {
                            e._addAriaAndCollapsedClass(n._getTargetFromElement(i), [i])
                        })), i
                    }, r._addAriaAndCollapsedClass = function(e, i) {
                        if (e) {
                            var n = t(e).hasClass(c);
                            i.length > 0 && t(i).toggleClass(h, !n).attr("aria-expanded", n)
                        }
                    }, n._getTargetFromElement = function(e) {
                        var i = S.getSelectorFromElement(e);
                        return i ? t(i)[0] : null
                    }, n._jQueryInterface = function(e) {
                        return this.each((function() {
                            var o = t(this),
                                r = o.data(i),
                                l = s({}, a, o.data(), "object" == typeof e && e);
                            if (!r && l.toggle && /show|hide/.test(e) && (l.toggle = !1), r || (r = new n(this, l), o.data(i, r)), "string" == typeof e) {
                                if (void 0 === r[e]) throw new TypeError('No method named "' + e + '"');
                                r[e]()
                            }
                        }))
                    }, o(n, null, [{
                        key: "VERSION",
                        get: function() {
                            return "4.0.0"
                        }
                    }, {
                        key: "Default",
                        get: function() {
                            return a
                        }
                    }]), n
                }();
            return t(document).on(d.CLICK_DATA_API, m, (function(e) {
                "A" === e.currentTarget.tagName && e.preventDefault();
                var n = t(this),
                    o = S.getSelectorFromElement(this);
                t(o).each((function() {
                    var e = t(this),
                        o = e.data(i) ? "toggle" : n.data();
                    v._jQueryInterface.call(e, o)
                }))
            })), t.fn[e] = v._jQueryInterface, t.fn[e].Constructor = v, t.fn[e].noConflict = function() {
                return t.fn[e] = r, v._jQueryInterface
            }, v
        }(e),
        I = function(t) {
            var e = "dropdown",
                n = "bs.dropdown",
                r = "." + n,
                a = ".data-api",
                l = t.fn[e],
                d = new RegExp("38|40|27"),
                c = {
                    HIDE: "hide" + r,
                    HIDDEN: "hidden" + r,
                    SHOW: "show" + r,
                    SHOWN: "shown" + r,
                    CLICK: "click" + r,
                    CLICK_DATA_API: "click" + r + a,
                    KEYDOWN_DATA_API: "keydown" + r + a,
                    KEYUP_DATA_API: "keyup" + r + a
                },
                p = "disabled",
                u = "show",
                h = "dropup",
                f = "dropdown-menu-right",
                g = '[data-toggle="dropdown"]',
                m = ".dropdown-menu",
                v = {
                    offset: 0,
                    flip: !0,
                    boundary: "scrollParent"
                },
                y = {
                    offset: "(number|string|function)",
                    flip: "boolean",
                    boundary: "(string|element)"
                },
                _ = function() {
                    function a(t, e) {
                        this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners()
                    }
                    var l = a.prototype;
                    return l.toggle = function() {
                        if (!this._element.disabled && !t(this._element).hasClass(p)) {
                            var e = a._getParentFromElement(this._element),
                                n = t(this._menu).hasClass(u);
                            if (a._clearMenus(), !n) {
                                var o = {
                                        relatedTarget: this._element
                                    },
                                    s = t.Event(c.SHOW, o);
                                if (t(e).trigger(s), !s.isDefaultPrevented()) {
                                    if (!this._inNavbar) {
                                        if (void 0 === i) throw new TypeError("Bootstrap dropdown require Popper.js (https://popper.js.org)");
                                        var r = this._element;
                                        t(e).hasClass(h) && (t(this._menu).hasClass("dropdown-menu-left") || t(this._menu).hasClass(f)) && (r = e), "scrollParent" !== this._config.boundary && t(e).addClass("position-static"), this._popper = new i(r, this._menu, this._getPopperConfig())
                                    }
                                    "ontouchstart" in document.documentElement && 0 === t(e).closest(".navbar-nav").length && t("body").children().on("mouseover", null, t.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), t(this._menu).toggleClass(u), t(e).toggleClass(u).trigger(t.Event(c.SHOWN, o))
                                }
                            }
                        }
                    }, l.dispose = function() {
                        t.removeData(this._element, n), t(this._element).off(r), this._element = null, this._menu = null, null !== this._popper && (this._popper.destroy(), this._popper = null)
                    }, l.update = function() {
                        this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate()
                    }, l._addEventListeners = function() {
                        var e = this;
                        t(this._element).on(c.CLICK, (function(t) {
                            t.preventDefault(), t.stopPropagation(), e.toggle()
                        }))
                    }, l._getConfig = function(i) {
                        return i = s({}, this.constructor.Default, t(this._element).data(), i), S.typeCheckConfig(e, i, this.constructor.DefaultType), i
                    }, l._getMenuElement = function() {
                        if (!this._menu) {
                            var e = a._getParentFromElement(this._element);
                            this._menu = t(e).find(m)[0]
                        }
                        return this._menu
                    }, l._getPlacement = function() {
                        var e = t(this._element).parent(),
                            i = "bottom-start";
                        return e.hasClass(h) ? (i = "top-start", t(this._menu).hasClass(f) && (i = "top-end")) : e.hasClass("dropright") ? i = "right-start" : e.hasClass("dropleft") ? i = "left-start" : t(this._menu).hasClass(f) && (i = "bottom-end"), i
                    }, l._detectNavbar = function() {
                        return t(this._element).closest(".navbar").length > 0
                    }, l._getPopperConfig = function() {
                        var t = this,
                            e = {};
                        return "function" == typeof this._config.offset ? e.fn = function(e) {
                            return e.offsets = s({}, e.offsets, t._config.offset(e.offsets) || {}), e
                        } : e.offset = this._config.offset, {
                            placement: this._getPlacement(),
                            modifiers: {
                                offset: e,
                                flip: {
                                    enabled: this._config.flip
                                },
                                preventOverflow: {
                                    boundariesElement: this._config.boundary
                                }
                            }
                        }
                    }, a._jQueryInterface = function(e) {
                        return this.each((function() {
                            var i = t(this).data(n);
                            if (i || (i = new a(this, "object" == typeof e ? e : null), t(this).data(n, i)), "string" == typeof e) {
                                if (void 0 === i[e]) throw new TypeError('No method named "' + e + '"');
                                i[e]()
                            }
                        }))
                    }, a._clearMenus = function(e) {
                        if (!e || 3 !== e.which && ("keyup" !== e.type || 9 === e.which))
                            for (var i = t.makeArray(t(g)), o = 0; o < i.length; o++) {
                                var s = a._getParentFromElement(i[o]),
                                    r = t(i[o]).data(n),
                                    l = {
                                        relatedTarget: i[o]
                                    };
                                if (r) {
                                    var d = r._menu;
                                    if (t(s).hasClass(u) && !(e && ("click" === e.type && /input|textarea/i.test(e.target.tagName) || "keyup" === e.type && 9 === e.which) && t.contains(s, e.target))) {
                                        var p = t.Event(c.HIDE, l);
                                        t(s).trigger(p), p.isDefaultPrevented() || ("ontouchstart" in document.documentElement && t("body").children().off("mouseover", null, t.noop), i[o].setAttribute("aria-expanded", "false"), t(d).removeClass(u), t(s).removeClass(u).trigger(t.Event(c.HIDDEN, l)))
                                    }
                                }
                            }
                    }, a._getParentFromElement = function(e) {
                        var i, n = S.getSelectorFromElement(e);
                        return n && (i = t(n)[0]), i || e.parentNode
                    }, a._dataApiKeydownHandler = function(e) {
                        if ((/input|textarea/i.test(e.target.tagName) ? !(32 === e.which || 27 !== e.which && (40 !== e.which && 38 !== e.which || t(e.target).closest(m).length)) : d.test(e.which)) && (e.preventDefault(), e.stopPropagation(), !this.disabled && !t(this).hasClass(p))) {
                            var i = a._getParentFromElement(this),
                                n = t(i).hasClass(u);
                            if ((n || 27 === e.which && 32 === e.which) && (!n || 27 !== e.which && 32 !== e.which)) {
                                var o = t(i).find(".dropdown-menu .dropdown-item:not(.disabled)").get();
                                if (0 !== o.length) {
                                    var s = o.indexOf(e.target);
                                    38 === e.which && s > 0 && s--, 40 === e.which && s < o.length - 1 && s++, s < 0 && (s = 0), o[s].focus()
                                }
                            } else {
                                if (27 === e.which) {
                                    var r = t(i).find(g)[0];
                                    t(r).trigger("focus")
                                }
                                t(this).trigger("click")
                            }
                        }
                    }, o(a, null, [{
                        key: "VERSION",
                        get: function() {
                            return "4.0.0"
                        }
                    }, {
                        key: "Default",
                        get: function() {
                            return v
                        }
                    }, {
                        key: "DefaultType",
                        get: function() {
                            return y
                        }
                    }]), a
                }();
            return t(document).on(c.KEYDOWN_DATA_API, g, _._dataApiKeydownHandler).on(c.KEYDOWN_DATA_API, m, _._dataApiKeydownHandler).on(c.CLICK_DATA_API + " " + c.KEYUP_DATA_API, _._clearMenus).on(c.CLICK_DATA_API, g, (function(e) {
                e.preventDefault(), e.stopPropagation(), _._jQueryInterface.call(t(this), "toggle")
            })).on(c.CLICK_DATA_API, ".dropdown form", (function(t) {
                t.stopPropagation()
            })), t.fn[e] = _._jQueryInterface, t.fn[e].Constructor = _, t.fn[e].noConflict = function() {
                return t.fn[e] = l, _._jQueryInterface
            }, _
        }(e),
        D = function(t) {
            var e = "bs.modal",
                i = "." + e,
                n = t.fn.modal,
                r = {
                    backdrop: !0,
                    keyboard: !0,
                    focus: !0,
                    show: !0
                },
                a = {
                    backdrop: "(boolean|string)",
                    keyboard: "boolean",
                    focus: "boolean",
                    show: "boolean"
                },
                l = {
                    HIDE: "hide" + i,
                    HIDDEN: "hidden" + i,
                    SHOW: "show" + i,
                    SHOWN: "shown" + i,
                    FOCUSIN: "focusin" + i,
                    RESIZE: "resize" + i,
                    CLICK_DISMISS: "click.dismiss" + i,
                    KEYDOWN_DISMISS: "keydown.dismiss" + i,
                    MOUSEUP_DISMISS: "mouseup.dismiss" + i,
                    MOUSEDOWN_DISMISS: "mousedown.dismiss" + i,
                    CLICK_DATA_API: "click" + i + ".data-api"
                },
                d = "modal-open",
                c = "fade",
                p = "show",
                u = ".modal-dialog",
                h = '[data-toggle="modal"]',
                f = '[data-dismiss="modal"]',
                g = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
                m = ".sticky-top",
                v = ".navbar-toggler",
                y = function() {
                    function n(e, i) {
                        this._config = this._getConfig(i), this._element = e, this._dialog = t(e).find(u)[0], this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._originalBodyPadding = 0, this._scrollbarWidth = 0
                    }
                    var h = n.prototype;
                    return h.toggle = function(t) {
                        return this._isShown ? this.hide() : this.show(t)
                    }, h.show = function(e) {
                        var i = this;
                        if (!this._isTransitioning && !this._isShown) {
                            S.supportsTransitionEnd() && t(this._element).hasClass(c) && (this._isTransitioning = !0);
                            var n = t.Event(l.SHOW, {
                                relatedTarget: e
                            });
                            t(this._element).trigger(n), this._isShown || n.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), t(document.body).addClass(d), this._setEscapeEvent(), this._setResizeEvent(), t(this._element).on(l.CLICK_DISMISS, f, (function(t) {
                                return i.hide(t)
                            })), t(this._dialog).on(l.MOUSEDOWN_DISMISS, (function() {
                                t(i._element).one(l.MOUSEUP_DISMISS, (function(e) {
                                    t(e.target).is(i._element) && (i._ignoreBackdropClick = !0)
                                }))
                            })), this._showBackdrop((function() {
                                return i._showElement(e)
                            })))
                        }
                    }, h.hide = function(e) {
                        var i = this;
                        if (e && e.preventDefault(), !this._isTransitioning && this._isShown) {
                            var n = t.Event(l.HIDE);
                            if (t(this._element).trigger(n), this._isShown && !n.isDefaultPrevented()) {
                                this._isShown = !1;
                                var o = S.supportsTransitionEnd() && t(this._element).hasClass(c);
                                o && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), t(document).off(l.FOCUSIN), t(this._element).removeClass(p), t(this._element).off(l.CLICK_DISMISS), t(this._dialog).off(l.MOUSEDOWN_DISMISS), o ? t(this._element).one(S.TRANSITION_END, (function(t) {
                                    return i._hideModal(t)
                                })).emulateTransitionEnd(300) : this._hideModal()
                            }
                        }
                    }, h.dispose = function() {
                        t.removeData(this._element, e), t(window, document, this._element, this._backdrop).off(i), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._scrollbarWidth = null
                    }, h.handleUpdate = function() {
                        this._adjustDialog()
                    }, h._getConfig = function(t) {
                        return t = s({}, r, t), S.typeCheckConfig("modal", t, a), t
                    }, h._showElement = function(e) {
                        var i = this,
                            n = S.supportsTransitionEnd() && t(this._element).hasClass(c);
                        this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.scrollTop = 0, n && S.reflow(this._element), t(this._element).addClass(p), this._config.focus && this._enforceFocus();
                        var o = t.Event(l.SHOWN, {
                                relatedTarget: e
                            }),
                            s = function() {
                                i._config.focus && i._element.focus(), i._isTransitioning = !1, t(i._element).trigger(o)
                            };
                        n ? t(this._dialog).one(S.TRANSITION_END, s).emulateTransitionEnd(300) : s()
                    }, h._enforceFocus = function() {
                        var e = this;
                        t(document).off(l.FOCUSIN).on(l.FOCUSIN, (function(i) {
                            document !== i.target && e._element !== i.target && 0 === t(e._element).has(i.target).length && e._element.focus()
                        }))
                    }, h._setEscapeEvent = function() {
                        var e = this;
                        this._isShown && this._config.keyboard ? t(this._element).on(l.KEYDOWN_DISMISS, (function(t) {
                            27 === t.which && (t.preventDefault(), e.hide())
                        })) : this._isShown || t(this._element).off(l.KEYDOWN_DISMISS)
                    }, h._setResizeEvent = function() {
                        var e = this;
                        this._isShown ? t(window).on(l.RESIZE, (function(t) {
                            return e.handleUpdate(t)
                        })) : t(window).off(l.RESIZE)
                    }, h._hideModal = function() {
                        var e = this;
                        this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._isTransitioning = !1, this._showBackdrop((function() {
                            t(document.body).removeClass(d), e._resetAdjustments(), e._resetScrollbar(), t(e._element).trigger(l.HIDDEN)
                        }))
                    }, h._removeBackdrop = function() {
                        this._backdrop && (t(this._backdrop).remove(), this._backdrop = null)
                    }, h._showBackdrop = function(e) {
                        var i = this,
                            n = t(this._element).hasClass(c) ? c : "";
                        if (this._isShown && this._config.backdrop) {
                            var o = S.supportsTransitionEnd() && n;
                            if (this._backdrop = document.createElement("div"), this._backdrop.className = "modal-backdrop", n && t(this._backdrop).addClass(n), t(this._backdrop).appendTo(document.body), t(this._element).on(l.CLICK_DISMISS, (function(t) {
                                    i._ignoreBackdropClick ? i._ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" === i._config.backdrop ? i._element.focus() : i.hide())
                                })), o && S.reflow(this._backdrop), t(this._backdrop).addClass(p), !e) return;
                            if (!o) return void e();
                            t(this._backdrop).one(S.TRANSITION_END, e).emulateTransitionEnd(150)
                        } else if (!this._isShown && this._backdrop) {
                            t(this._backdrop).removeClass(p);
                            var s = function() {
                                i._removeBackdrop(), e && e()
                            };
                            S.supportsTransitionEnd() && t(this._element).hasClass(c) ? t(this._backdrop).one(S.TRANSITION_END, s).emulateTransitionEnd(150) : s()
                        } else e && e()
                    }, h._adjustDialog = function() {
                        var t = this._element.scrollHeight > document.documentElement.clientHeight;
                        !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px")
                    }, h._resetAdjustments = function() {
                        this._element.style.paddingLeft = "", this._element.style.paddingRight = ""
                    }, h._checkScrollbar = function() {
                        var t = document.body.getBoundingClientRect();
                        this._isBodyOverflowing = t.left + t.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth()
                    }, h._setScrollbar = function() {
                        var e = this;
                        if (this._isBodyOverflowing) {
                            t(g).each((function(i, n) {
                                var o = t(n)[0].style.paddingRight,
                                    s = t(n).css("padding-right");
                                t(n).data("padding-right", o).css("padding-right", parseFloat(s) + e._scrollbarWidth + "px")
                            })), t(m).each((function(i, n) {
                                var o = t(n)[0].style.marginRight,
                                    s = t(n).css("margin-right");
                                t(n).data("margin-right", o).css("margin-right", parseFloat(s) - e._scrollbarWidth + "px")
                            })), t(v).each((function(i, n) {
                                var o = t(n)[0].style.marginRight,
                                    s = t(n).css("margin-right");
                                t(n).data("margin-right", o).css("margin-right", parseFloat(s) + e._scrollbarWidth + "px")
                            }));
                            var i = document.body.style.paddingRight,
                                n = t("body").css("padding-right");
                            t("body").data("padding-right", i).css("padding-right", parseFloat(n) + this._scrollbarWidth + "px")
                        }
                    }, h._resetScrollbar = function() {
                        t(g).each((function(e, i) {
                            var n = t(i).data("padding-right");
                            void 0 !== n && t(i).css("padding-right", n).removeData("padding-right")
                        })), t(m + ", " + v).each((function(e, i) {
                            var n = t(i).data("margin-right");
                            void 0 !== n && t(i).css("margin-right", n).removeData("margin-right")
                        }));
                        var e = t("body").data("padding-right");
                        void 0 !== e && t("body").css("padding-right", e).removeData("padding-right")
                    }, h._getScrollbarWidth = function() {
                        var t = document.createElement("div");
                        t.className = "modal-scrollbar-measure", document.body.appendChild(t);
                        var e = t.getBoundingClientRect().width - t.clientWidth;
                        return document.body.removeChild(t), e
                    }, n._jQueryInterface = function(i, o) {
                        return this.each((function() {
                            var r = t(this).data(e),
                                a = s({}, n.Default, t(this).data(), "object" == typeof i && i);
                            if (r || (r = new n(this, a), t(this).data(e, r)), "string" == typeof i) {
                                if (void 0 === r[i]) throw new TypeError('No method named "' + i + '"');
                                r[i](o)
                            } else a.show && r.show(o)
                        }))
                    }, o(n, null, [{
                        key: "VERSION",
                        get: function() {
                            return "4.0.0"
                        }
                    }, {
                        key: "Default",
                        get: function() {
                            return r
                        }
                    }]), n
                }();
            return t(document).on(l.CLICK_DATA_API, h, (function(i) {
                var n, o = this,
                    r = S.getSelectorFromElement(this);
                r && (n = t(r)[0]);
                var a = t(n).data(e) ? "toggle" : s({}, t(n).data(), t(this).data());
                "A" !== this.tagName && "AREA" !== this.tagName || i.preventDefault();
                var d = t(n).one(l.SHOW, (function(e) {
                    e.isDefaultPrevented() || d.one(l.HIDDEN, (function() {
                        t(o).is(":visible") && o.focus()
                    }))
                }));
                y._jQueryInterface.call(t(n), a, this)
            })), t.fn.modal = y._jQueryInterface, t.fn.modal.Constructor = y, t.fn.modal.noConflict = function() {
                return t.fn.modal = n, y._jQueryInterface
            }, y
        }(e),
        O = function(t) {
            var e = "tooltip",
                n = "bs.tooltip",
                r = "." + n,
                a = t.fn[e],
                l = new RegExp("(^|\\s)bs-tooltip\\S+", "g"),
                d = {
                    animation: "boolean",
                    template: "string",
                    title: "(string|element|function)",
                    trigger: "string",
                    delay: "(number|object)",
                    html: "boolean",
                    selector: "(string|boolean)",
                    placement: "(string|function)",
                    offset: "(number|string)",
                    container: "(string|element|boolean)",
                    fallbackPlacement: "(string|array)",
                    boundary: "(string|element)"
                },
                c = {
                    AUTO: "auto",
                    TOP: "top",
                    RIGHT: "right",
                    BOTTOM: "bottom",
                    LEFT: "left"
                },
                p = {
                    animation: !0,
                    template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
                    trigger: "hover focus",
                    title: "",
                    delay: 0,
                    html: !1,
                    selector: !1,
                    placement: "top",
                    offset: 0,
                    container: !1,
                    fallbackPlacement: "flip",
                    boundary: "scrollParent"
                },
                u = "show",
                h = "out",
                f = {
                    HIDE: "hide" + r,
                    HIDDEN: "hidden" + r,
                    SHOW: "show" + r,
                    SHOWN: "shown" + r,
                    INSERTED: "inserted" + r,
                    CLICK: "click" + r,
                    FOCUSIN: "focusin" + r,
                    FOCUSOUT: "focusout" + r,
                    MOUSEENTER: "mouseenter" + r,
                    MOUSELEAVE: "mouseleave" + r
                },
                g = "fade",
                m = "show",
                v = "hover",
                y = "focus",
                _ = function() {
                    function a(t, e) {
                        if (void 0 === i) throw new TypeError("Bootstrap tooltips require Popper.js (https://popper.js.org)");
                        this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = t, this.config = this._getConfig(e), this.tip = null, this._setListeners()
                    }
                    var _ = a.prototype;
                    return _.enable = function() {
                        this._isEnabled = !0
                    }, _.disable = function() {
                        this._isEnabled = !1
                    }, _.toggleEnabled = function() {
                        this._isEnabled = !this._isEnabled
                    }, _.toggle = function(e) {
                        if (this._isEnabled)
                            if (e) {
                                var i = this.constructor.DATA_KEY,
                                    n = t(e.currentTarget).data(i);
                                n || (n = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(i, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n)
                            } else {
                                if (t(this.getTipElement()).hasClass(m)) return void this._leave(null, this);
                                this._enter(null, this)
                            }
                    }, _.dispose = function() {
                        clearTimeout(this._timeout), t.removeData(this.element, this.constructor.DATA_KEY), t(this.element).off(this.constructor.EVENT_KEY), t(this.element).closest(".modal").off("hide.bs.modal"), this.tip && t(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, this._activeTrigger = null, null !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null
                    }, _.show = function() {
                        var e = this;
                        if ("none" === t(this.element).css("display")) throw new Error("Please use show on visible elements");
                        var n = t.Event(this.constructor.Event.SHOW);
                        if (this.isWithContent() && this._isEnabled) {
                            t(this.element).trigger(n);
                            var o = t.contains(this.element.ownerDocument.documentElement, this.element);
                            if (n.isDefaultPrevented() || !o) return;
                            var s = this.getTipElement(),
                                r = S.getUID(this.constructor.NAME);
                            s.setAttribute("id", r), this.element.setAttribute("aria-describedby", r), this.setContent(), this.config.animation && t(s).addClass(g);
                            var l = "function" == typeof this.config.placement ? this.config.placement.call(this, s, this.element) : this.config.placement,
                                d = this._getAttachment(l);
                            this.addAttachmentClass(d);
                            var c = !1 === this.config.container ? document.body : t(this.config.container);
                            t(s).data(this.constructor.DATA_KEY, this), t.contains(this.element.ownerDocument.documentElement, this.tip) || t(s).appendTo(c), t(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new i(this.element, s, {
                                placement: d,
                                modifiers: {
                                    offset: {
                                        offset: this.config.offset
                                    },
                                    flip: {
                                        behavior: this.config.fallbackPlacement
                                    },
                                    arrow: {
                                        element: ".arrow"
                                    },
                                    preventOverflow: {
                                        boundariesElement: this.config.boundary
                                    }
                                },
                                onCreate: function(t) {
                                    t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t)
                                },
                                onUpdate: function(t) {
                                    e._handlePopperPlacementChange(t)
                                }
                            }), t(s).addClass(m), "ontouchstart" in document.documentElement && t("body").children().on("mouseover", null, t.noop);
                            var p = function() {
                                e.config.animation && e._fixTransition();
                                var i = e._hoverState;
                                e._hoverState = null, t(e.element).trigger(e.constructor.Event.SHOWN), i === h && e._leave(null, e)
                            };
                            S.supportsTransitionEnd() && t(this.tip).hasClass(g) ? t(this.tip).one(S.TRANSITION_END, p).emulateTransitionEnd(a._TRANSITION_DURATION) : p()
                        }
                    }, _.hide = function(e) {
                        var i = this,
                            n = this.getTipElement(),
                            o = t.Event(this.constructor.Event.HIDE),
                            s = function() {
                                i._hoverState !== u && n.parentNode && n.parentNode.removeChild(n), i._cleanTipClass(), i.element.removeAttribute("aria-describedby"), t(i.element).trigger(i.constructor.Event.HIDDEN), null !== i._popper && i._popper.destroy(), e && e()
                            };
                        t(this.element).trigger(o), o.isDefaultPrevented() || (t(n).removeClass(m), "ontouchstart" in document.documentElement && t("body").children().off("mouseover", null, t.noop), this._activeTrigger.click = !1, this._activeTrigger[y] = !1, this._activeTrigger[v] = !1, S.supportsTransitionEnd() && t(this.tip).hasClass(g) ? t(n).one(S.TRANSITION_END, s).emulateTransitionEnd(150) : s(), this._hoverState = "")
                    }, _.update = function() {
                        null !== this._popper && this._popper.scheduleUpdate()
                    }, _.isWithContent = function() {
                        return Boolean(this.getTitle())
                    }, _.addAttachmentClass = function(e) {
                        t(this.getTipElement()).addClass("bs-tooltip-" + e)
                    }, _.getTipElement = function() {
                        return this.tip = this.tip || t(this.config.template)[0], this.tip
                    }, _.setContent = function() {
                        var e = t(this.getTipElement());
                        this.setElementContent(e.find(".tooltip-inner"), this.getTitle()), e.removeClass(g + " " + m)
                    }, _.setElementContent = function(e, i) {
                        var n = this.config.html;
                        "object" == typeof i && (i.nodeType || i.jquery) ? n ? t(i).parent().is(e) || e.empty().append(i) : e.text(t(i).text()) : e[n ? "html" : "text"](i)
                    }, _.getTitle = function() {
                        var t = this.element.getAttribute("data-original-title");
                        return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t
                    }, _._getAttachment = function(t) {
                        return c[t.toUpperCase()]
                    }, _._setListeners = function() {
                        var e = this;
                        this.config.trigger.split(" ").forEach((function(i) {
                            if ("click" === i) t(e.element).on(e.constructor.Event.CLICK, e.config.selector, (function(t) {
                                return e.toggle(t)
                            }));
                            else if ("manual" !== i) {
                                var n = i === v ? e.constructor.Event.MOUSEENTER : e.constructor.Event.FOCUSIN,
                                    o = i === v ? e.constructor.Event.MOUSELEAVE : e.constructor.Event.FOCUSOUT;
                                t(e.element).on(n, e.config.selector, (function(t) {
                                    return e._enter(t)
                                })).on(o, e.config.selector, (function(t) {
                                    return e._leave(t)
                                }))
                            }
                            t(e.element).closest(".modal").on("hide.bs.modal", (function() {
                                return e.hide()
                            }))
                        })), this.config.selector ? this.config = s({}, this.config, {
                            trigger: "manual",
                            selector: ""
                        }) : this._fixTitle()
                    }, _._fixTitle = function() {
                        var t = typeof this.element.getAttribute("data-original-title");
                        (this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
                    }, _._enter = function(e, i) {
                        var n = this.constructor.DATA_KEY;
                        (i = i || t(e.currentTarget).data(n)) || (i = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(n, i)), e && (i._activeTrigger["focusin" === e.type ? y : v] = !0), t(i.getTipElement()).hasClass(m) || i._hoverState === u ? i._hoverState = u : (clearTimeout(i._timeout), i._hoverState = u, i.config.delay && i.config.delay.show ? i._timeout = setTimeout((function() {
                            i._hoverState === u && i.show()
                        }), i.config.delay.show) : i.show())
                    }, _._leave = function(e, i) {
                        var n = this.constructor.DATA_KEY;
                        (i = i || t(e.currentTarget).data(n)) || (i = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(n, i)), e && (i._activeTrigger["focusout" === e.type ? y : v] = !1), i._isWithActiveTrigger() || (clearTimeout(i._timeout), i._hoverState = h, i.config.delay && i.config.delay.hide ? i._timeout = setTimeout((function() {
                            i._hoverState === h && i.hide()
                        }), i.config.delay.hide) : i.hide())
                    }, _._isWithActiveTrigger = function() {
                        for (var t in this._activeTrigger)
                            if (this._activeTrigger[t]) return !0;
                        return !1
                    }, _._getConfig = function(i) {
                        return "number" == typeof(i = s({}, this.constructor.Default, t(this.element).data(), i)).delay && (i.delay = {
                            show: i.delay,
                            hide: i.delay
                        }), "number" == typeof i.title && (i.title = i.title.toString()), "number" == typeof i.content && (i.content = i.content.toString()), S.typeCheckConfig(e, i, this.constructor.DefaultType), i
                    }, _._getDelegateConfig = function() {
                        var t = {};
                        if (this.config)
                            for (var e in this.config) this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
                        return t
                    }, _._cleanTipClass = function() {
                        var e = t(this.getTipElement()),
                            i = e.attr("class").match(l);
                        null !== i && i.length > 0 && e.removeClass(i.join(""))
                    }, _._handlePopperPlacementChange = function(t) {
                        this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement))
                    }, _._fixTransition = function() {
                        var e = this.getTipElement(),
                            i = this.config.animation;
                        null === e.getAttribute("x-placement") && (t(e).removeClass(g), this.config.animation = !1, this.hide(), this.show(), this.config.animation = i)
                    }, a._jQueryInterface = function(e) {
                        return this.each((function() {
                            var i = t(this).data(n),
                                o = "object" == typeof e && e;
                            if ((i || !/dispose|hide/.test(e)) && (i || (i = new a(this, o), t(this).data(n, i)), "string" == typeof e)) {
                                if (void 0 === i[e]) throw new TypeError('No method named "' + e + '"');
                                i[e]()
                            }
                        }))
                    }, o(a, null, [{
                        key: "VERSION",
                        get: function() {
                            return "4.0.0"
                        }
                    }, {
                        key: "Default",
                        get: function() {
                            return p
                        }
                    }, {
                        key: "NAME",
                        get: function() {
                            return e
                        }
                    }, {
                        key: "DATA_KEY",
                        get: function() {
                            return n
                        }
                    }, {
                        key: "Event",
                        get: function() {
                            return f
                        }
                    }, {
                        key: "EVENT_KEY",
                        get: function() {
                            return r
                        }
                    }, {
                        key: "DefaultType",
                        get: function() {
                            return d
                        }
                    }]), a
                }();
            return t.fn[e] = _._jQueryInterface, t.fn[e].Constructor = _, t.fn[e].noConflict = function() {
                return t.fn[e] = a, _._jQueryInterface
            }, _
        }(e),
        $ = function(t) {
            var e = "popover",
                i = "bs.popover",
                n = "." + i,
                r = t.fn[e],
                a = new RegExp("(^|\\s)bs-popover\\S+", "g"),
                l = s({}, O.Default, {
                    placement: "right",
                    trigger: "click",
                    content: "",
                    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
                }),
                d = s({}, O.DefaultType, {
                    content: "(string|element|function)"
                }),
                c = {
                    HIDE: "hide" + n,
                    HIDDEN: "hidden" + n,
                    SHOW: "show" + n,
                    SHOWN: "shown" + n,
                    INSERTED: "inserted" + n,
                    CLICK: "click" + n,
                    FOCUSIN: "focusin" + n,
                    FOCUSOUT: "focusout" + n,
                    MOUSEENTER: "mouseenter" + n,
                    MOUSELEAVE: "mouseleave" + n
                },
                p = function(s) {
                    var r, p;

                    function u() {
                        return s.apply(this, arguments) || this
                    }
                    p = s, (r = u).prototype = Object.create(p.prototype), r.prototype.constructor = r, r.__proto__ = p;
                    var h = u.prototype;
                    return h.isWithContent = function() {
                        return this.getTitle() || this._getContent()
                    }, h.addAttachmentClass = function(e) {
                        t(this.getTipElement()).addClass("bs-popover-" + e)
                    }, h.getTipElement = function() {
                        return this.tip = this.tip || t(this.config.template)[0], this.tip
                    }, h.setContent = function() {
                        var e = t(this.getTipElement());
                        this.setElementContent(e.find(".popover-header"), this.getTitle());
                        var i = this._getContent();
                        "function" == typeof i && (i = i.call(this.element)), this.setElementContent(e.find(".popover-body"), i), e.removeClass("fade show")
                    }, h._getContent = function() {
                        return this.element.getAttribute("data-content") || this.config.content
                    }, h._cleanTipClass = function() {
                        var e = t(this.getTipElement()),
                            i = e.attr("class").match(a);
                        null !== i && i.length > 0 && e.removeClass(i.join(""))
                    }, u._jQueryInterface = function(e) {
                        return this.each((function() {
                            var n = t(this).data(i),
                                o = "object" == typeof e ? e : null;
                            if ((n || !/destroy|hide/.test(e)) && (n || (n = new u(this, o), t(this).data(i, n)), "string" == typeof e)) {
                                if (void 0 === n[e]) throw new TypeError('No method named "' + e + '"');
                                n[e]()
                            }
                        }))
                    }, o(u, null, [{
                        key: "VERSION",
                        get: function() {
                            return "4.0.0"
                        }
                    }, {
                        key: "Default",
                        get: function() {
                            return l
                        }
                    }, {
                        key: "NAME",
                        get: function() {
                            return e
                        }
                    }, {
                        key: "DATA_KEY",
                        get: function() {
                            return i
                        }
                    }, {
                        key: "Event",
                        get: function() {
                            return c
                        }
                    }, {
                        key: "EVENT_KEY",
                        get: function() {
                            return n
                        }
                    }, {
                        key: "DefaultType",
                        get: function() {
                            return d
                        }
                    }]), u
                }(O);
            return t.fn[e] = p._jQueryInterface, t.fn[e].Constructor = p, t.fn[e].noConflict = function() {
                return t.fn[e] = r, p._jQueryInterface
            }, p
        }(e),
        N = function(t) {
            var e = "scrollspy",
                i = "bs.scrollspy",
                n = "." + i,
                r = t.fn[e],
                a = {
                    offset: 10,
                    method: "auto",
                    target: ""
                },
                l = {
                    offset: "number",
                    method: "string",
                    target: "(string|element)"
                },
                d = {
                    ACTIVATE: "activate" + n,
                    SCROLL: "scroll" + n,
                    LOAD_DATA_API: "load" + n + ".data-api"
                },
                c = "active",
                p = '[data-spy="scroll"]',
                u = ".active",
                h = ".nav, .list-group",
                f = ".nav-link",
                g = ".nav-item",
                m = ".list-group-item",
                v = ".dropdown",
                y = ".dropdown-item",
                _ = ".dropdown-toggle",
                b = "position",
                w = function() {
                    function r(e, i) {
                        var n = this;
                        this._element = e, this._scrollElement = "BODY" === e.tagName ? window : e, this._config = this._getConfig(i), this._selector = this._config.target + " " + f + "," + this._config.target + " " + m + "," + this._config.target + " " + y, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, t(this._scrollElement).on(d.SCROLL, (function(t) {
                            return n._process(t)
                        })), this.refresh(), this._process()
                    }
                    var p = r.prototype;
                    return p.refresh = function() {
                        var e = this,
                            i = this._scrollElement === this._scrollElement.window ? "offset" : b,
                            n = "auto" === this._config.method ? i : this._config.method,
                            o = n === b ? this._getScrollTop() : 0;
                        this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), t.makeArray(t(this._selector)).map((function(e) {
                            var i, s = S.getSelectorFromElement(e);
                            if (s && (i = t(s)[0]), i) {
                                var r = i.getBoundingClientRect();
                                if (r.width || r.height) return [t(i)[n]().top + o, s]
                            }
                            return null
                        })).filter((function(t) {
                            return t
                        })).sort((function(t, e) {
                            return t[0] - e[0]
                        })).forEach((function(t) {
                            e._offsets.push(t[0]), e._targets.push(t[1])
                        }))
                    }, p.dispose = function() {
                        t.removeData(this._element, i), t(this._scrollElement).off(n), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null
                    }, p._getConfig = function(i) {
                        if ("string" != typeof(i = s({}, a, i)).target) {
                            var n = t(i.target).attr("id");
                            n || (n = S.getUID(e), t(i.target).attr("id", n)), i.target = "#" + n
                        }
                        return S.typeCheckConfig(e, i, l), i
                    }, p._getScrollTop = function() {
                        return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
                    }, p._getScrollHeight = function() {
                        return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
                    }, p._getOffsetHeight = function() {
                        return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
                    }, p._process = function() {
                        var t = this._getScrollTop() + this._config.offset,
                            e = this._getScrollHeight(),
                            i = this._config.offset + e - this._getOffsetHeight();
                        if (this._scrollHeight !== e && this.refresh(), t >= i) {
                            var n = this._targets[this._targets.length - 1];
                            this._activeTarget !== n && this._activate(n)
                        } else {
                            if (this._activeTarget && t < this._offsets[0] && this._offsets[0] > 0) return this._activeTarget = null, void this._clear();
                            for (var o = this._offsets.length; o--;) this._activeTarget !== this._targets[o] && t >= this._offsets[o] && (void 0 === this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o])
                        }
                    }, p._activate = function(e) {
                        this._activeTarget = e, this._clear();
                        var i = this._selector.split(",");
                        i = i.map((function(t) {
                            return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]'
                        }));
                        var n = t(i.join(","));
                        n.hasClass("dropdown-item") ? (n.closest(v).find(_).addClass(c), n.addClass(c)) : (n.addClass(c), n.parents(h).prev(f + ", " + m).addClass(c), n.parents(h).prev(g).children(f).addClass(c)), t(this._scrollElement).trigger(d.ACTIVATE, {
                            relatedTarget: e
                        })
                    }, p._clear = function() {
                        t(this._selector).filter(u).removeClass(c)
                    }, r._jQueryInterface = function(e) {
                        return this.each((function() {
                            var n = t(this).data(i);
                            if (n || (n = new r(this, "object" == typeof e && e), t(this).data(i, n)), "string" == typeof e) {
                                if (void 0 === n[e]) throw new TypeError('No method named "' + e + '"');
                                n[e]()
                            }
                        }))
                    }, o(r, null, [{
                        key: "VERSION",
                        get: function() {
                            return "4.0.0"
                        }
                    }, {
                        key: "Default",
                        get: function() {
                            return a
                        }
                    }]), r
                }();
            return t(window).on(d.LOAD_DATA_API, (function() {
                for (var e = t.makeArray(t(p)), i = e.length; i--;) {
                    var n = t(e[i]);
                    w._jQueryInterface.call(n, n.data())
                }
            })), t.fn[e] = w._jQueryInterface, t.fn[e].Constructor = w, t.fn[e].noConflict = function() {
                return t.fn[e] = r, w._jQueryInterface
            }, w
        }(e),
        P = function(t) {
            var e = "bs.tab",
                i = "." + e,
                n = t.fn.tab,
                s = {
                    HIDE: "hide" + i,
                    HIDDEN: "hidden" + i,
                    SHOW: "show" + i,
                    SHOWN: "shown" + i,
                    CLICK_DATA_API: "click.bs.tab.data-api"
                },
                r = "active",
                a = "show",
                l = ".active",
                d = "> li > .active",
                c = function() {
                    function i(t) {
                        this._element = t
                    }
                    var n = i.prototype;
                    return n.show = function() {
                        var e = this;
                        if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && t(this._element).hasClass(r) || t(this._element).hasClass("disabled"))) {
                            var i, n, o = t(this._element).closest(".nav, .list-group")[0],
                                a = S.getSelectorFromElement(this._element);
                            if (o) {
                                var c = "UL" === o.nodeName ? d : l;
                                n = (n = t.makeArray(t(o).find(c)))[n.length - 1]
                            }
                            var p = t.Event(s.HIDE, {
                                    relatedTarget: this._element
                                }),
                                u = t.Event(s.SHOW, {
                                    relatedTarget: n
                                });
                            if (n && t(n).trigger(p), t(this._element).trigger(u), !u.isDefaultPrevented() && !p.isDefaultPrevented()) {
                                a && (i = t(a)[0]), this._activate(this._element, o);
                                var h = function() {
                                    var i = t.Event(s.HIDDEN, {
                                            relatedTarget: e._element
                                        }),
                                        o = t.Event(s.SHOWN, {
                                            relatedTarget: n
                                        });
                                    t(n).trigger(i), t(e._element).trigger(o)
                                };
                                i ? this._activate(i, i.parentNode, h) : h()
                            }
                        }
                    }, n.dispose = function() {
                        t.removeData(this._element, e), this._element = null
                    }, n._activate = function(e, i, n) {
                        var o = this,
                            s = ("UL" === i.nodeName ? t(i).find(d) : t(i).children(l))[0],
                            r = n && S.supportsTransitionEnd() && s && t(s).hasClass("fade"),
                            a = function() {
                                return o._transitionComplete(e, s, n)
                            };
                        s && r ? t(s).one(S.TRANSITION_END, a).emulateTransitionEnd(150) : a()
                    }, n._transitionComplete = function(e, i, n) {
                        if (i) {
                            t(i).removeClass(a + " " + r);
                            var o = t(i.parentNode).find("> .dropdown-menu .active")[0];
                            o && t(o).removeClass(r), "tab" === i.getAttribute("role") && i.setAttribute("aria-selected", !1)
                        }
                        if (t(e).addClass(r), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !0), S.reflow(e), t(e).addClass(a), e.parentNode && t(e.parentNode).hasClass("dropdown-menu")) {
                            var s = t(e).closest(".dropdown")[0];
                            s && t(s).find(".dropdown-toggle").addClass(r), e.setAttribute("aria-expanded", !0)
                        }
                        n && n()
                    }, i._jQueryInterface = function(n) {
                        return this.each((function() {
                            var o = t(this),
                                s = o.data(e);
                            if (s || (s = new i(this), o.data(e, s)), "string" == typeof n) {
                                if (void 0 === s[n]) throw new TypeError('No method named "' + n + '"');
                                s[n]()
                            }
                        }))
                    }, o(i, null, [{
                        key: "VERSION",
                        get: function() {
                            return "4.0.0"
                        }
                    }]), i
                }();
            return t(document).on(s.CLICK_DATA_API, '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', (function(e) {
                e.preventDefault(), c._jQueryInterface.call(t(this), "show")
            })), t.fn.tab = c._jQueryInterface, t.fn.tab.Constructor = c, t.fn.tab.noConflict = function() {
                return t.fn.tab = n, c._jQueryInterface
            }, c
        }(e);
    ! function(t) {
        if (void 0 === t) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
        var e = t.fn.jquery.split(" ")[0].split(".");
        if (e[0] < 2 && e[1] < 9 || 1 === e[0] && 9 === e[1] && e[2] < 1 || e[0] >= 4) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
    }(e), t.Util = S, t.Alert = C, t.Button = E, t.Carousel = A, t.Collapse = x, t.Dropdown = I, t.Modal = D, t.Popover = $, t.Scrollspy = N, t.Tab = P, t.Tooltip = O, Object.defineProperty(t, "__esModule", {
        value: !0
    })
})),
function(t, e, i) {
    var n = {
            label: "MENU",
            duplicate: !0,
            duration: 200,
            easingOpen: "swing",
            easingClose: "swing",
            closedSymbol: "&#9658;",
            openedSymbol: "&#9660;",
            prependTo: "body",
            appendTo: "",
            parentTag: "a",
            closeOnClick: !1,
            allowParentLinks: !1,
            nestedParentLinks: !0,
            showChildren: !1,
            removeIds: !0,
            removeClasses: !1,
            removeStyles: !1,
            brand: "",
            animations: "jquery",
            init: function() {},
            beforeOpen: function() {},
            beforeClose: function() {},
            afterOpen: function() {},
            afterClose: function() {}
        },
        o = "slicknav",
        s = "slicknav",
        r = 40,
        a = 13,
        l = 27,
        d = 37,
        c = 39,
        p = 32,
        u = 38;

    function h(e, i) {
        this.element = e, this.settings = t.extend({}, n, i), this.settings.duplicate || i.hasOwnProperty("removeIds") || (this.settings.removeIds = !1), this._defaults = n, this._name = o, this.init()
    }
    h.prototype.init = function() {
        var i, n, o = this,
            h = t(this.element),
            f = this.settings;
        if (f.duplicate ? o.mobileNav = h.clone() : o.mobileNav = h, f.removeIds && (o.mobileNav.removeAttr("id"), o.mobileNav.find("*").each((function(e, i) {
                t(i).removeAttr("id")
            }))), f.removeClasses && (o.mobileNav.removeAttr("class"), o.mobileNav.find("*").each((function(e, i) {
                t(i).removeAttr("class")
            }))), f.removeStyles && (o.mobileNav.removeAttr("style"), o.mobileNav.find("*").each((function(e, i) {
                t(i).removeAttr("style")
            }))), i = s + "_icon", "" === f.label && (i += " slicknav_no-text"), "a" == f.parentTag && (f.parentTag = 'a href="#"'), o.mobileNav.attr("class", s + "_nav"), n = t('<div class="slicknav_menu"></div>'), "" !== f.brand) {
            var g = t('<div class="slicknav_brand">' + f.brand + "</div>");
            t(n).append(g)
        }
        o.btn = t(['<div class="container"><' + f.parentTag + ' aria-haspopup="true" role="button" tabindex="0" class="' + s + "_btn " + s + '_collapsed">', '<span class="slicknav_menutxt">' + f.label + "</span>", '<span class="' + i + '">', '<span class=""><i class="ti-menu-alt"></i></span>', '<span class="ti-close"></span>', "</span>", "</" + f.parentTag + "></div>"].join("")), t(n).append(o.btn), "" !== f.appendTo ? t(f.appendTo).append(n) : t(f.prependTo).prepend(n), n.append(o.mobileNav);
        var m = o.mobileNav.find("li");
        t(m).each((function() {
            var e = t(this),
                i = {};
            if (i.children = e.children("ul").attr("role", "menu"), e.data("menu", i), i.children.length > 0) {
                var n = e.contents(),
                    r = !1,
                    a = [];
                t(n).each((function() {
                    if (t(this).is("ul")) return !1;
                    a.push(this), t(this).is("a") && (r = !0)
                }));
                var l = t("<" + f.parentTag + ' role="menuitem" aria-haspopup="true" tabindex="-1" class="' + s + '_item"/>');
                if (f.allowParentLinks && !f.nestedParentLinks && r) t(a).wrapAll('<span class="slicknav_parent-link slicknav_row"/>').parent();
                else t(a).wrapAll(l).parent().addClass(s + "_row");
                f.showChildren ? e.addClass(s + "_open") : e.addClass(s + "_collapsed"), e.addClass(s + "_parent");
                var d = t('<span class="slicknav_arrow">' + (f.showChildren ? f.openedSymbol : f.closedSymbol) + "</span>");
                f.allowParentLinks && !f.nestedParentLinks && r && (d = d.wrap(l).parent()), t(a).last().after(d)
            } else 0 === e.children().length && e.addClass(s + "_txtnode");
            e.children("a").attr("role", "menuitem").click((function(e) {
                f.closeOnClick && !t(e.target).parent().closest("li").hasClass(s + "_parent") && t(o.btn).click()
            })), f.closeOnClick && f.allowParentLinks && (e.children("a").children("a").click((function(e) {
                t(o.btn).click()
            })), e.find(".slicknav_parent-link a:not(.slicknav_item)").click((function(e) {
                t(o.btn).click()
            })))
        })), t(m).each((function() {
            var e = t(this).data("menu");
            f.showChildren || o._visibilityToggle(e.children, null, !1, null, !0)
        })), o._visibilityToggle(o.mobileNav, null, !1, "init", !0), o.mobileNav.attr("role", "menu"), t(e).mousedown((function() {
            o._outlines(!1)
        })), t(e).keyup((function() {
            o._outlines(!0)
        })), t(o.btn).click((function(t) {
            t.preventDefault(), o._menuToggle()
        })), o.mobileNav.on("click", ".slicknav_item", (function(e) {
            e.preventDefault(), o._itemClick(t(this))
        })), t(o.btn).keydown((function(e) {
            var i = e || event;
            switch (i.keyCode) {
                case a:
                case p:
                case r:
                    e.preventDefault(), i.keyCode === r && t(o.btn).hasClass(s + "_open") || o._menuToggle(), t(o.btn).next().find('[role="menuitem"]').first().focus()
            }
        })), o.mobileNav.on("keydown", ".slicknav_item", (function(e) {
            switch ((e || event).keyCode) {
                case a:
                    e.preventDefault(), o._itemClick(t(e.target));
                    break;
                case c:
                    e.preventDefault(), t(e.target).parent().hasClass(s + "_collapsed") && o._itemClick(t(e.target)), t(e.target).next().find('[role="menuitem"]').first().focus()
            }
        })), o.mobileNav.on("keydown", '[role="menuitem"]', (function(e) {
            switch ((e || event).keyCode) {
                case r:
                    e.preventDefault();
                    var i = (a = (n = t(e.target).parent().parent().children().children('[role="menuitem"]:visible')).index(e.target)) + 1;
                    n.length <= i && (i = 0), n.eq(i).focus();
                    break;
                case u:
                    e.preventDefault();
                    var n, a = (n = t(e.target).parent().parent().children().children('[role="menuitem"]:visible')).index(e.target);
                    n.eq(a - 1).focus();
                    break;
                case d:
                    if (e.preventDefault(), t(e.target).parent().parent().parent().hasClass(s + "_open")) {
                        var c = t(e.target).parent().parent().prev();
                        c.focus(), o._itemClick(c)
                    } else t(e.target).parent().parent().hasClass(s + "_nav") && (o._menuToggle(), t(o.btn).focus());
                    break;
                case l:
                    e.preventDefault(), o._menuToggle(), t(o.btn).focus()
            }
        })), f.allowParentLinks && f.nestedParentLinks && t(".slicknav_item a").click((function(t) {
            t.stopImmediatePropagation()
        }))
    }, h.prototype._menuToggle = function(t) {
        var e = this,
            i = e.btn,
            n = e.mobileNav;
        i.hasClass(s + "_collapsed") ? (i.removeClass(s + "_collapsed"), i.addClass(s + "_open")) : (i.removeClass(s + "_open"), i.addClass(s + "_collapsed")), i.addClass(s + "_animating"), e._visibilityToggle(n, i.parent(), !0, i)
    }, h.prototype._itemClick = function(t) {
        var e = this,
            i = e.settings,
            n = t.data("menu");
        n || ((n = {}).arrow = t.children(".slicknav_arrow"), n.ul = t.next("ul"), n.parent = t.parent(), n.parent.hasClass(s + "_parent-link") && (n.parent = t.parent().parent(), n.ul = t.parent().next("ul")), t.data("menu", n)), n.parent.hasClass(s + "_collapsed") ? (n.arrow.html(i.openedSymbol), n.parent.removeClass(s + "_collapsed"), n.parent.addClass(s + "_open"), n.parent.addClass(s + "_animating"), e._visibilityToggle(n.ul, n.parent, !0, t)) : (n.arrow.html(i.closedSymbol), n.parent.addClass(s + "_collapsed"), n.parent.removeClass(s + "_open"), n.parent.addClass(s + "_animating"), e._visibilityToggle(n.ul, n.parent, !0, t))
    }, h.prototype._visibilityToggle = function(e, i, n, o, r) {
        var a = this,
            l = a.settings,
            d = a._getActionItems(e),
            c = 0;

        function p(e, i) {
            t(e).removeClass(s + "_animating"), t(i).removeClass(s + "_animating"), r || l.afterOpen(e)
        }

        function u(i, n) {
            e.attr("aria-hidden", "true"), d.attr("tabindex", "-1"), a._setVisAttr(e, !0), e.hide(), t(i).removeClass(s + "_animating"), t(n).removeClass(s + "_animating"), r ? "init" == i && l.init() : l.afterClose(i)
        }
        n && (c = l.duration), e.hasClass(s + "_hidden") ? (e.removeClass(s + "_hidden"), r || l.beforeOpen(o), "jquery" === l.animations ? e.stop(!0, !0).slideDown(c, l.easingOpen, (function() {
            p(o, i)
        })) : "velocity" === l.animations && e.velocity("finish").velocity("slideDown", {
            duration: c,
            easing: l.easingOpen,
            complete: function() {
                p(o, i)
            }
        }), e.attr("aria-hidden", "false"), d.attr("tabindex", "0"), a._setVisAttr(e, !1)) : (e.addClass(s + "_hidden"), r || l.beforeClose(o), "jquery" === l.animations ? e.stop(!0, !0).slideUp(c, this.settings.easingClose, (function() {
            u(o, i)
        })) : "velocity" === l.animations && e.velocity("finish").velocity("slideUp", {
            duration: c,
            easing: l.easingClose,
            complete: function() {
                u(o, i)
            }
        }))
    }, h.prototype._setVisAttr = function(e, i) {
        var n = this,
            o = e.children("li").children("ul").not(".slicknav_hidden");
        i ? o.each((function() {
            var e = t(this);
            e.attr("aria-hidden", "true"), n._getActionItems(e).attr("tabindex", "-1"), n._setVisAttr(e, i)
        })) : o.each((function() {
            var e = t(this);
            e.attr("aria-hidden", "false"), n._getActionItems(e).attr("tabindex", "0"), n._setVisAttr(e, i)
        }))
    }, h.prototype._getActionItems = function(t) {
        var e = t.data("menu");
        if (!e) {
            e = {};
            var i = t.children("li"),
                n = i.find("a");
            e.links = n.add(i.find(".slicknav_item")), t.data("menu", e)
        }
        return e.links
    }, h.prototype._outlines = function(e) {
        e ? t(".slicknav_item, .slicknav_btn").css("outline", "") : t(".slicknav_item, .slicknav_btn").css("outline", "none")
    }, h.prototype.toggle = function() {
        this._menuToggle()
    }, h.prototype.open = function() {
        this.btn.hasClass(s + "_collapsed") && this._menuToggle()
    }, h.prototype.close = function() {
        this.btn.hasClass(s + "_open") && this._menuToggle()
    }, t.fn.slicknav = function(e) {
        var i, n = arguments;
        return void 0 === e || "object" == typeof e ? this.each((function() {
            t.data(this, "plugin_slicknav") || t.data(this, "plugin_slicknav", new h(this, e))
        })) : "string" == typeof e && "_" !== e[0] && "init" !== e ? (this.each((function() {
            var o = t.data(this, "plugin_slicknav");
            o instanceof h && "function" == typeof o[e] && (i = o[e].apply(o, Array.prototype.slice.call(n, 1)))
        })), void 0 !== i ? i : this) : void 0
    }
}(jQuery, document, window),
function(t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], t) : "undefined" != typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
}((function(t) {
    "use strict";
    var e = window.Slick || {};
    (e = function() {
        var e = 0;
        return function(i, n) {
            var o, s = this;
            s.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: t(i),
                appendDots: t(i),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
                nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function(e, i) {
                    return t('<button type="button" />').text(i + 1)
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                focusOnChange: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnFocus: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !0,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            }, s.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                scrolling: !1,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                swiping: !1,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            }, t.extend(s, s.initials), s.activeBreakpoint = null, s.animType = null, s.animProp = null, s.breakpoints = [], s.breakpointSettings = [], s.cssTransitions = !1, s.focussed = !1, s.interrupted = !1, s.hidden = "hidden", s.paused = !0, s.positionProp = null, s.respondTo = null, s.rowCount = 1, s.shouldClick = !0, s.$slider = t(i), s.$slidesCache = null, s.transformType = null, s.transitionType = null, s.visibilityChange = "visibilitychange", s.windowWidth = 0, s.windowTimer = null, o = t(i).data("slick") || {}, s.options = t.extend({}, s.defaults, n, o), s.currentSlide = s.options.initialSlide, s.originalSettings = s.options, void 0 !== document.mozHidden ? (s.hidden = "mozHidden", s.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (s.hidden = "webkitHidden", s.visibilityChange = "webkitvisibilitychange"), s.autoPlay = t.proxy(s.autoPlay, s), s.autoPlayClear = t.proxy(s.autoPlayClear, s), s.autoPlayIterator = t.proxy(s.autoPlayIterator, s), s.changeSlide = t.proxy(s.changeSlide, s), s.clickHandler = t.proxy(s.clickHandler, s), s.selectHandler = t.proxy(s.selectHandler, s), s.setPosition = t.proxy(s.setPosition, s), s.swipeHandler = t.proxy(s.swipeHandler, s), s.dragHandler = t.proxy(s.dragHandler, s), s.keyHandler = t.proxy(s.keyHandler, s), s.instanceUid = e++, s.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, s.registerBreakpoints(), s.init(!0)
        }
    }()).prototype.activateADA = function() {
        this.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        })
    }, e.prototype.addSlide = e.prototype.slickAdd = function(e, i, n) {
        var o = this;
        if ("boolean" == typeof i) n = i, i = null;
        else if (i < 0 || i >= o.slideCount) return !1;
        o.unload(), "number" == typeof i ? 0 === i && 0 === o.$slides.length ? t(e).appendTo(o.$slideTrack) : n ? t(e).insertBefore(o.$slides.eq(i)) : t(e).insertAfter(o.$slides.eq(i)) : !0 === n ? t(e).prependTo(o.$slideTrack) : t(e).appendTo(o.$slideTrack), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slides.each((function(e, i) {
            t(i).attr("data-slick-index", e)
        })), o.$slidesCache = o.$slides, o.reinit()
    }, e.prototype.animateHeight = function() {
        var t = this;
        if (1 === t.options.slidesToShow && !0 === t.options.adaptiveHeight && !1 === t.options.vertical) {
            var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
            t.$list.animate({
                height: e
            }, t.options.speed)
        }
    }, e.prototype.animateSlide = function(e, i) {
        var n = {},
            o = this;
        o.animateHeight(), !0 === o.options.rtl && !1 === o.options.vertical && (e = -e), !1 === o.transformsEnabled ? !1 === o.options.vertical ? o.$slideTrack.animate({
            left: e
        }, o.options.speed, o.options.easing, i) : o.$slideTrack.animate({
            top: e
        }, o.options.speed, o.options.easing, i) : !1 === o.cssTransitions ? (!0 === o.options.rtl && (o.currentLeft = -o.currentLeft), t({
            animStart: o.currentLeft
        }).animate({
            animStart: e
        }, {
            duration: o.options.speed,
            easing: o.options.easing,
            step: function(t) {
                t = Math.ceil(t), !1 === o.options.vertical ? (n[o.animType] = "translate(" + t + "px, 0px)", o.$slideTrack.css(n)) : (n[o.animType] = "translate(0px," + t + "px)", o.$slideTrack.css(n))
            },
            complete: function() {
                i && i.call()
            }
        })) : (o.applyTransition(), e = Math.ceil(e), !1 === o.options.vertical ? n[o.animType] = "translate3d(" + e + "px, 0px, 0px)" : n[o.animType] = "translate3d(0px," + e + "px, 0px)", o.$slideTrack.css(n), i && setTimeout((function() {
            o.disableTransition(), i.call()
        }), o.options.speed))
    }, e.prototype.getNavTarget = function() {
        var e = this.options.asNavFor;
        return e && null !== e && (e = t(e).not(this.$slider)), e
    }, e.prototype.asNavFor = function(e) {
        var i = this.getNavTarget();
        null !== i && "object" == typeof i && i.each((function() {
            var i = t(this).slick("getSlick");
            i.unslicked || i.slideHandler(e, !0)
        }))
    }, e.prototype.applyTransition = function(t) {
        var e = this,
            i = {};
        !1 === e.options.fade ? i[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : i[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase, !1 === e.options.fade ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i)
    }, e.prototype.autoPlay = function() {
        var t = this;
        t.autoPlayClear(), t.slideCount > t.options.slidesToShow && (t.autoPlayTimer = setInterval(t.autoPlayIterator, t.options.autoplaySpeed))
    }, e.prototype.autoPlayClear = function() {
        this.autoPlayTimer && clearInterval(this.autoPlayTimer)
    }, e.prototype.autoPlayIterator = function() {
        var t = this,
            e = t.currentSlide + t.options.slidesToScroll;
        t.paused || t.interrupted || t.focussed || (!1 === t.options.infinite && (1 === t.direction && t.currentSlide + 1 === t.slideCount - 1 ? t.direction = 0 : 0 === t.direction && (e = t.currentSlide - t.options.slidesToScroll, t.currentSlide - 1 == 0 && (t.direction = 1))), t.slideHandler(e))
    }, e.prototype.buildArrows = function() {
        var e = this;
        !0 === e.options.arrows && (e.$prevArrow = t(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = t(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }))
    }, e.prototype.buildDots = function() {
        var e, i, n = this;
        if (!0 === n.options.dots) {
            for (n.$slider.addClass("slick-dotted"), i = t("<ul />").addClass(n.options.dotsClass), e = 0; e <= n.getDotCount(); e += 1) i.append(t("<li />").append(n.options.customPaging.call(this, n, e)));
            n.$dots = i.appendTo(n.options.appendDots), n.$dots.find("li").first().addClass("slick-active")
        }
    }, e.prototype.buildOut = function() {
        var e = this;
        e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each((function(e, i) {
            t(i).attr("data-slick-index", e).data("originalStyling", t(i).attr("style") || "")
        })), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? t('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), t("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable")
    }, e.prototype.buildRows = function() {
        var t, e, i, n, o, s, r, a = this;
        if (n = document.createDocumentFragment(), s = a.$slider.children(), a.options.rows > 1) {
            for (r = a.options.slidesPerRow * a.options.rows, o = Math.ceil(s.length / r), t = 0; t < o; t++) {
                var l = document.createElement("div");
                for (e = 0; e < a.options.rows; e++) {
                    var d = document.createElement("div");
                    for (i = 0; i < a.options.slidesPerRow; i++) {
                        var c = t * r + (e * a.options.slidesPerRow + i);
                        s.get(c) && d.appendChild(s.get(c))
                    }
                    l.appendChild(d)
                }
                n.appendChild(l)
            }
            a.$slider.empty().append(n), a.$slider.children().children().children().css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block"
            })
        }
    }, e.prototype.checkResponsive = function(e, i) {
        var n, o, s, r = this,
            a = !1,
            l = r.$slider.width(),
            d = window.innerWidth || t(window).width();
        if ("window" === r.respondTo ? s = d : "slider" === r.respondTo ? s = l : "min" === r.respondTo && (s = Math.min(d, l)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
            for (n in o = null, r.breakpoints) r.breakpoints.hasOwnProperty(n) && (!1 === r.originalSettings.mobileFirst ? s < r.breakpoints[n] && (o = r.breakpoints[n]) : s > r.breakpoints[n] && (o = r.breakpoints[n]));
            null !== o ? null !== r.activeBreakpoint ? (o !== r.activeBreakpoint || i) && (r.activeBreakpoint = o, "unslick" === r.breakpointSettings[o] ? r.unslick(o) : (r.options = t.extend({}, r.originalSettings, r.breakpointSettings[o]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), a = o) : (r.activeBreakpoint = o, "unslick" === r.breakpointSettings[o] ? r.unslick(o) : (r.options = t.extend({}, r.originalSettings, r.breakpointSettings[o]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), a = o) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e), a = o), e || !1 === a || r.$slider.trigger("breakpoint", [r, a])
        }
    }, e.prototype.changeSlide = function(e, i) {
        var n, o, s = this,
            r = t(e.currentTarget);
        switch (r.is("a") && e.preventDefault(), r.is("li") || (r = r.closest("li")), n = s.slideCount % s.options.slidesToScroll != 0 ? 0 : (s.slideCount - s.currentSlide) % s.options.slidesToScroll, e.data.message) {
            case "previous":
                o = 0 === n ? s.options.slidesToScroll : s.options.slidesToShow - n, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide - o, !1, i);
                break;
            case "next":
                o = 0 === n ? s.options.slidesToScroll : n, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide + o, !1, i);
                break;
            case "index":
                var a = 0 === e.data.index ? 0 : e.data.index || r.index() * s.options.slidesToScroll;
                s.slideHandler(s.checkNavigable(a), !1, i), r.children().trigger("focus");
                break;
            default:
                return
        }
    }, e.prototype.checkNavigable = function(t) {
        var e, i;
        if (i = 0, t > (e = this.getNavigableIndexes())[e.length - 1]) t = e[e.length - 1];
        else
            for (var n in e) {
                if (t < e[n]) {
                    t = i;
                    break
                }
                i = e[n]
            }
        return t
    }, e.prototype.cleanUpEvents = function() {
        var e = this;
        e.options.dots && null !== e.$dots && (t("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", t.proxy(e.interrupt, e, !0)).off("mouseleave.slick", t.proxy(e.interrupt, e, !1)), !0 === e.options.accessibility && e.$dots.off("keydown.slick", e.keyHandler)), e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler), e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), t(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && t(e.$slideTrack).children().off("click.slick", e.selectHandler), t(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), t(window).off("resize.slick.slick-" + e.instanceUid, e.resize), t("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), t(window).off("load.slick.slick-" + e.instanceUid, e.setPosition)
    }, e.prototype.cleanUpSlideEvents = function() {
        var e = this;
        e.$list.off("mouseenter.slick", t.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", t.proxy(e.interrupt, e, !1))
    }, e.prototype.cleanUpRows = function() {
        var t, e = this;
        e.options.rows > 1 && ((t = e.$slides.children().children()).removeAttr("style"), e.$slider.empty().append(t))
    }, e.prototype.clickHandler = function(t) {
        !1 === this.shouldClick && (t.stopImmediatePropagation(), t.stopPropagation(), t.preventDefault())
    }, e.prototype.destroy = function(e) {
        var i = this;
        i.autoPlayClear(), i.touchObject = {}, i.cleanUpEvents(), t(".slick-cloned", i.$slider).detach(), i.$dots && i.$dots.remove(), i.$prevArrow && i.$prevArrow.length && (i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove()), i.$nextArrow && i.$nextArrow.length && (i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove()), i.$slides && (i.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each((function() {
            t(this).attr("style", t(this).data("originalStyling"))
        })), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.detach(), i.$list.detach(), i.$slider.append(i.$slides)), i.cleanUpRows(), i.$slider.removeClass("slick-slider"), i.$slider.removeClass("slick-initialized"), i.$slider.removeClass("slick-dotted"), i.unslicked = !0, e || i.$slider.trigger("destroy", [i])
    }, e.prototype.disableTransition = function(t) {
        var e = this,
            i = {};
        i[e.transitionType] = "", !1 === e.options.fade ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i)
    }, e.prototype.fadeSlide = function(t, e) {
        var i = this;
        !1 === i.cssTransitions ? (i.$slides.eq(t).css({
            zIndex: i.options.zIndex
        }), i.$slides.eq(t).animate({
            opacity: 1
        }, i.options.speed, i.options.easing, e)) : (i.applyTransition(t), i.$slides.eq(t).css({
            opacity: 1,
            zIndex: i.options.zIndex
        }), e && setTimeout((function() {
            i.disableTransition(t), e.call()
        }), i.options.speed))
    }, e.prototype.fadeSlideOut = function(t) {
        var e = this;
        !1 === e.cssTransitions ? e.$slides.eq(t).animate({
            opacity: 0,
            zIndex: e.options.zIndex - 2
        }, e.options.speed, e.options.easing) : (e.applyTransition(t), e.$slides.eq(t).css({
            opacity: 0,
            zIndex: e.options.zIndex - 2
        }))
    }, e.prototype.filterSlides = e.prototype.slickFilter = function(t) {
        var e = this;
        null !== t && (e.$slidesCache = e.$slides, e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.filter(t).appendTo(e.$slideTrack), e.reinit())
    }, e.prototype.focusHandler = function() {
        var e = this;
        e.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", (function(i) {
            i.stopImmediatePropagation();
            var n = t(this);
            setTimeout((function() {
                e.options.pauseOnFocus && (e.focussed = n.is(":focus"), e.autoPlay())
            }), 0)
        }))
    }, e.prototype.getCurrent = e.prototype.slickCurrentSlide = function() {
        return this.currentSlide
    }, e.prototype.getDotCount = function() {
        var t = this,
            e = 0,
            i = 0,
            n = 0;
        if (!0 === t.options.infinite)
            if (t.slideCount <= t.options.slidesToShow) ++n;
            else
                for (; e < t.slideCount;) ++n, e = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        else if (!0 === t.options.centerMode) n = t.slideCount;
        else if (t.options.asNavFor)
            for (; e < t.slideCount;) ++n, e = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        else n = 1 + Math.ceil((t.slideCount - t.options.slidesToShow) / t.options.slidesToScroll);
        return n - 1
    }, e.prototype.getLeft = function(t) {
        var e, i, n, o, s = this,
            r = 0;
        return s.slideOffset = 0, i = s.$slides.first().outerHeight(!0), !0 === s.options.infinite ? (s.slideCount > s.options.slidesToShow && (s.slideOffset = s.slideWidth * s.options.slidesToShow * -1, o = -1, !0 === s.options.vertical && !0 === s.options.centerMode && (2 === s.options.slidesToShow ? o = -1.5 : 1 === s.options.slidesToShow && (o = -2)), r = i * s.options.slidesToShow * o), s.slideCount % s.options.slidesToScroll != 0 && t + s.options.slidesToScroll > s.slideCount && s.slideCount > s.options.slidesToShow && (t > s.slideCount ? (s.slideOffset = (s.options.slidesToShow - (t - s.slideCount)) * s.slideWidth * -1, r = (s.options.slidesToShow - (t - s.slideCount)) * i * -1) : (s.slideOffset = s.slideCount % s.options.slidesToScroll * s.slideWidth * -1, r = s.slideCount % s.options.slidesToScroll * i * -1))) : t + s.options.slidesToShow > s.slideCount && (s.slideOffset = (t + s.options.slidesToShow - s.slideCount) * s.slideWidth, r = (t + s.options.slidesToShow - s.slideCount) * i), s.slideCount <= s.options.slidesToShow && (s.slideOffset = 0, r = 0), !0 === s.options.centerMode && s.slideCount <= s.options.slidesToShow ? s.slideOffset = s.slideWidth * Math.floor(s.options.slidesToShow) / 2 - s.slideWidth * s.slideCount / 2 : !0 === s.options.centerMode && !0 === s.options.infinite ? s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2) - s.slideWidth : !0 === s.options.centerMode && (s.slideOffset = 0, s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2)), e = !1 === s.options.vertical ? t * s.slideWidth * -1 + s.slideOffset : t * i * -1 + r, !0 === s.options.variableWidth && (n = s.slideCount <= s.options.slidesToShow || !1 === s.options.infinite ? s.$slideTrack.children(".slick-slide").eq(t) : s.$slideTrack.children(".slick-slide").eq(t + s.options.slidesToShow), e = !0 === s.options.rtl ? n[0] ? -1 * (s.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, !0 === s.options.centerMode && (n = s.slideCount <= s.options.slidesToShow || !1 === s.options.infinite ? s.$slideTrack.children(".slick-slide").eq(t) : s.$slideTrack.children(".slick-slide").eq(t + s.options.slidesToShow + 1), e = !0 === s.options.rtl ? n[0] ? -1 * (s.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, e += (s.$list.width() - n.outerWidth()) / 2)), e
    }, e.prototype.getOption = e.prototype.slickGetOption = function(t) {
        return this.options[t]
    }, e.prototype.getNavigableIndexes = function() {
        var t, e = this,
            i = 0,
            n = 0,
            o = [];
        for (!1 === e.options.infinite ? t = e.slideCount : (i = -1 * e.options.slidesToScroll, n = -1 * e.options.slidesToScroll, t = 2 * e.slideCount); i < t;) o.push(i), i = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
        return o
    }, e.prototype.getSlick = function() {
        return this
    }, e.prototype.getSlideCount = function() {
        var e, i, n = this;
        return i = !0 === n.options.centerMode ? n.slideWidth * Math.floor(n.options.slidesToShow / 2) : 0, !0 === n.options.swipeToSlide ? (n.$slideTrack.find(".slick-slide").each((function(o, s) {
            if (s.offsetLeft - i + t(s).outerWidth() / 2 > -1 * n.swipeLeft) return e = s, !1
        })), Math.abs(t(e).attr("data-slick-index") - n.currentSlide) || 1) : n.options.slidesToScroll
    }, e.prototype.goTo = e.prototype.slickGoTo = function(t, e) {
        this.changeSlide({
            data: {
                message: "index",
                index: parseInt(t)
            }
        }, e)
    }, e.prototype.init = function(e) {
        var i = this;
        t(i.$slider).hasClass("slick-initialized") || (t(i.$slider).addClass("slick-initialized"), i.buildRows(), i.buildOut(), i.setProps(), i.startLoad(), i.loadSlider(), i.initializeEvents(), i.updateArrows(), i.updateDots(), i.checkResponsive(!0), i.focusHandler()), e && i.$slider.trigger("init", [i]), !0 === i.options.accessibility && i.initADA(), i.options.autoplay && (i.paused = !1, i.autoPlay())
    }, e.prototype.initADA = function() {
        var e = this,
            i = Math.ceil(e.slideCount / e.options.slidesToShow),
            n = e.getNavigableIndexes().filter((function(t) {
                return t >= 0 && t < e.slideCount
            }));
        e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({
            tabindex: "-1"
        }), null !== e.$dots && (e.$slides.not(e.$slideTrack.find(".slick-cloned")).each((function(i) {
            var o = n.indexOf(i);
            t(this).attr({
                role: "tabpanel",
                id: "slick-slide" + e.instanceUid + i,
                tabindex: -1
            }), -1 !== o && t(this).attr({
                "aria-describedby": "slick-slide-control" + e.instanceUid + o
            })
        })), e.$dots.attr("role", "tablist").find("li").each((function(o) {
            var s = n[o];
            t(this).attr({
                role: "presentation"
            }), t(this).find("button").first().attr({
                role: "tab",
                id: "slick-slide-control" + e.instanceUid + o,
                "aria-controls": "slick-slide" + e.instanceUid + s,
                "aria-label": o + 1 + " of " + i,
                "aria-selected": null,
                tabindex: "-1"
            })
        })).eq(e.currentSlide).find("button").attr({
            "aria-selected": "true",
            tabindex: "0"
        }).end());
        for (var o = e.currentSlide, s = o + e.options.slidesToShow; o < s; o++) e.$slides.eq(o).attr("tabindex", 0);
        e.activateADA()
    }, e.prototype.initArrowEvents = function() {
        var t = this;
        !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.off("click.slick").on("click.slick", {
            message: "previous"
        }, t.changeSlide), t.$nextArrow.off("click.slick").on("click.slick", {
            message: "next"
        }, t.changeSlide), !0 === t.options.accessibility && (t.$prevArrow.on("keydown.slick", t.keyHandler), t.$nextArrow.on("keydown.slick", t.keyHandler)))
    }, e.prototype.initDotEvents = function() {
        var e = this;
        !0 === e.options.dots && (t("li", e.$dots).on("click.slick", {
            message: "index"
        }, e.changeSlide), !0 === e.options.accessibility && e.$dots.on("keydown.slick", e.keyHandler)), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && t("li", e.$dots).on("mouseenter.slick", t.proxy(e.interrupt, e, !0)).on("mouseleave.slick", t.proxy(e.interrupt, e, !1))
    }, e.prototype.initSlideEvents = function() {
        var e = this;
        e.options.pauseOnHover && (e.$list.on("mouseenter.slick", t.proxy(e.interrupt, e, !0)), e.$list.on("mouseleave.slick", t.proxy(e.interrupt, e, !1)))
    }, e.prototype.initializeEvents = function() {
        var e = this;
        e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), t(document).on(e.visibilityChange, t.proxy(e.visibility, e)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && t(e.$slideTrack).children().on("click.slick", e.selectHandler), t(window).on("orientationchange.slick.slick-" + e.instanceUid, t.proxy(e.orientationChange, e)), t(window).on("resize.slick.slick-" + e.instanceUid, t.proxy(e.resize, e)), t("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), t(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), t(e.setPosition)
    }, e.prototype.initUI = function() {
        var t = this;
        !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.show(), t.$nextArrow.show()), !0 === t.options.dots && t.slideCount > t.options.slidesToShow && t.$dots.show()
    }, e.prototype.keyHandler = function(t) {
        var e = this;
        t.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === t.keyCode && !0 === e.options.accessibility ? e.changeSlide({
            data: {
                message: !0 === e.options.rtl ? "next" : "previous"
            }
        }) : 39 === t.keyCode && !0 === e.options.accessibility && e.changeSlide({
            data: {
                message: !0 === e.options.rtl ? "previous" : "next"
            }
        }))
    }, e.prototype.lazyLoad = function() {
        function e(e) {
            t("img[data-lazy]", e).each((function() {
                var e = t(this),
                    i = t(this).attr("data-lazy"),
                    n = t(this).attr("data-srcset"),
                    o = t(this).attr("data-sizes") || s.$slider.attr("data-sizes"),
                    r = document.createElement("img");
                r.onload = function() {
                    e.animate({
                        opacity: 0
                    }, 100, (function() {
                        n && (e.attr("srcset", n), o && e.attr("sizes", o)), e.attr("src", i).animate({
                            opacity: 1
                        }, 200, (function() {
                            e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")
                        })), s.$slider.trigger("lazyLoaded", [s, e, i])
                    }))
                }, r.onerror = function() {
                    e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), s.$slider.trigger("lazyLoadError", [s, e, i])
                }, r.src = i
            }))
        }
        var i, n, o, s = this;
        if (!0 === s.options.centerMode ? !0 === s.options.infinite ? o = (n = s.currentSlide + (s.options.slidesToShow / 2 + 1)) + s.options.slidesToShow + 2 : (n = Math.max(0, s.currentSlide - (s.options.slidesToShow / 2 + 1)), o = s.options.slidesToShow / 2 + 1 + 2 + s.currentSlide) : (n = s.options.infinite ? s.options.slidesToShow + s.currentSlide : s.currentSlide, o = Math.ceil(n + s.options.slidesToShow), !0 === s.options.fade && (n > 0 && n--, o <= s.slideCount && o++)), i = s.$slider.find(".slick-slide").slice(n, o), "anticipated" === s.options.lazyLoad)
            for (var r = n - 1, a = o, l = s.$slider.find(".slick-slide"), d = 0; d < s.options.slidesToScroll; d++) r < 0 && (r = s.slideCount - 1), i = (i = i.add(l.eq(r))).add(l.eq(a)), r--, a++;
        e(i), s.slideCount <= s.options.slidesToShow ? e(s.$slider.find(".slick-slide")) : s.currentSlide >= s.slideCount - s.options.slidesToShow ? e(s.$slider.find(".slick-cloned").slice(0, s.options.slidesToShow)) : 0 === s.currentSlide && e(s.$slider.find(".slick-cloned").slice(-1 * s.options.slidesToShow))
    }, e.prototype.loadSlider = function() {
        var t = this;
        t.setPosition(), t.$slideTrack.css({
            opacity: 1
        }), t.$slider.removeClass("slick-loading"), t.initUI(), "progressive" === t.options.lazyLoad && t.progressiveLazyLoad()
    }, e.prototype.next = e.prototype.slickNext = function() {
        this.changeSlide({
            data: {
                message: "next"
            }
        })
    }, e.prototype.orientationChange = function() {
        this.checkResponsive(), this.setPosition()
    }, e.prototype.pause = e.prototype.slickPause = function() {
        this.autoPlayClear(), this.paused = !0
    }, e.prototype.play = e.prototype.slickPlay = function() {
        var t = this;
        t.autoPlay(), t.options.autoplay = !0, t.paused = !1, t.focussed = !1, t.interrupted = !1
    }, e.prototype.postSlide = function(e) {
        var i = this;
        i.unslicked || (i.$slider.trigger("afterChange", [i, e]), i.animating = !1, i.slideCount > i.options.slidesToShow && i.setPosition(), i.swipeLeft = null, i.options.autoplay && i.autoPlay(), !0 === i.options.accessibility && (i.initADA(), i.options.focusOnChange && t(i.$slides.get(i.currentSlide)).attr("tabindex", 0).focus()))
    }, e.prototype.prev = e.prototype.slickPrev = function() {
        this.changeSlide({
            data: {
                message: "previous"
            }
        })
    }, e.prototype.preventDefault = function(t) {
        t.preventDefault()
    }, e.prototype.progressiveLazyLoad = function(e) {
        e = e || 1;
        var i, n, o, s, r, a = this,
            l = t("img[data-lazy]", a.$slider);
        l.length ? (i = l.first(), n = i.attr("data-lazy"), o = i.attr("data-srcset"), s = i.attr("data-sizes") || a.$slider.attr("data-sizes"), (r = document.createElement("img")).onload = function() {
            o && (i.attr("srcset", o), s && i.attr("sizes", s)), i.attr("src", n).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === a.options.adaptiveHeight && a.setPosition(), a.$slider.trigger("lazyLoaded", [a, i, n]), a.progressiveLazyLoad()
        }, r.onerror = function() {
            e < 3 ? setTimeout((function() {
                a.progressiveLazyLoad(e + 1)
            }), 500) : (i.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), a.$slider.trigger("lazyLoadError", [a, i, n]), a.progressiveLazyLoad())
        }, r.src = n) : a.$slider.trigger("allImagesLoaded", [a])
    }, e.prototype.refresh = function(e) {
        var i, n, o = this;
        n = o.slideCount - o.options.slidesToShow, !o.options.infinite && o.currentSlide > n && (o.currentSlide = n), o.slideCount <= o.options.slidesToShow && (o.currentSlide = 0), i = o.currentSlide, o.destroy(!0), t.extend(o, o.initials, {
            currentSlide: i
        }), o.init(), e || o.changeSlide({
            data: {
                message: "index",
                index: i
            }
        }, !1)
    }, e.prototype.registerBreakpoints = function() {
        var e, i, n, o = this,
            s = o.options.responsive || null;
        if ("array" === t.type(s) && s.length) {
            for (e in o.respondTo = o.options.respondTo || "window", s)
                if (n = o.breakpoints.length - 1, s.hasOwnProperty(e)) {
                    for (i = s[e].breakpoint; n >= 0;) o.breakpoints[n] && o.breakpoints[n] === i && o.breakpoints.splice(n, 1), n--;
                    o.breakpoints.push(i), o.breakpointSettings[i] = s[e].settings
                }
            o.breakpoints.sort((function(t, e) {
                return o.options.mobileFirst ? t - e : e - t
            }))
        }
    }, e.prototype.reinit = function() {
        var e = this;
        e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && t(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e])
    }, e.prototype.resize = function() {
        var e = this;
        t(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout((function() {
            e.windowWidth = t(window).width(), e.checkResponsive(), e.unslicked || e.setPosition()
        }), 50))
    }, e.prototype.removeSlide = e.prototype.slickRemove = function(t, e, i) {
        var n = this;
        if (t = "boolean" == typeof t ? !0 === (e = t) ? 0 : n.slideCount - 1 : !0 === e ? --t : t, n.slideCount < 1 || t < 0 || t > n.slideCount - 1) return !1;
        n.unload(), !0 === i ? n.$slideTrack.children().remove() : n.$slideTrack.children(this.options.slide).eq(t).remove(), n.$slides = n.$slideTrack.children(this.options.slide), n.$slideTrack.children(this.options.slide).detach(), n.$slideTrack.append(n.$slides), n.$slidesCache = n.$slides, n.reinit()
    }, e.prototype.setCSS = function(t) {
        var e, i, n = this,
            o = {};
        !0 === n.options.rtl && (t = -t), e = "left" == n.positionProp ? Math.ceil(t) + "px" : "0px", i = "top" == n.positionProp ? Math.ceil(t) + "px" : "0px", o[n.positionProp] = t, !1 === n.transformsEnabled ? n.$slideTrack.css(o) : (o = {}, !1 === n.cssTransitions ? (o[n.animType] = "translate(" + e + ", " + i + ")", n.$slideTrack.css(o)) : (o[n.animType] = "translate3d(" + e + ", " + i + ", 0px)", n.$slideTrack.css(o)))
    }, e.prototype.setDimensions = function() {
        var t = this;
        !1 === t.options.vertical ? !0 === t.options.centerMode && t.$list.css({
            padding: "0px " + t.options.centerPadding
        }) : (t.$list.height(t.$slides.first().outerHeight(!0) * t.options.slidesToShow), !0 === t.options.centerMode && t.$list.css({
            padding: t.options.centerPadding + " 0px"
        })), t.listWidth = t.$list.width(), t.listHeight = t.$list.height(), !1 === t.options.vertical && !1 === t.options.variableWidth ? (t.slideWidth = Math.ceil(t.listWidth / t.options.slidesToShow), t.$slideTrack.width(Math.ceil(t.slideWidth * t.$slideTrack.children(".slick-slide").length))) : !0 === t.options.variableWidth ? t.$slideTrack.width(5e3 * t.slideCount) : (t.slideWidth = Math.ceil(t.listWidth), t.$slideTrack.height(Math.ceil(t.$slides.first().outerHeight(!0) * t.$slideTrack.children(".slick-slide").length)));
        var e = t.$slides.first().outerWidth(!0) - t.$slides.first().width();
        !1 === t.options.variableWidth && t.$slideTrack.children(".slick-slide").width(t.slideWidth - e)
    }, e.prototype.setFade = function() {
        var e, i = this;
        i.$slides.each((function(n, o) {
            e = i.slideWidth * n * -1, !0 === i.options.rtl ? t(o).css({
                position: "relative",
                right: e,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0
            }) : t(o).css({
                position: "relative",
                left: e,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0
            })
        })), i.$slides.eq(i.currentSlide).css({
            zIndex: i.options.zIndex - 1,
            opacity: 1
        })
    }, e.prototype.setHeight = function() {
        var t = this;
        if (1 === t.options.slidesToShow && !0 === t.options.adaptiveHeight && !1 === t.options.vertical) {
            var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
            t.$list.css("height", e)
        }
    }, e.prototype.setOption = e.prototype.slickSetOption = function() {
        var e, i, n, o, s, r = this,
            a = !1;
        if ("object" === t.type(arguments[0]) ? (n = arguments[0], a = arguments[1], s = "multiple") : "string" === t.type(arguments[0]) && (n = arguments[0], o = arguments[1], a = arguments[2], "responsive" === arguments[0] && "array" === t.type(arguments[1]) ? s = "responsive" : void 0 !== arguments[1] && (s = "single")), "single" === s) r.options[n] = o;
        else if ("multiple" === s) t.each(n, (function(t, e) {
            r.options[t] = e
        }));
        else if ("responsive" === s)
            for (i in o)
                if ("array" !== t.type(r.options.responsive)) r.options.responsive = [o[i]];
                else {
                    for (e = r.options.responsive.length - 1; e >= 0;) r.options.responsive[e].breakpoint === o[i].breakpoint && r.options.responsive.splice(e, 1), e--;
                    r.options.responsive.push(o[i])
                }
        a && (r.unload(), r.reinit())
    }, e.prototype.setPosition = function() {
        var t = this;
        t.setDimensions(), t.setHeight(), !1 === t.options.fade ? t.setCSS(t.getLeft(t.currentSlide)) : t.setFade(), t.$slider.trigger("setPosition", [t])
    }, e.prototype.setProps = function() {
        var t = this,
            e = document.body.style;
        t.positionProp = !0 === t.options.vertical ? "top" : "left", "top" === t.positionProp ? t.$slider.addClass("slick-vertical") : t.$slider.removeClass("slick-vertical"), void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || !0 === t.options.useCSS && (t.cssTransitions = !0), t.options.fade && ("number" == typeof t.options.zIndex ? t.options.zIndex < 3 && (t.options.zIndex = 3) : t.options.zIndex = t.defaults.zIndex), void 0 !== e.OTransform && (t.animType = "OTransform", t.transformType = "-o-transform", t.transitionType = "OTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)), void 0 !== e.MozTransform && (t.animType = "MozTransform", t.transformType = "-moz-transform", t.transitionType = "MozTransition", void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (t.animType = !1)), void 0 !== e.webkitTransform && (t.animType = "webkitTransform", t.transformType = "-webkit-transform", t.transitionType = "webkitTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)), void 0 !== e.msTransform && (t.animType = "msTransform", t.transformType = "-ms-transform", t.transitionType = "msTransition", void 0 === e.msTransform && (t.animType = !1)), void 0 !== e.transform && !1 !== t.animType && (t.animType = "transform", t.transformType = "transform", t.transitionType = "transition"), t.transformsEnabled = t.options.useTransform && null !== t.animType && !1 !== t.animType
    }, e.prototype.setSlideClasses = function(t) {
        var e, i, n, o, s = this;
        if (i = s.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), s.$slides.eq(t).addClass("slick-current"), !0 === s.options.centerMode) {
            var r = s.options.slidesToShow % 2 == 0 ? 1 : 0;
            e = Math.floor(s.options.slidesToShow / 2), !0 === s.options.infinite && (t >= e && t <= s.slideCount - 1 - e ? s.$slides.slice(t - e + r, t + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (n = s.options.slidesToShow + t, i.slice(n - e + 1 + r, n + e + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === t ? i.eq(i.length - 1 - s.options.slidesToShow).addClass("slick-center") : t === s.slideCount - 1 && i.eq(s.options.slidesToShow).addClass("slick-center")), s.$slides.eq(t).addClass("slick-center")
        } else t >= 0 && t <= s.slideCount - s.options.slidesToShow ? s.$slides.slice(t, t + s.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= s.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (o = s.slideCount % s.options.slidesToShow, n = !0 === s.options.infinite ? s.options.slidesToShow + t : t, s.options.slidesToShow == s.options.slidesToScroll && s.slideCount - t < s.options.slidesToShow ? i.slice(n - (s.options.slidesToShow - o), n + o).addClass("slick-active").attr("aria-hidden", "false") : i.slice(n, n + s.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
        "ondemand" !== s.options.lazyLoad && "anticipated" !== s.options.lazyLoad || s.lazyLoad()
    }, e.prototype.setupInfinite = function() {
        var e, i, n, o = this;
        if (!0 === o.options.fade && (o.options.centerMode = !1), !0 === o.options.infinite && !1 === o.options.fade && (i = null, o.slideCount > o.options.slidesToShow)) {
            for (n = !0 === o.options.centerMode ? o.options.slidesToShow + 1 : o.options.slidesToShow, e = o.slideCount; e > o.slideCount - n; e -= 1) i = e - 1, t(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i - o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");
            for (e = 0; e < n + o.slideCount; e += 1) i = e, t(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i + o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");
            o.$slideTrack.find(".slick-cloned").find("[id]").each((function() {
                t(this).attr("id", "")
            }))
        }
    }, e.prototype.interrupt = function(t) {
        t || this.autoPlay(), this.interrupted = t
    }, e.prototype.selectHandler = function(e) {
        var i = this,
            n = t(e.target).is(".slick-slide") ? t(e.target) : t(e.target).parents(".slick-slide"),
            o = parseInt(n.attr("data-slick-index"));
        o || (o = 0), i.slideCount <= i.options.slidesToShow ? i.slideHandler(o, !1, !0) : i.slideHandler(o)
    }, e.prototype.slideHandler = function(t, e, i) {
        var n, o, s, r, a, l = null,
            d = this;
        if (e = e || !1, !(!0 === d.animating && !0 === d.options.waitForAnimate || !0 === d.options.fade && d.currentSlide === t))
            if (!1 === e && d.asNavFor(t), n = t, l = d.getLeft(n), r = d.getLeft(d.currentSlide), d.currentLeft = null === d.swipeLeft ? r : d.swipeLeft, !1 === d.options.infinite && !1 === d.options.centerMode && (t < 0 || t > d.getDotCount() * d.options.slidesToScroll)) !1 === d.options.fade && (n = d.currentSlide, !0 !== i ? d.animateSlide(r, (function() {
                d.postSlide(n)
            })) : d.postSlide(n));
            else if (!1 === d.options.infinite && !0 === d.options.centerMode && (t < 0 || t > d.slideCount - d.options.slidesToScroll)) !1 === d.options.fade && (n = d.currentSlide, !0 !== i ? d.animateSlide(r, (function() {
            d.postSlide(n)
        })) : d.postSlide(n));
        else {
            if (d.options.autoplay && clearInterval(d.autoPlayTimer), o = n < 0 ? d.slideCount % d.options.slidesToScroll != 0 ? d.slideCount - d.slideCount % d.options.slidesToScroll : d.slideCount + n : n >= d.slideCount ? d.slideCount % d.options.slidesToScroll != 0 ? 0 : n - d.slideCount : n, d.animating = !0, d.$slider.trigger("beforeChange", [d, d.currentSlide, o]), s = d.currentSlide, d.currentSlide = o, d.setSlideClasses(d.currentSlide), d.options.asNavFor && (a = (a = d.getNavTarget()).slick("getSlick")).slideCount <= a.options.slidesToShow && a.setSlideClasses(d.currentSlide), d.updateDots(), d.updateArrows(), !0 === d.options.fade) return !0 !== i ? (d.fadeSlideOut(s), d.fadeSlide(o, (function() {
                d.postSlide(o)
            }))) : d.postSlide(o), void d.animateHeight();
            !0 !== i ? d.animateSlide(l, (function() {
                d.postSlide(o)
            })) : d.postSlide(o)
        }
    }, e.prototype.startLoad = function() {
        var t = this;
        !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.hide(), t.$nextArrow.hide()), !0 === t.options.dots && t.slideCount > t.options.slidesToShow && t.$dots.hide(), t.$slider.addClass("slick-loading")
    }, e.prototype.swipeDirection = function() {
        var t, e, i, n, o = this;
        return t = o.touchObject.startX - o.touchObject.curX, e = o.touchObject.startY - o.touchObject.curY, i = Math.atan2(e, t), (n = Math.round(180 * i / Math.PI)) < 0 && (n = 360 - Math.abs(n)), n <= 45 && n >= 0 || n <= 360 && n >= 315 ? !1 === o.options.rtl ? "left" : "right" : n >= 135 && n <= 225 ? !1 === o.options.rtl ? "right" : "left" : !0 === o.options.verticalSwiping ? n >= 35 && n <= 135 ? "down" : "up" : "vertical"
    }, e.prototype.swipeEnd = function(t) {
        var e, i, n = this;
        if (n.dragging = !1, n.swiping = !1, n.scrolling) return n.scrolling = !1, !1;
        if (n.interrupted = !1, n.shouldClick = !(n.touchObject.swipeLength > 10), void 0 === n.touchObject.curX) return !1;
        if (!0 === n.touchObject.edgeHit && n.$slider.trigger("edge", [n, n.swipeDirection()]), n.touchObject.swipeLength >= n.touchObject.minSwipe) {
            switch (i = n.swipeDirection()) {
                case "left":
                case "down":
                    e = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide + n.getSlideCount()) : n.currentSlide + n.getSlideCount(), n.currentDirection = 0;
                    break;
                case "right":
                case "up":
                    e = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide - n.getSlideCount()) : n.currentSlide - n.getSlideCount(), n.currentDirection = 1
            }
            "vertical" != i && (n.slideHandler(e), n.touchObject = {}, n.$slider.trigger("swipe", [n, i]))
        } else n.touchObject.startX !== n.touchObject.curX && (n.slideHandler(n.currentSlide), n.touchObject = {})
    }, e.prototype.swipeHandler = function(t) {
        var e = this;
        if (!(!1 === e.options.swipe || "ontouchend" in document && !1 === e.options.swipe || !1 === e.options.draggable && -1 !== t.type.indexOf("mouse"))) switch (e.touchObject.fingerCount = t.originalEvent && void 0 !== t.originalEvent.touches ? t.originalEvent.touches.length : 1, e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold, !0 === e.options.verticalSwiping && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold), t.data.action) {
            case "start":
                e.swipeStart(t);
                break;
            case "move":
                e.swipeMove(t);
                break;
            case "end":
                e.swipeEnd(t)
        }
    }, e.prototype.swipeMove = function(t) {
        var e, i, n, o, s, r, a = this;
        return s = void 0 !== t.originalEvent ? t.originalEvent.touches : null, !(!a.dragging || a.scrolling || s && 1 !== s.length) && (e = a.getLeft(a.currentSlide), a.touchObject.curX = void 0 !== s ? s[0].pageX : t.clientX, a.touchObject.curY = void 0 !== s ? s[0].pageY : t.clientY, a.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(a.touchObject.curX - a.touchObject.startX, 2))), r = Math.round(Math.sqrt(Math.pow(a.touchObject.curY - a.touchObject.startY, 2))), !a.options.verticalSwiping && !a.swiping && r > 4 ? (a.scrolling = !0, !1) : (!0 === a.options.verticalSwiping && (a.touchObject.swipeLength = r), i = a.swipeDirection(), void 0 !== t.originalEvent && a.touchObject.swipeLength > 4 && (a.swiping = !0, t.preventDefault()), o = (!1 === a.options.rtl ? 1 : -1) * (a.touchObject.curX > a.touchObject.startX ? 1 : -1), !0 === a.options.verticalSwiping && (o = a.touchObject.curY > a.touchObject.startY ? 1 : -1), n = a.touchObject.swipeLength, a.touchObject.edgeHit = !1, !1 === a.options.infinite && (0 === a.currentSlide && "right" === i || a.currentSlide >= a.getDotCount() && "left" === i) && (n = a.touchObject.swipeLength * a.options.edgeFriction, a.touchObject.edgeHit = !0), !1 === a.options.vertical ? a.swipeLeft = e + n * o : a.swipeLeft = e + n * (a.$list.height() / a.listWidth) * o, !0 === a.options.verticalSwiping && (a.swipeLeft = e + n * o), !0 !== a.options.fade && !1 !== a.options.touchMove && (!0 === a.animating ? (a.swipeLeft = null, !1) : void a.setCSS(a.swipeLeft))))
    }, e.prototype.swipeStart = function(t) {
        var e, i = this;
        if (i.interrupted = !0, 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow) return i.touchObject = {}, !1;
        void 0 !== t.originalEvent && void 0 !== t.originalEvent.touches && (e = t.originalEvent.touches[0]), i.touchObject.startX = i.touchObject.curX = void 0 !== e ? e.pageX : t.clientX, i.touchObject.startY = i.touchObject.curY = void 0 !== e ? e.pageY : t.clientY, i.dragging = !0
    }, e.prototype.unfilterSlides = e.prototype.slickUnfilter = function() {
        var t = this;
        null !== t.$slidesCache && (t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.appendTo(t.$slideTrack), t.reinit())
    }, e.prototype.unload = function() {
        var e = this;
        t(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }, e.prototype.unslick = function(t) {
        var e = this;
        e.$slider.trigger("unslick", [e, t]), e.destroy()
    }, e.prototype.updateArrows = function() {
        var t = this;
        Math.floor(t.options.slidesToShow / 2), !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && !t.options.infinite && (t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === t.currentSlide ? (t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : (t.currentSlide >= t.slideCount - t.options.slidesToShow && !1 === t.options.centerMode || t.currentSlide >= t.slideCount - 1 && !0 === t.options.centerMode) && (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    }, e.prototype.updateDots = function() {
        var t = this;
        null !== t.$dots && (t.$dots.find("li").removeClass("slick-active").end(), t.$dots.find("li").eq(Math.floor(t.currentSlide / t.options.slidesToScroll)).addClass("slick-active"))
    }, e.prototype.visibility = function() {
        var t = this;
        t.options.autoplay && (document[t.hidden] ? t.interrupted = !0 : t.interrupted = !1)
    }, t.fn.slick = function() {
        var t, i, n = this,
            o = arguments[0],
            s = Array.prototype.slice.call(arguments, 1),
            r = n.length;
        for (t = 0; t < r; t++)
            if ("object" == typeof o || void 0 === o ? n[t].slick = new e(n[t], o) : i = n[t].slick[o].apply(n[t].slick, s), void 0 !== i) return i;
        return n
    }
})),
function(t, e, i) {
    "use strict";
    t.fn.scrollUp = function(e) {
        t.data(i.body, "scrollUp") || (t.data(i.body, "scrollUp", !0), t.fn.scrollUp.init(e))
    }, t.fn.scrollUp.init = function(n) {
        var o, s, r, a, l, d, c = t.fn.scrollUp.settings = t.extend({}, t.fn.scrollUp.defaults, n),
            p = !1;
        switch (d = c.scrollTrigger ? t(c.scrollTrigger) : t("<a/>", {
            id: c.scrollName,
            href: "#top"
        }), c.scrollTitle && d.attr("title", c.scrollTitle), d.appendTo("body"), c.scrollImg || c.scrollTrigger || d.html(c.scrollText), d.css({
            display: "none",
            position: "fixed",
            zIndex: c.zIndex
        }), c.activeOverlay && t("<div/>", {
            id: c.scrollName + "-active"
        }).css({
            position: "absolute",
            top: c.scrollDistance + "px",
            width: "100%",
            borderTop: "1px dotted" + c.activeOverlay,
            zIndex: c.zIndex
        }).appendTo("body"), c.animation) {
            case "fade":
                o = "fadeIn", s = "fadeOut", r = c.animationSpeed;
                break;
            case "slide":
                o = "slideDown", s = "slideUp", r = c.animationSpeed;
                break;
            default:
                o = "show", s = "hide", r = 0
        }
        a = "top" === c.scrollFrom ? c.scrollDistance : t(i).height() - t(e).height() - c.scrollDistance, t(e).scroll((function() {
            t(e).scrollTop() > a ? p || (d[o](r), p = !0) : p && (d[s](r), p = !1)
        })), c.scrollTarget ? "number" == typeof c.scrollTarget ? l = c.scrollTarget : "string" == typeof c.scrollTarget && (l = Math.floor(t(c.scrollTarget).offset().top)) : l = 0, d.click((function(e) {
            e.preventDefault(), t("html, body").animate({
                scrollTop: l
            }, c.scrollSpeed, c.easingType)
        }))
    }, t.fn.scrollUp.defaults = {
        scrollName: "scrollUp",
        scrollDistance: 300,
        scrollFrom: "top",
        scrollSpeed: 300,
        easingType: "linear",
        animation: "fade",
        animationSpeed: 200,
        scrollTrigger: !1,
        scrollTarget: !1,
        scrollText: "Scroll to top",
        scrollTitle: !1,
        scrollImg: !1,
        activeOverlay: !1,
        zIndex: 2147483647
    }, t.fn.scrollUp.destroy = function(n) {
        t.removeData(i.body, "scrollUp"), t("#" + t.fn.scrollUp.settings.scrollName).remove(), t("#" + t.fn.scrollUp.settings.scrollName + "-active").remove(), t.fn.jquery.split(".")[1] >= 7 ? t(e).off("scroll", n) : t(e).unbind("scroll", n)
    }, t.scrollUp = t.fn.scrollUp
}(jQuery, window, document),
function(t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof module && module.exports ? module.exports = t(require("jquery")) : t(jQuery)
}((function(t) {
    var e = Array.prototype.slice,
        i = Array.prototype.splice,
        n = {
            topSpacing: 0,
            bottomSpacing: 0,
            className: "is-sticky",
            wrapperClassName: "sticky-wrapper",
            center: !1,
            getWidthFrom: "",
            widthFromWrapper: !0,
            responsiveWidth: !1,
            zIndex: "inherit"
        },
        o = t(window),
        s = t(document),
        r = [],
        a = o.height(),
        l = function() {
            for (var e = o.scrollTop(), i = s.height(), n = i - a, l = e > n ? n - e : 0, d = 0, c = r.length; d < c; d++) {
                var p = r[d],
                    u = p.stickyWrapper.offset().top - p.topSpacing - l;
                if (p.stickyWrapper.css("height", p.stickyElement.outerHeight()), e <= u) null !== p.currentTop && (p.stickyElement.css({
                    width: "",
                    position: "",
                    top: "",
                    "z-index": ""
                }), p.stickyElement.parent().removeClass(p.className), p.stickyElement.trigger("sticky-end", [p]), p.currentTop = null);
                else {
                    var h, f = i - p.stickyElement.outerHeight() - p.topSpacing - p.bottomSpacing - e - l;
                    if (f < 0 ? f += p.topSpacing : f = p.topSpacing, p.currentTop !== f) p.getWidthFrom ? (padding = p.stickyElement.innerWidth() - p.stickyElement.width(), h = t(p.getWidthFrom).width() - padding || null) : p.widthFromWrapper && (h = p.stickyWrapper.width()), null == h && (h = p.stickyElement.width()), p.stickyElement.css("width", h).css("position", "fixed").css("top", f).css("z-index", p.zIndex), p.stickyElement.parent().addClass(p.className), null === p.currentTop ? p.stickyElement.trigger("sticky-start", [p]) : p.stickyElement.trigger("sticky-update", [p]), p.currentTop === p.topSpacing && p.currentTop > f || null === p.currentTop && f < p.topSpacing ? p.stickyElement.trigger("sticky-bottom-reached", [p]) : null !== p.currentTop && f === p.topSpacing && p.currentTop < f && p.stickyElement.trigger("sticky-bottom-unreached", [p]), p.currentTop = f;
                    var g = p.stickyWrapper.parent();
                    p.stickyElement.offset().top + p.stickyElement.outerHeight() >= g.offset().top + g.outerHeight() && p.stickyElement.offset().top <= p.topSpacing ? p.stickyElement.css("position", "absolute").css("top", "").css("bottom", 0).css("z-index", "") : p.stickyElement.css("position", "fixed").css("top", f).css("bottom", "").css("z-index", p.zIndex)
                }
            }
        },
        d = function() {
            a = o.height();
            for (var e = 0, i = r.length; e < i; e++) {
                var n = r[e],
                    s = null;
                n.getWidthFrom ? n.responsiveWidth && (s = t(n.getWidthFrom).width()) : n.widthFromWrapper && (s = n.stickyWrapper.width()), null != s && n.stickyElement.css("width", s)
            }
        },
        c = {
            init: function(e) {
                return this.each((function() {
                    var i = t.extend({}, n, e),
                        o = t(this),
                        s = o.attr("id"),
                        a = s ? s + "-" + n.wrapperClassName : n.wrapperClassName,
                        l = t("<div></div>").attr("id", a).addClass(i.wrapperClassName);
                    o.wrapAll((function() {
                        if (0 == t(this).parent("#" + a).length) return l
                    }));
                    var d = o.parent();
                    i.center && d.css({
                        width: o.outerWidth(),
                        marginLeft: "auto",
                        marginRight: "auto"
                    }), "right" === o.css("float") && o.css({
                        float: "none"
                    }).parent().css({
                        float: "right"
                    }), i.stickyElement = o, i.stickyWrapper = d, i.currentTop = null, r.push(i), c.setWrapperHeight(this), c.setupChangeListeners(this)
                }))
            },
            setWrapperHeight: function(e) {
                var i = t(e),
                    n = i.parent();
                n && n.css("height", i.outerHeight())
            },
            setupChangeListeners: function(t) {
                window.MutationObserver ? new window.MutationObserver((function(e) {
                    (e[0].addedNodes.length || e[0].removedNodes.length) && c.setWrapperHeight(t)
                })).observe(t, {
                    subtree: !0,
                    childList: !0
                }) : window.addEventListener ? (t.addEventListener("DOMNodeInserted", (function() {
                    c.setWrapperHeight(t)
                }), !1), t.addEventListener("DOMNodeRemoved", (function() {
                    c.setWrapperHeight(t)
                }), !1)) : window.attachEvent && (t.attachEvent("onDOMNodeInserted", (function() {
                    c.setWrapperHeight(t)
                })), t.attachEvent("onDOMNodeRemoved", (function() {
                    c.setWrapperHeight(t)
                })))
            },
            update: l,
            unstick: function(e) {
                return this.each((function() {
                    for (var e = t(this), n = -1, o = r.length; o-- > 0;) r[o].stickyElement.get(0) === this && (i.call(r, o, 1), n = o); - 1 !== n && (e.unwrap(), e.css({
                        width: "",
                        position: "",
                        top: "",
                        float: "",
                        "z-index": ""
                    }))
                }))
            }
        };
    window.addEventListener ? (window.addEventListener("scroll", l, !1), window.addEventListener("resize", d, !1)) : window.attachEvent && (window.attachEvent("onscroll", l), window.attachEvent("onresize", d)), t.fn.sticky = function(i) {
        return c[i] ? c[i].apply(this, e.call(arguments, 1)) : "object" != typeof i && i ? void t.error("Method " + i + " does not exist on jQuery.sticky") : c.init.apply(this, arguments)
    }, t.fn.unstick = function(i) {
        return c[i] ? c[i].apply(this, e.call(arguments, 1)) : "object" != typeof i && i ? void t.error("Method " + i + " does not exist on jQuery.sticky") : c.unstick.apply(this, arguments)
    }, t((function() {
        setTimeout(l, 0)
    }))
})),
function(t) {
    t.fn.theiaStickySidebar = function(e) {
        function i(e, i) {
            return !0 === e.initialized || !(t("body").width() < e.minWidth) && (function(e, i) {
                e.initialized = !0, 0 === t("#theia-sticky-sidebar-stylesheet-" + e.namespace).length && t("head").append(t('<style id="theia-sticky-sidebar-stylesheet-' + e.namespace + '">.theiaStickySidebar:after {content: ""; display: table; clear: both;}</style>'));
                i.each((function() {
                    var i = {};
                    if (i.sidebar = t(this), i.options = e || {}, i.container = t(i.options.containerSelector), 0 == i.container.length && (i.container = i.sidebar.parent()), i.sidebar.parents().css("-webkit-transform", "none"), i.sidebar.css({
                            position: i.options.defaultPosition,
                            overflow: "visible",
                            "-webkit-box-sizing": "border-box",
                            "-moz-box-sizing": "border-box",
                            "box-sizing": "border-box"
                        }), i.stickySidebar = i.sidebar.find(".theiaStickySidebar"), 0 == i.stickySidebar.length) {
                        var o = /(?:text|application)\/(?:x-)?(?:javascript|ecmascript)/i;
                        i.sidebar.find("script").filter((function(t, e) {
                            return 0 === e.type.length || e.type.match(o)
                        })).remove(), i.stickySidebar = t("<div>").addClass("theiaStickySidebar").append(i.sidebar.children()), i.sidebar.append(i.stickySidebar)
                    }
                    i.marginBottom = parseInt(i.sidebar.css("margin-bottom")), i.paddingTop = parseInt(i.sidebar.css("padding-top")), i.paddingBottom = parseInt(i.sidebar.css("padding-bottom"));
                    var s = i.stickySidebar.offset().top,
                        r = i.stickySidebar.outerHeight();

                    function a() {
                        i.fixedScrollTop = 0, i.sidebar.css({
                            "min-height": "1px"
                        }), i.stickySidebar.css({
                            position: "static",
                            width: "",
                            transform: "none"
                        })
                    }

                    function l(e) {
                        var i = e.height();
                        return e.children().each((function() {
                            i = Math.max(i, t(this).height())
                        })), i
                    }
                    i.stickySidebar.css("padding-top", 1), i.stickySidebar.css("padding-bottom", 1), s -= i.stickySidebar.offset().top, r = i.stickySidebar.outerHeight() - r - s, 0 == s ? (i.stickySidebar.css("padding-top", 0), i.stickySidebarPaddingTop = 0) : i.stickySidebarPaddingTop = 1, 0 == r ? (i.stickySidebar.css("padding-bottom", 0), i.stickySidebarPaddingBottom = 0) : i.stickySidebarPaddingBottom = 1, i.previousScrollTop = null, i.fixedScrollTop = 0, a(), i.onScroll = function(i) {
                        if (i.stickySidebar.is(":visible"))
                            if (t("body").width() < i.options.minWidth) a();
                            else {
                                if (i.options.disableOnResponsiveLayouts)
                                    if (i.sidebar.outerWidth("none" == i.sidebar.css("float")) + 50 > i.container.width()) return void a();
                                var o = t(document).scrollTop(),
                                    s = "static";
                                if (o >= i.sidebar.offset().top + (i.paddingTop - i.options.additionalMarginTop)) {
                                    var r, d = i.paddingTop + e.additionalMarginTop,
                                        c = i.paddingBottom + i.marginBottom + e.additionalMarginBottom,
                                        p = i.sidebar.offset().top,
                                        u = i.sidebar.offset().top + l(i.container),
                                        h = 0 + e.additionalMarginTop;
                                    r = i.stickySidebar.outerHeight() + d + c < t(window).height() ? h + i.stickySidebar.outerHeight() : t(window).height() - i.marginBottom - i.paddingBottom - e.additionalMarginBottom;
                                    var f = p - o + i.paddingTop,
                                        g = u - o - i.paddingBottom - i.marginBottom,
                                        m = i.stickySidebar.offset().top - o,
                                        v = i.previousScrollTop - o;
                                    "fixed" == i.stickySidebar.css("position") && "modern" == i.options.sidebarBehavior && (m += v), "stick-to-top" == i.options.sidebarBehavior && (m = e.additionalMarginTop), "stick-to-bottom" == i.options.sidebarBehavior && (m = r - i.stickySidebar.outerHeight()), m = v > 0 ? Math.min(m, h) : Math.max(m, r - i.stickySidebar.outerHeight()), m = Math.max(m, f), m = Math.min(m, g - i.stickySidebar.outerHeight());
                                    var y = i.container.height() == i.stickySidebar.outerHeight();
                                    s = (y || m != h) && (y || m != r - i.stickySidebar.outerHeight()) ? o + m - i.sidebar.offset().top - i.paddingTop <= e.additionalMarginTop ? "static" : "absolute" : "fixed"
                                }
                                if ("fixed" == s) {
                                    var _ = t(document).scrollLeft();
                                    i.stickySidebar.css({
                                        position: "fixed",
                                        width: n(i.stickySidebar) + "px",
                                        transform: "translateY(" + m + "px)",
                                        left: i.sidebar.offset().left + parseInt(i.sidebar.css("padding-left")) - _ + "px",
                                        top: "0px"
                                    })
                                } else if ("absolute" == s) {
                                    var b = {};
                                    "absolute" != i.stickySidebar.css("position") && (b.position = "absolute", b.transform = "translateY(" + (o + m - i.sidebar.offset().top - i.stickySidebarPaddingTop - i.stickySidebarPaddingBottom) + "px)", b.top = "0px"), b.width = n(i.stickySidebar) + "px", b.left = "", i.stickySidebar.css(b)
                                } else "static" == s && a();
                                "static" != s && 1 == i.options.updateSidebarHeight && i.sidebar.css({
                                    "min-height": i.stickySidebar.outerHeight() + i.stickySidebar.offset().top - i.sidebar.offset().top + i.paddingBottom
                                }), i.previousScrollTop = o
                            }
                    }, i.onScroll(i), t(document).on("scroll." + i.options.namespace, function(t) {
                        return function() {
                            t.onScroll(t)
                        }
                    }(i)), t(window).on("resize." + i.options.namespace, function(t) {
                        return function() {
                            t.stickySidebar.css({
                                position: "static"
                            }), t.onScroll(t)
                        }
                    }(i)), "undefined" != typeof ResizeSensor && new ResizeSensor(i.stickySidebar[0], function(t) {
                        return function() {
                            t.onScroll(t)
                        }
                    }(i))
                }))
            }(e, i), !0)
        }

        function n(t) {
            var e;
            try {
                e = t[0].getBoundingClientRect().width
            } catch (t) {}
            return void 0 === e && (e = t.width()), e
        }
        return (e = t.extend({
                containerSelector: "",
                additionalMarginTop: 100,
                additionalMarginBottom: 0,
                updateSidebarHeight: !0,
                minWidth: 0,
                disableOnResponsiveLayouts: !0,
                sidebarBehavior: "modern",
                defaultPosition: "relative",
                namespace: "TSS"
            }, e)).additionalMarginTop = parseInt(e.additionalMarginTop) || 0, e.additionalMarginBottom = parseInt(e.additionalMarginBottom) || 0,
            function(e, n) {
                i(e, n) || (console.log("TSS: Body width smaller than options.minWidth. Init is delayed."), t(document).on("scroll." + e.namespace, function(e, n) {
                    return function(o) {
                        i(e, n) && t(this).unbind(o)
                    }
                }(e, n)), t(window).on("resize." + e.namespace, function(e, n) {
                    return function(o) {
                        i(e, n) && t(this).unbind(o)
                    }
                }(e, n)))
            }(e, this), this
    }
}(jQuery);