<?php require_once('views/web/layouts/index.php') ?>

<?php startblock('content') ?>
<div class="banner header-text">
    <div class="owl-banner owl-carousel owl-loaded owl-drag">

        <div class="owl-stage-outer">
            <div class="owl-stage" style="transform: translate3d(-2380px, 0px, 0px); transition: all 0s ease 0s; width: 8330px;">
                <div class="owl-item cloned" style="width: 1160px; margin-right: 30px;">
                    <div class="banner-item-02">
                        <div class="text-content">
                            <h4>Flash Deals</h4>
                            <h2>Get your best products</h2>
                        </div>
                    </div>
                </div>
                <div class="owl-item cloned" style="width: 1160px; margin-right: 30px;">
                    <div class="banner-item-03">
                        <div class="text-content">
                            <h4>Last Minute</h4>
                            <h2>Grab last minute deals</h2>
                        </div>
                    </div>
                </div>
                <div class="owl-item active" style="width: 1160px; margin-right: 30px;">
                    <div class="banner-item-01">
                        <div class="text-content">
                            <h4>Best Offer</h4>
                            <h2>New Arrivals On Sale</h2>
                        </div>
                    </div>
                </div>
                <div class="owl-item" style="width: 1160px; margin-right: 30px;">
                    <div class="banner-item-02">
                        <div class="text-content">
                            <h4>Flash Deals</h4>
                            <h2>Get your best products</h2>
                        </div>
                    </div>
                </div>
                <div class="owl-item" style="width: 1160px; margin-right: 30px;">
                    <div class="banner-item-03">
                        <div class="text-content">
                            <h4>Last Minute</h4>
                            <h2>Grab last minute deals</h2>
                        </div>
                    </div>
                </div>
                <div class="owl-item cloned" style="width: 1160px; margin-right: 30px;">
                    <div class="banner-item-01">
                        <div class="text-content">
                            <h4>Best Offer</h4>
                            <h2>New Arrivals On Sale</h2>
                        </div>
                    </div>
                </div>
                <div class="owl-item cloned" style="width: 1160px; margin-right: 30px;">
                    <div class="banner-item-02">
                        <div class="text-content">
                            <h4>Flash Deals</h4>
                            <h2>Get your best products</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button></div>
        <div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot"><span></span></button></div>
    </div>
</div>

<div class="latest-products">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Đăng kí</h2>
                </div>
            </div>
            <div class="col-md-12">
                <form class="form" action="<?php echo url('auth/handleRegister') ?>" method="POST">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control <?php echo !empty($errors['email']) ? 'is-invalid' : '' ?>" />
                        <?php if (!empty($errors['email'])) { ?>
                            <div class="invalid-feedback"><?php echo $errors['email'] ?></div>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Mật khẩu</label>
                        <input type="password" name="password" class="form-control <?php echo !empty($errors['password']) ? 'is-invalid' : '' ?>" />
                        <?php if (!empty($errors['password'])) { ?>
                            <div class="invalid-feedback"><?php echo $errors['password'] ?></div>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Xác nhận mật khẩu</label>
                        <input type="password" name="password_confirmation" class="form-control <?php echo !empty($errors['password_confirmation']) ? 'is-invalid' : '' ?>" />
                        <?php if (!empty($errors['password_confirmation'])) { ?>
                            <div class="invalid-feedback"><?php echo $errors['password_confirmation'] ?></div>
                        <?php } ?>
                    </div>

                    <button type="submit" class="btn btn-primary">Đăng kí</button>
                </form>

                <p>Đã có tài khoản, ấn vào <a href="<?php echo url('auth/login') ?>">đây</a> để đăng nhập</p>

            </div>
        </div>
    </div>
</div>

<?php endblock() ?>