<?php require_once('core/Flash.php') ?>
<?php 
    if (Flash::has('loginErrors')) {
        $loginErrors = Flash::get('loginErrors');
    }

    if (Flash::has('registerErrors')) {
        $registerErrors = Flash::get('registerErrors');
    }
    
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Webykhoa | <?php defineblock('title') ?></title>
    <link rel="icon" href="<?= asset('assets/web') ?>/images/favicon.ico" />
    <link href="<?= asset('assets/web') ?>/css/app.css" rel="stylesheet">
    <link href="<?= asset('assets/web') ?>/css/web/vendor.css" rel="stylesheet">
    <link href="<?= asset('assets/web') ?>/css/web/main.css" rel="stylesheet">
    <link href="<?= asset('assets/web') ?>/css/web/category.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <div class="main-wrap">
        <?php include('views/web/layouts/includes/header.php') ?>
        <?php defineblock('content') ?>
        <?php include('views/web/layouts/includes/footer.php') ?>
        
        <!-- Login Modal -->
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title login-title" id="loginModalLabel">Đăng nhập để tiếp tục</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <form action="<?= url('auth/handleLogin') ?>" method="POST" style="width:100%; margin:auto">
                                    <?php if (isset($loginErrors['another'])): ?>
                                        <div class="alert alert-dismissible alert-danger js-login-error">
                                        <?= $loginErrors['another'] ?>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endif ?>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Địa chỉ email">
                                        <span class="text-danger"><?= isset($loginErrors['email']) ? $loginErrors['email'] : '' ?></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Mật khẩu">
                                        <span class="text-danger"><?= isset($loginErrors['password']) ? $loginErrors['password'] : '' ?></span>
                                    </div>
                                    <div class="form-check pb-20">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember">
                                        <label class="form-check-label" for="remember">
                                            Ghi nhớ đăng nhập
                                        </label>
                                    </div>
                                    <div class="form-group row login-btn-div">
                                        <div class="col-6">
                                            <button type="submit" id="ajax-login" class="btn btn-primary login-button">
                                                Đăng nhập
                                            </button>
                                        </div>
                                        <!-- <div class="col-6">
                                            <button type="button" id="ajax-forgot-password" class="btn btn-primary forgot-password">
                                                Quên mật khẩu
                                            </button>
                                        </div> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- register Modal -->
        <div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title login-title" id="loginModalLabel">Đăng ký tài khoản</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <form action="<?= url('auth/handleRegister') ?>" method="POST" style="width:100%; margin:auto">
                                    <?php if (isset($registerErrors['another'])): ?>
                                        <div class="alert alert-dismissible alert-danger js-login-error">
                                        <?= $registerErrors['another'] ?>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endif ?>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Họ và tên" value="<?= isset($registerErrors['old_data']['name']) ? $registerErrors['old_data']['name'] : '' ?>">
                                        <span class="text-danger"><?= isset($registerErrors['name']) ? $registerErrors['name'] : '' ?></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Địa chỉ email" value="<?= isset($registerErrors['old_data']['email']) ? $registerErrors['old_data']['email'] : '' ?>">
                                        <span class="text-danger"><?= isset($registerErrors['email']) ? $registerErrors['email'] : '' ?></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Mật khẩu">
                                        <span class="text-danger"><?= isset($registerErrors['password']) ? $registerErrors['password'] : '' ?></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Nhập lại mật khẩu">
                                        <span class="text-danger"><?= isset($registerErrors['password_confirmation']) ? $registerErrors['password_confirmation'] : '' ?></span>
                                    </div>
                                    <div class="form-group row login-btn-div">
                                        <div class="col-6">
                                            <button type="submit" id="ajax-login" class="btn btn-primary login-button">
                                                Đăng ký
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dark-mark"></div>
    <script src="<?= asset('assets/web') ?>/js/app.js"></script>
    <script src="<?= asset('assets/web') ?>/js/web/vendor.js"></script>
    <script src="<?= asset('assets/web') ?>/js/web/main.js"></script>
    <script>
        $('.js-search-form').submit(function(e) {
            if ($(this).children().val() == '') {
                e.preventDefault()
            }
        });
        /* Show login modal */
        $('body').on('click', '.js-login-btn, .js-login-modal', function(e) {
            $('#login-modal').modal('show');
        });

        $('body').on('click', '.js-register-btn, .js-register-modal', function(e) {
            $('#register-modal').modal('show');
        });

        <?php if (isset($loginErrors)):?>
            $('#login-modal').modal('show');
        <?php endif ?>
        <?php if (isset($registerErrors)):?>
            $('#register-modal').modal('show');
        <?php endif ?>
    </script>
</body>

</html>