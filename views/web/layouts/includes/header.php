 <?php require_once('core/Auth.php') ?>
 <!-- Main Header -->
 <header class="main-header header-style-2 mb-30">
            <div class="header-bottom header-sticky background-white text-center sticky-bar">
                <div class="scroll-progress gradient-bg-1"></div>
                <div class="mobile_menu d-lg-none d-block"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-12">
                            <div class="header-logo d-none d-lg-block">
                                <a href="https://webykhoa.vn">
                            <img class="logo-img d-inline" src="<?= asset('assets/web') ?>/images/health_news_logo.svg" alt="Webykhoa.vn - Tin tức sức khỏe - Cập nhật hàng ngày" height="47px">
                        </a>
                            </div>
                            <div class="logo-tablet d-md-inline d-lg-none d-none" style="z-index: 10;">
                                <a href="https://webykhoa.vn">
                            <img class="logo-img d-inline" src="<?= asset('assets/web') ?>/images/health_news_logo.svg" alt="Webykhoa.vn - Tin tức sức khỏe - Cập nhật hàng ngày" height="47px">
                        </a>
                            </div>
                            <div class="logo-mobile d-block d-md-none" style="z-index: 10; margin-left: 30%">
                                <a href="https://webykhoa.vn">
                            <img class="logo-img d-inline" src="<?= asset('assets/web') ?>/images/health_news_logo.svg" alt="Webykhoa.vn - Tin tức sức khỏe - Cập nhật hàng ngày" height="47px">
                        </a>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 main-header-navigation">
                            <!-- Main-menu -->
                            <div class="main-nav text-left float-lg-left float-md-right">
                                <!-- Slick nav for mobile -->
                                <ul class="mobi-menu d-none menu-3-columns" id="navigation">
                                    <li>
                                        Sống khỏe
                                        <ul>
                                            <li><a href="https://webykhoa.vn/danh-muc/dinh-duong">
                                                Dinh dưỡng </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/the-duc-the-thao">
                                                Thể dục thể thao </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/lam-dep">
                                                Làm đẹp </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/suc-khoe-tinh-than">
                                                Sức khỏe tinh thần </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/suc-khoe-gioi-tinh">
                                                Sức khỏe giới tính </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/cham-soc-rang-mieng">
                                                Chăm sóc răng miệng </a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        Bệnh lý
                                        <ul>
                                            <li><a href="https://webykhoa.vn/danh-muc/chi-so-sinh-hoc">
                                                Chỉ số sinh học </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/co-xuong-khop">
                                                Cơ xương khớp </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/da-lieu">
                                                Da liễu </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/di-ung">
                                                Dị ứng </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/ho-hap">
                                                Hô hấp </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/huyet-hoc">
                                                Huyết học </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/nhi-khoa">
                                                Nhi khoa </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/noi-tiet">
                                                Nội tiết </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/rang-ham-mat">
                                                Răng hàm mặt </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/san-phu-khoa">
                                                Sản phụ khoa </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/tai-mui-hong">
                                                Tai mũi họng </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/than-tiet-nieu">
                                                Thận Tiết niệu </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/tieu-hoa">
                                                Tiêu hóa </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/tim-mach">
                                                Tim mạch </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/truyen-nhiem">
                                                Truyền nhiễm </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/ung-thu">
                                                Ung thư </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/tam-than-kskp6">
                                                Tâm thần </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/than-kinh-rmnx1">
                                                Thần kinh </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/di-truyen">
                                                Di truyền </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/nhan-khoa">
                                                Nhãn khoa </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/ngo-doc">
                                                Ngộ độc </a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        Thuốc và thực phẩm chức năng
                                        <ul>
                                            <li><a href="https://webykhoa.vn/danh-muc/thuc-pham-chuc-nang">
                                                Thực phẩm chức năng </a></li>
                                            <li><a href="https://webykhoa.vn/danh-muc/thuoc">
                                                Thuốc </a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        Mang thai và nuôi con
                                        <ul>
                                            <li> Mang thai
                                                <ul>
                                                    <li><a href="https://webykhoa.vn/danh-muc/chuan-bi-mang-thai">Chuẩn bị mang thai</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/3-thang-dau">3 tháng đầu</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/3-thang-giua">3 tháng giữa</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/3-thang-cuoi">3 tháng cuối</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/bien-chung-khi-mang-thai">Biến chứng khi mang thai</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/chuyen-da-va-sinh-de">Chuyển dạ và sinh đẻ</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/cham-soc-ba-me-sau-sinh">Chăm sóc bà mẹ sau sinh</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li> Nuôi dạy con cái
                                                <ul>
                                                    <li><a href="https://webykhoa.vn/danh-muc/dinh-duong-cho-tre">Dinh dưỡng cho trẻ</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/su-phat-trien-cua-tre">Sự phát triển của trẻ</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/giac-ngu-cua-be">Giấc ngủ của bé</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/hanh-vi-va-ky-luat">Hành vi và kỷ luật</a>
                                                    </li>
                                                    <li><a href="https://webykhoa.vn/danh-muc/do-dung-can-thiet-cho-tre">Đồ dùng cần thiết cho trẻ</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="https://webykhoa.vn/hoi-dap">
                                    Hỏi đáp
                                </a>
                                    </li>
                                </ul>
                                <!-- End slick nav  -->
                                <nav class="ml-4 pc-menu">
                                    <ul class="main-menu d-none d-lg-inline">
                                        <li class="menu-item-has-children mega-menu-item">
                                            <a href="https://webykhoa.vn/danh-muc/song-khoe">
                                            Sống khỏe
                                        </a>

                                            <div class="sub-mega-menu sub-menu-list row width-one-column">

                                                <ul class="col">
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/dinh-duong">
                                                        <strong>Dinh dưỡng</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/the-duc-the-thao">
                                                        <strong>Thể dục thể thao</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/lam-dep">
                                                        <strong>Làm đẹp</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/suc-khoe-tinh-than">
                                                        <strong>Sức khỏe tinh thần</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/suc-khoe-gioi-tinh">
                                                        <strong>Sức khỏe giới tính</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/cham-soc-rang-mieng">
                                                        <strong>Chăm sóc răng miệng</strong>
                                                    </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu-item-has-children mega-menu-item">
                                            <a href="https://webykhoa.vn/danh-muc/benh-ly">
                                            Bệnh lý
                                        </a>

                                            <div class="sub-mega-menu sub-menu-list row width-three-column">

                                                <ul class="col">
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/chi-so-sinh-hoc">
                                                        <strong>Chỉ số sinh học</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/co-xuong-khop">
                                                        <strong>Cơ xương khớp</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/da-lieu">
                                                        <strong>Da liễu</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/di-ung">
                                                        <strong>Dị ứng</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/ho-hap">
                                                        <strong>Hô hấp</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/huyet-hoc">
                                                        <strong>Huyết học</strong>
                                                    </a>
                                                    </li>
                                                </ul>
                                                <ul class="col">
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/nhi-khoa">
                                                        <strong>Nhi khoa</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/noi-tiet">
                                                        <strong>Nội tiết</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/rang-ham-mat">
                                                        <strong>Răng hàm mặt</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/san-phu-khoa">
                                                        <strong>Sản phụ khoa</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/tai-mui-hong">
                                                        <strong>Tai mũi họng</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/than-tiet-nieu">
                                                        <strong>Thận Tiết niệu</strong>
                                                    </a>
                                                    </li>
                                                </ul>
                                                <ul class="col">
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/tieu-hoa">
                                                        <strong>Tiêu hóa</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/tim-mach">
                                                        <strong>Tim mạch</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/truyen-nhiem">
                                                        <strong>Truyền nhiễm</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/ung-thu">
                                                        <strong>Ung thư</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/tam-than-kskp6">
                                                        <strong>Tâm thần</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/than-kinh-rmnx1">
                                                        <strong>Thần kinh</strong>
                                                    </a>
                                                    </li>
                                                </ul>
                                                <ul class="col">
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/di-truyen">
                                                        <strong>Di truyền</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/nhan-khoa">
                                                        <strong>Nhãn khoa</strong>
                                                    </a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/ngo-doc">
                                                        <strong>Ngộ độc</strong>
                                                    </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="#">
                                            Thuốc và thực phẩm chức năng
                                        </a>

                                            <ul class="sub-menu">
                                                <li>
                                                    <a href="https://webykhoa.vn/danh-muc/thuc-pham-chuc-nang">
                                                    <strong> Thực phẩm chức năng </strong>
                                                </a>
                                                </li>
                                                <li>
                                                    <a href="https://webykhoa.vn/danh-muc/thuoc">
                                                    <strong> Thuốc </strong>
                                                </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children mega-menu-item">
                                            <a href="https://webykhoa.vn/danh-muc/mang-thai-va-nuoi-con">
                                            Mang thai và nuôi con
                                        </a>

                                            <div class="sub-mega-menu sub-menu-list row">



                                                <ul class="col">
                                                    <li><strong>Mang thai </strong></li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/chuan-bi-mang-thai">Chuẩn bị mang thai</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/3-thang-dau">3 tháng đầu</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/3-thang-giua">3 tháng giữa</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/3-thang-cuoi">3 tháng cuối</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/bien-chung-khi-mang-thai">Biến chứng khi mang thai</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/chuyen-da-va-sinh-de">Chuyển dạ và sinh đẻ</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/cham-soc-ba-me-sau-sinh">Chăm sóc bà mẹ sau sinh</a>
                                                    </li>
                                                </ul>


                                                <ul class="col">
                                                    <li><strong>Nuôi dạy con cái </strong></li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/dinh-duong-cho-tre">Dinh dưỡng cho trẻ</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/su-phat-trien-cua-tre">Sự phát triển của trẻ</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/giac-ngu-cua-be">Giấc ngủ của bé</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/hanh-vi-va-ky-luat">Hành vi và kỷ luật</a>
                                                    </li>
                                                    <li>
                                                        <a href="https://webykhoa.vn/danh-muc/do-dung-can-thiet-cho-tre">Đồ dùng cần thiết cho trẻ</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li>
                                            <a href="https://webykhoa.vn/hoi-dap">
                                        Hỏi đáp
                                    </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="main-nav text-left float-right">
                                <nav>
                                    <ul class="main-menu d-none d-lg-inline">
                                        <!-- chưa đăng nhập -->
                                        <?php if (!Auth::loggedIn('user')): ?>
                                                <li class="">
                                                    <a href="#" class="js-login-btn">
                                                        Đăng nhập
                                                    </a>
                                                </li>
                                                <li class="">
                                                    <a href="#" class="js-register-btn">
                                                        Đăng ký
                                                    </a>
                                                </li>
                                        <?php else: ?>
                                        <!-- sau khi đăng nhập -->
                                        <li class="menu-item-has-children">
                                            <div class="user-avatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100"><circle cx="50" cy="50" r="49.5" stroke="#3F51B5" stroke-width="1" fill="#3F51B5" /><text font-size="48" fill="#FFFFFF" x="50%" y="50%" dy=".1em" style="line-height:1" alignment-baseline="middle" text-anchor="middle" dominant-baseline="central"><?= substr(Auth::getUser('admin')['name'], 0, 1)[0] ?></text></svg>
                                            </div>
                                            <ul class="sub-menu user-div">
                                                <li>
                                                    <a href="#">
                                                    <i class="fa fa-user pr-2" aria-hidden="true"></i>
                                                    Trang cá nhân
                                                </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                    <i class="fa fa-bookmark pr-2" aria-hidden="true"></i>
                                                    Xem bài viết đã ghim
                                                </a>
                                                </li>
                                                <li>
                                                    <a href="<?= url('auth/logout') ?>">
                                                        <i class="fa fa-sign-out pr-2" aria-hidden="true"></i>
                                                        Đăng xuất
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <?php endif ?>
                                        <!-- Search -->
                                        <form action="https://webykhoa.vn/search" method="get" class="search-form d-xl-inline float-right position-relative d-none js-search-form">
                                            <input type="text" class="search_field form-control" placeholder="Tìm kiếm" value="" name="q">
                                            <span class="search-icon"><i class="ti-search ml-5"></i></span>
                                            <span class="search-close-icon d-xl-none d-inline"><i class="ti-close mr-5"></i></span>
                                        </form>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>