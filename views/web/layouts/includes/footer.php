<footer>
            <div class="footer-area pt-50 bg-white">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-4 mb-15">
                            <a href="/">
                        <img class="logo-img d-inline" src="<?= asset('assets/web') ?>/images/health_news_logo.svg" alt="" width="200">
                    </a>
                            <p class="mt-15">
                                Health News mong muốn trở thành nền tảng thông tin y khoa hàng đầu tại Việt Nam, giúp bạn đưa ra những quyết định đúng đắn liên quan về chăm sóc sức khỏe và hỗ trợ bạn cải thiện chất lượng cuộc sống.
                            </p>
                            <p class="mt-15">
                                Kết nối với chúng tôi:
                            </p>
                            <div class="social-icons mb-10">
                                <a class="mr-10" data-event-category="Footer" data-event-action="Follow us - Facebook" data-content-label="" href="https://www.facebook.com/webykhoa.vn"><svg width="32" height="32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M27 0H5a5 5 0 00-5 5v22a5 5 0 005 5h22a5 5 0 005-5V5a5 5 0 00-5-5z" fill="#1778F2"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M20.314 32V19.499h3.255L24 15.19h-3.686l.006-2.156c0-1.123.1-1.725 1.623-1.725h2.034V7h-3.255c-3.91 0-5.285 2.09-5.285 5.604v2.587H13v4.308h2.437V32h4.877z" fill="#fff"></path></svg></a>
                                <!-- <a data-event-category="Footer" data-event-action="Follow us - Youtube" data-event-label="" href=""><svg width="32" height="32" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="32" height="32" rx="6" fill="#D42428"></rect><path fill-rule="evenodd" clip-rule="evenodd" d="M7 12a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2H9a2 2 0 01-2-2V12zm7.2.444V18.9l5.14-3.228-5.14-3.228z" fill="#fff"></path></svg></a> -->
                            </div>
                        </div>
                        <div class="col-md-2 mb-15">
                            <p class="text-muted font-weight-bold text-uppercase">Bài viết</p>
                            <ul class="float-left mr-30 font-medium">
                                <li class="cat-item cat-item-2">
                                    <a href="https://webykhoa.vn/bai-viet-moi-nhat">Bài viết mới nhất</a>
                                </li>
                                <li class="cat-item cat-item-3">
                                    <a href="https://webykhoa.vn/chu-de-noi-bat">Tất cả chuyên mục</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 mb-15">
                            <p class="text-muted font-weight-bold text-uppercase">Chính sách</p>
                            <ul class="float-left mr-30 font-medium">
                                <li class="cat-item cat-item-2">
                                    <a href="https://webykhoa.vn/dieu-khoan-su-dung">Điều khoản sử dụng</a>
                                </li>
                                <li class="cat-item cat-item-3">
                                    <a href="https://webykhoa.vn/chinh-sach-quyen-rieng-tu">Chính sách Quyền riêng tư</a>
                                </li>
                                <li class="cat-item cat-item-4">
                                    <a href="https://webykhoa.vn/bien-tap">Chính sách Biên tập</a>
                                </li>
                                <li class="cat-item cat-item-5">
                                    <a href="https://webykhoa.vn/lien-he-quang-cao">Chính sách Quảng cáo và Tài trợ</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 mb-15">
                            <p class="text-muted font-weight-bold text-uppercase">Liên kết</p>
                            <ul class="float-left mr-30 font-medium">
                                <li class="cat-item cat-item-2"><a href="https://webykhoa.vn/gioi-thieu">Tự giới thiệu</a></li>
                                <li class="cat-item cat-item-3"><a href="https://webykhoa.vn/cau-hoi-thuong-gap">Câu hỏi thường gặp</a></li>
                                <li class="cat-item cat-item-4"><a href="https://webykhoa.vn/lien-he">Liên hệ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer-bottom aera -->
            <div class="footer-bottom-area bg-white text-muted">
                <div class="container">
                    <div class="footer-border pt-20 pb-20">
                        <div class="row d-flex align-items-center justify-content-between">
                            <div class="col-12">
                                <div class="footer-copy-right">
                                    <p class="font-small text-muted">© 2021, NewsViral | All rights reserved | Design by <a href="https://vietjack.com/" target="_blank" rel="noreferrer">VietJack</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End-->
        </footer>