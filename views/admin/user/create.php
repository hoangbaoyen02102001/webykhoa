<?php require_once('views/admin/layouts/index.php') ?>

<?php startblock('title') ?>
    Thêm người dùng
<?php endblock() ?>


<?php startblock('header')?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Người dùng</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Bảng điều khiển</a></li>
                    <li class="breadcrumb-item active">Thêm người dùng</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php endblock()?>


<?php startblock('content') ?>
<div class="content">
                <div class="container-fluid">
                                        

<!-- <form method="POST" action="https://webykhoa.vn/admin/users" accept-charset="UTF-8" class="form-user js-form-submit" data-redirect="https://webykhoa.vn/admin/users" autocomplete="off"><input name="_token" type="hidden" value="hrL3MG7SbspMv54FR89Cfn1TZxYsrXphsNROQOu1"> -->
<div class="row">
    <!-- left column -->
    <!-- <div class="col-md-3">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Menu</h3>
            </div>
            <div class="card-body">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-generation-tab" data-toggle="pill" href="#v-pills-generation" role="tab" aria-controls="v-pills-generation" aria-selected="true">Thông tin chung</a>
                    <a class="nav-link" id="v-pills-add-information-tab" data-toggle="pill" href="#v-pills-add-information" role="tab" aria-controls="v-pills-profile" aria-selected="false">Thông tin khác</a>
                    <a class="nav-link" id="v-pills-change-password-tab" data-toggle="pill" href="#v-pills-change-password" role="tab" aria-controls="v-pills-messages" aria-selected="false">Thay đổi mật khẩu</a>
                </div>
            </div>
        </div>
    </div> -->
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-12 tab-content">
        <div class="tab-pane fade show active" id="v-pills-generation" role="tabpanel" aria-labelledby="v-pills-generation-tab">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Thêm mới người dùng</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="admin/user/handleCreate" method="post">
                        <div class="form-group required ">
                            <label for="name">Tên</label>
                            <input class="form-control" required="" autofocus="" name="name" type="text" id="name" name="name">
                            <span class="help-block"></span>
                        </div>
    
                        <div class="form-group required ">
                            <label for="email">Địa chỉ email</label>
                            <input class="form-control" required="" name="email" type="text" id="email" name="email">
                            <span class="help-block"></span>
                            <p class="text-danger"><?= isset($errors['email']) ? $errors['email'] : '' ?></p>
                        </div>
                        <div class="form-group required ">
                            <label for="password">Mật khẩu</label>
                            <input class="form-control" required="" name="password" type="password" value="" id="password" name="password">
                            <span class="help-block"></span>
                        </div>
    
                        <div class="form-group required ">
                            <label for="password_confirmation">Xác nhân mật khẩu</label>
                            <input class="form-control" required="" name="password_confirmation" type="password" value="" id="password_confirmation" name="password_confirmation">
                            <span class="help-block"></span>
                            <p class="text-danger"><?= isset($errors['password_confirmation']) ? $errors['password_confirmation'] : '' ?></p>
                        </div>
    <!--                     
                        <div class="form-group required ">
                            <label for="role">Quyền</label>
                            <select id="role" class="form-control select2-hidden-accessible" required="" name="role" data-select2-id="role" tabindex="-1" aria-hidden="true"><option value="1" data-select2-id="2">Admin</option><option value="2">Partner</option><option value="3">Inspector</option><option value="4">Nomal user</option></select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="1" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-role-container"><span class="select2-selection__rendered" id="select2-role-container" role="textbox" aria-readonly="true" title="Admin">Admin</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="avatar">Ảnh đại diện</label>
                            <div class="js-uploadOuter-avatar">
                                <div class="d-flex">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default js-uploadOuter-btn-avatar" type="button">
                                            <i class="far fa-image"></i>
                                            <input name="image" type="hidden">
                                        </button>
                                    </span>
                                    <span class="dragBox">
                                        Upload file here
                                        <input name="avatar" type="file" id="uploadFile-avatar">
                                    </span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default js-clear-uploadOuter-btn-avatar" type="button">
                                            <i class="fa fa-times text-red"></i>
                                        </button>
                                    </span>
                                </div>
                                <span class="help-block form-control-feedback js-help-block"></span>
                                <div id="uploadPreview-avatar" class="image-preview">
                                    <img class="js-uploadPreview-avatar" src="">
                                </div>
                                <input name="old_avatar" value="" type="hidden" id="old_avatar">
                            </div>
    
                            <span class="help-block"></span>
                        </div> -->
    
                        <!-- <div class="form-check ">
                            <input name="status" type="checkbox" value="1">
                            <label for="status">Người dùng hoạt động</label>
                            <span class="help-block"></span>
                        </div> -->
                        <button type="submit" class="btn btn-primary">Thêm người dùng</button>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        <div class="tab-pane fade" id="v-pills-add-information" role="tabpanel" aria-labelledby="v-pills-add-information-tab">
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title">Thông tin khác</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="dob">Sinh nhật</label>
                                <div class="input-group date">
                                    <input class="form-control" autofocus="" id="birthday" name="dob" type="text">
                                    <div class="input-group-append" data-target="#birthday" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="gender">Giới tính</label>
                                <select id="gender" class="form-control" name="gender"><option value="1">Undefined</option><option value="2">Male</option><option value="3">Female</option></select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="phone_number">Số điện thoại</label>
                                <input class="form-control" autofocus="" placeholder="Định dạng 10 số: 0123456789" name="phone_number" type="text" id="phone_number">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="address">Địa chỉ</label>
                                <textarea class="form-control" autofocus="" row="3" name="address" cols="50" rows="10" id="address"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class="tab-pane fade" id="v-pills-change-password" role="tabpanel" aria-labelledby="v-pills-change-password-tab">
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title">Thay đổi mật khẩu</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Mật khẩu cũ</label>
                                <input type="text" class="form-control" placeholder="Enter ...">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Mật khẩu mới</label>
                                <input type="text" class="form-control" placeholder="Enter ...">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Xác nhận mật khẩu mới</label>
                                <input type="text" class="form-control" placeholder="Enter ...">
                            </div>
                        </div>
                        <button class="btn btn-primary">Thay đổi mật khẩu</button>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (right) -->
</div>
<!-- </form> -->
                </div>
            </div>
<?php endblock() ?>