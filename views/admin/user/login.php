<html lang="en">
<head>
    <meta charset="utf-8">
    <base href="<?php echo BASE_PATH ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminLTE 3 | Log in</title>
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="public/assets/admin/libs/admin-lte/dist/css/adminlte.min.css?v=3.2.0">
    <style type="text/css">
        @font-face {
            font-family: Roboto;
            src: url("chrome-extension://mcgbeeipkmelnpldkobichboakdfaeon/css/Roboto-Regular.ttf");
        }
    </style>
</head>
<body class="login-page" style="min-height: 496.797px;">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>Web</b>Ykhoa</a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Đăng nhập</p>
                <?php if (Flash::has('error')): ?>
                    <p class="text-danger">
                        <?= Flash::get('error') ?>
                    </p>
                <?php endif ?>
                <form action="admin/auth/handleLogin" method="post">
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" name="email" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger"><?= isset($errors['email']) ? $errors['email'] : '' ?></p>
                    <div class="input-group mb-3">
                            <input type="password" class="form-control" name="password" placeholder="Mật khẩu">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                    </div>
                    <p class="text-danger"><?= isset($errors['password']) ? $errors['password'] : '' ?></p>
                    <div class="d-flex justify-content-between align-items-center">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember" name="remember">
                                <label for="remember">
                                    Ghi nhớ
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary">Đăng nhập</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</body>

</html>