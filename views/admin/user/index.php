<?php require_once('views/admin/layouts/index.php') ?>

<?php startblock('title') ?>
    Quản lý người dùng
<?php endblock() ?>

<?php startblock('header')?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Người dùng</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Bảng điều khiển</a></li>
                    <li class="breadcrumb-item active">Tất cả người dùng</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php endblock()?>

<?php startblock('content') ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="admin/user/create" class="btn btn-primary btn-sm align-self-center">
                            <i class="fas fa-plus-circle"></i> Thêm người dùng mới
                        </a>
                        <div class="mt-3 ml-3">
                            <form method="GET" action="https://webykhoa.vn/admin/users" accept-charset="UTF-8" class="js-filter-table" data-table="#table-users">
                                <input name="sortBy" type="hidden">
                                <input name="sortDir" type="hidden">
                                <div class="row">
                                    <div class="col-md-2 pl-0 pr-1">
                                        <div class="form-group">
                                            <select id="role" class="form-control" name="role"><option value="" selected="selected">Tất cả quyền</option><option value="1">Admin</option><option value="2">Partner</option><option value="3">Inspector</option><option value="4">Nomal user</option></select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0 pr-1">
                                        <div class="form-group">
                                            <select id="status" class="form-control" name="status"><option value="" selected="selected">Tất cả trạng thái</option><option value="1">Đang hoạt động</option><option value="0">Đang tạm khóa</option></select>
                                        </div>
                                    </div>
                                    <div class="col-md-5 pl-0 pr-1">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Tìm kiếm" name="keyword" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-3 pl-0 pr-1">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info"><i class="fas fa-search"></i> Tìm kiếm</button>
                                            <a href="admin/user" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Xóa
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover" id="table-users">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên</th>
                                    <th>Địa chỉ email</th>
                                    <th>Quyền</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($users as $user): ?>
                                <tr>
                                    <td><?= $user['id'] ?></td>
                                    <td>
                                        <a href="admin/user/update?id=<?= $user['id'] ?>"><?= $user['name'] ?></a>
                                    </td>
                                    <td><?= $user['email'] ?></td>
                                    <td><?= $user['role'] ?></td>
                                    <td>
                                        <span class="d-flex">
                                            <a href="admin/user/update?id=<?= $user['id'] ?>" class="btn btn-primary btn-sm mr-3">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <?php if (Auth::getUser('admin')['id'] != $user['id']) : ?>
                                            <a href="admin/user/delete?id=<?= $user['id'] ?>" onclick="return confirm('Bạn có muốn xoá người dùng này không ?')" class="btn btn-danger btn-sm mr-3">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                            <?php endif ?>
                                        </span>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        <ul class="pagination pagination-sm m-0 float-right">

                            <li class="page-item disabled" aria-disabled="true" aria-label="&laquo; Trang sau">
                                <a class="page-link" href="#">«</a>
                            </li>
                            <li class="page-item active" aria-current="page">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="https://webykhoa.vn/admin/users?page=2">2</a>
                            </li>


                            <li class="page-item">
                                <a class="page-link" href="https://webykhoa.vn/admin/users?page=2" rel="next" aria-label="Trang trước &raquo;">»</a>
                            </li>
                        </ul>

                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</div>
<?php endblock() ?>