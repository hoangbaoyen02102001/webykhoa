<?php require_once 'views/admin/layouts/index.php'?>



<?php startblock('title')?>
Cập nhật bài viết
<?php endblock()?>

<?php startblock('header')?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Bài viết</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Bảng điều khiển</a></li>
                    <li class="breadcrumb-item active">Cập nhật bài viết</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php endblock()?>

<?php startblock('content')?>
<div class="content">
    <div class="container-fluid">
        <form method="POST" action="admin/blog/handleUpdate" accept-charset="UTF-8"
            class="form-post" autocomplete="off"
            enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= $blog['id'] ?>">
            <div class="row">
                <div class="col-md-8 transition-content" id="generation-section">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                Phần chung
                            </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse"
                                    data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-link" id="seo-setting">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body pad row">
                            <div class="form-group  col-sm-12">
                                <label for="title">Tiêu đề</label>
                                <input class="form-control" placeholder="Enter title"
                                    name="title" type="text" value="<?= isset($updateBlogErrors) ? $updateBlogErrors['old_data']['title'] : $blog['title'] ?>">
                                <span class="text-danger"><?= isset($updateBlogErrors['title']) ? $updateBlogErrors['title'] : '' ?></span>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="category_id">Danh mục</label>
                                <select class="form-control" name="category_id" id="category_id"
                                    tabindex="-1" aria-hidden="true">
                                    <option value="">Chọn</option>
                                    <?php foreach($categories as $category): ?>
                                        <option value="<?= $category['id'] ?>" <?= (isset($updateBlogErrors) && $updateBlogErrors['old_data']['category_id'] == $category['id']) ? 'selected' : ($blog['category_id'] == $category['id'] ? 'selected' : '') ?> ><?= $category['title'] ?></option>
                                    <?php endforeach ?>
                                </select>
                                <span class="text-danger"><?= isset($updateBlogErrors['category_id']) ? $updateBlogErrors['category_id'] : '' ?></span>
                            </div>
                            <div class="form-group  col-sm-12">
                                <label for="description">Mô tả</label>
                                <textarea class="form-control" placeholder="Place some text here" rows="5" name="description" cols="50"><?= isset($updateBlogErrors) ? $updateBlogErrors['old_data']['description'] : $blog['description'] ?></textarea>
                                <span class="text-danger"><?= isset($updateBlogErrors['description']) ? $updateBlogErrors['description'] : '' ?></span>
                            </div>
                            <div class="form-group col-sm-12 content-div">
                                <label for="content">Nội dung</label>
                                <textarea name="content" id="content" cols="30" rows="10"><?= isset($updateBlogErrors) ? $updateBlogErrors['old_data']['content'] : $blog['content'] ?></textarea>
                                <span class="text-danger"><?= isset($updateBlogErrors['content']) ? $updateBlogErrors['content'] : '' ?></span>
                            </div>
                            <div class="form-group">
                                <div class="form-check ml-2">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck2" name="active" <?= isset($updateBlogErrors['old_data']['active']) ? 'checked' : ($blog['active'] == 1 ? 'checked' : '') ?>>
                                    <label class="form-check-label" for="exampleCheck2">Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="float-left" id="action-form">
                                <input class="btn btn-primary" type="submit" value="Cập nhật">
                            </div>
                            <div class="float-right">
                                <a href="admin/blog" class="btn btn-link">Quay lại</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 transition-content" id="seo-div">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                Thêm thông tin
                            </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse"
                                    data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body pad">
                            <div class="form-row">
                                <div class="form-group col-md-12 ">
                                    <label for="banner">Ảnh bìa</label>
                                    <div class="js-uploadOuter-banner">
                                        <div class="d-flex">
                                            <span class="dragBox">
                                                Upload file here
                                                <input name="thumbnail" type="file" id="uploadFile-banner" accept="image/jpg, image/png, image/jpeg">
                                            </span>
                                        </div>
                                        <span class="text-danger"><?= isset($updateBlogErrors['thumbnail']) ? $updateBlogErrors['thumbnail'] : '' ?></span>
                                        <div class="image-preview">
                                            <img id="img-preview" class="js-uploadPreview-banner" style="width:100%"
                                                src="public/storage/blogs/<?= $blog['thumbnail'] ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php endblock()?>

<?php startblock('script') ?>
<script>
    tinymce.init({
        selector: '#content',
        plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
        toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
    });
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img-preview').attr('src', e.target.result).show();
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
    $('input[type=file]').change(function () {
        readURL(this);
    });

</script>
<?php endblock() ?>