<?php require_once 'views/admin/layouts/index.php'?>



<?php startblock('title')?>
Quản lý bài viết
<?php endblock()?>

<?php startblock('header')?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Bài viết</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Bảng điều khiển</a></li>
                    <li class="breadcrumb-item active">Tất cả bài viết</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php endblock()?>

<?php startblock('content')?>
<div class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-12">
        <!-- <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Lọc bài viết</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <form method="GET" action="https://webykhoa.vn/admin/posts" id="filter-post-form">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Bạn nhập liệu</label>
                                <select class="form-control" name="author_id"><option value="" selected="selected">Tất cả</option><option value="1">Viet Jack</option><option value="2">Ngọc Anh</option><option value="3">Nguyễn Văn Minh</option><option value="4">Minh Nguyễn Văn</option><option value="5">Bình Trọng</option><option value="6">Tuan Dung Nguyen</option><option value="7">Pham Ken</option><option value="10">Nguyễn Văn Minh</option><option value="11">Nezuko Kamado</option><option value="12">TRUC ANH</option><option value="16">Hà Ngọc Cát Tường</option><option value="17">Dung</option><option value="18">thanh mai</option><option value="19">Thuỳ Dung Thân</option><option value="20">Dung Nguyen</option><option value="23">Khánh Hạ</option><option value="24">Quỳnh Anh</option><option value="25">Sponge Bobbie</option><option value="26">Kiều Trinh</option><option value="27">Mai Chi</option><option value="28">Giap Tuyen</option><option value="29">Trọng Bình.</option><option value="30">Khánh Linh</option><option value="31">Tham my vien Nevada</option><option value="33">dinh khuyen</option><option value="34">Cướpsạchnhàbăng Sanbằngthếgiới Đàobớitìnhyêu Kiếmtìmhạnhphúc</option><option value="35">Linh Trang</option><option value="36">Loan Đinh</option><option value="37">Mr. Joe</option><option value="38">Diep Vu Van</option><option value="39">Thu Thuy</option><option value="40">Duyên Trần Thị Kim</option><option value="41">HUY</option><option value="42">Tường Vy</option><option value="43">Mai Ha</option><option value="44">Loan Nguyen</option><option value="45">Bà bầu Sắt</option><option value="46">Đức Hoà</option><option value="48">Hue Kim</option><option value="49">Anh</option></select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Thể loại</label>
                                <select class="form-control" name="category_id" id="category_id" data-select2-id="category_id" tabindex="-1" aria-hidden="true">
                                    <option value="" data-select2-id="2"> Tất cả </option>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select class="form-control" name="status"><option value="" selected="selected">Tất cả</option><option value="active">Active</option><option value="draft">Drart</option><option value="cancel">Cancel</option></select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" name="keyword" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Ngày nhập</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control float-right" name="datefilter" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Bài viết nổi bật (Hot)</label>
                                <select class="form-control" name="is_hot"><option value="" selected="selected">Tất cả</option><option value="1">Bài viết nổi bật</option><option value="0">Bài viết không nổi bật</option></select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-primary">Tìm kiếm</button>
                                <button class="btn btn-danger" id="reset-filter" type="button">Reset</button>
                            </div>
                        </div>
                    </div>
                    <input name="sortBy" type="hidden">
                    <input name="sortDir" type="hidden">
                </form>
            </div>
        </div> -->

        <div class="card">
            <div class="card-header">
                <a href="admin/blog/create" class="btn btn-primary btn-sm align-self-center">
                    <i class="fas fa-plus-circle"></i> Thêm bài viết mới
                </a>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <!-- <h3 style="padding-top: 20px; padding-left: 20px;"> 9124 Bài viết </h3>    -->
                <table class="table table-hover" id="table-posts">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th width="100">Ảnh bìa</th>
                            <th width="350">Tiêu đề</th>
                            <th width="150">Danh mục</th>
                            <th>Mô tả</th>
                            <th>Trạng thái</th>
                            <th>Thời gian tạo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($blogs as $blog): ?>
                        <tr>
                            <td><?= $blog['id'] ?></td>
                            <td>
                                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#blog-<?= $blog['id'] ?>">
                                    <i class="far fa-image"></i>
                                </button>
                                <div class="modal" id="blog-<?= $blog['id'] ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Thumbnail</h4>
                                                <button type="button" class="close" data-dismiss="modal">
                                                    ×
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="public/storage/blogs/<?= $blog['thumbnail'] ?>" class="img-thumbnail">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="blog?id=<?= $blog['id'] ?>" target="_blank"><?= $blog['title'] ?></a>
                            </td>
                            <td>
                                <?= $blog['category_title'] ?>
                            </td>
                            <td>
                                <?= $blog['description'] ?>
                            </td>
                            <td>
                                <label class="switch">
                                    <input class="js-switch-btn" name="status" type="checkbox" <?= $blog['active'] == 1 ? 'checked' : '' ?> data-active="<?= $blog['active'] ?>" data-id="<?= $blog['id'] ?>" onclick="changeStatus(this)">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>
                                <?= $blog['created_at'] ?>
                            </td>
                            <td>
                                <span class="d-flex">
                                    <a href="admin/blog/update?id=<?= $blog['id'] ?>" class="btn btn-primary btn-sm mr-3">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="admin/blog/delete?id=<?= $blog['id'] ?>" onclick="return confirm('Bạn có muốn xoá bài viết này không ?')" >
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </span>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item disabled" aria-disabled="true" aria-label="« Trang sau">
                        <a class="page-link" href="#">«</a>
                    </li>
                    <a class="page-link" href="#">1</a>
                        </li>
                                                                                <li class="page-item">
                            <a class="page-link" href="#">2</a>
                        </li>
                                                                                <li class="page-item">
                            <a class="page-link" href="#">3</a>
                        </li>
                                                                                <li class="page-item">
                            <a class="page-link" href="#">4</a>
                        </li>
                                                                                <li class="page-item">
                            <a class="page-link" href="#">5</a>
                        </li>
                                                                                <li class="page-item">
                            <a class="page-link" href="#">6</a>
                        </li>
                                                                                <li class="page-item">
                            <a class="page-link" href="#">7</a>
                        </li>
                                                                                <li class="page-item">
                            <a class="page-link" href="#">8</a>
                        </li>
                                                                    
                            <li class="page-item disabled" aria-disabled="true">
                    <a class="page-link" href="#">...</a>
                    </li>
                                                                            <li class="page-item">
                                <a class="page-link" href="#64">364</a>
                            </li>
                                                                                    <li class="page-item">
                                <a class="page-link" href="#65">365</a>
                            </li>
                        <li class="page-item">
                    <a class="page-link" href="#" rel="next" aria-label="Trang trước »">»</a>
                </li>
            </ul>
            </div>
        </div>
        <!-- /.card -->
            </div>
        </div>
    </div>
</div>
<?php endblock()?>

<?php startblock('script') ?>
<script>
    function changeStatus(e) {
        console.log(e)
        let id = e.getAttribute('data-id')
        let active = 1 -e.getAttribute('data-active')
        window.location.href = `<?= url('admin/blog/status') ?>?id=${id}&active=${active}`
    }
</script>
<?php endblock() ?>