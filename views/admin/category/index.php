<?php require_once 'views/admin/layouts/index.php'?>

<?php startblock('title')?>
Quản lý danh mục
<?php endblock()?>

<?php startblock('header')?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Danh mục</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Bảng điều khiển</a></li>
                    <li class="breadcrumb-item active">Tất cả danh mục</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php endblock()?>

<?php startblock('content')?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="text-right">
                            <button type="button" class="btn btn-primary btn-sm align-self-center" data-toggle="modal"
                                data-target="#create-form-category" data-title="Thêm chủ đề"
                                data-form-action="https://webykhoa.vn/admin/categories" data-input-parent_id="0"
                                data-form-method="POST">
                                <i class="fas fa-plus-circle"></i> Thêm chủ đề
                            </button>
                        </div>
                        <div class="mt-3 ml-3">
                            <form method="GET" accept-charset="UTF-8" class="js-filter-table">
                                <div class="row">
                                    <div class="col-md-6 pl-0 pr-1">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Tìm kiếm" name="keyword"
                                                type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-0 pr-1">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info"><i class="fas fa-search"></i>
                                            Tìm kiếm
                                            </button>
                                            <a href="<?= url('admin/category') ?>" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Xóa
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover" id="table-users">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên danh mục</th>
                                    <th>Tên danh mục cha</th>
                                    <th>Mô tả</th>
                                    <th>Trạng thái</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($categories as $category): ?>
                                <tr>
                                    <td><?=$category['id']?></td>
                                    <td>
                                        <a
                                            href="admin/category/update?id=<?=$category['id']?>"><?=$category['title']?></a>
                                    </td>
                                    <td><?=$category['menu_title']?></td>
                                    <td><?=$category['description']?></td>
                                    <td>
                                        <label class="switch">
                                            <input class="js-switch-btn" name="status" type="checkbox" <?= $category['active'] == 1 ? 'checked' : '' ?> data-active="<?= $category['active'] ?>" data-id="<?= $category['id'] ?>" onclick="changeStatus(this)">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <span class="d-flex">
                                            <a href="javascript:void(0)"
                                                onclick="openUpdateModal(this)"
                                                data-id="<?= $category['id'] ?>"
                                                data-title="<?= $category['title'] ?>"
                                                data-menu-id="<?= $category['menu_id'] ?>"
                                                data-description="<?= $category['description'] ?>"
                                                data-active="<?= $category['active'] ?>"
                                                class="btn btn-primary btn-sm mr-3">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="admin/category/delete?id=<?=$category['id']?>"
                                                onclick="return confirm('Bạn có muốn xoá danh mục này không ?')"
                                                class="btn btn-danger btn-sm mr-3">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                                <?php endforeach?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        <ul class="pagination pagination-sm m-0 float-right">
                            <li class="page-item disabled" aria-disabled="true" aria-label="&laquo; Trang sau">
                                <a class="page-link" href="#">«</a>
                            </li>
                            <li class="page-item active" aria-current="page">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="https://webykhoa.vn/admin/users?page=2">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="https://webykhoa.vn/admin/users?page=2" rel="next"
                                    aria-label="Trang trước &raquo;">»</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="create-form-category" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg">
        <form method="POST" action="<?= url('admin/category/handleCreate') ?>" >
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm danh mục</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="form-group col-md-12">
                        <label>Tiêu đề</label>
                        <input name="title" type="text" class="form-control" placeholder="Enter ..." id="category-title" value="<?= isset($createCategoryErrors) ? $createCategoryErrors['old_data']['title'] : '' ?>">
                        <span class="text-danger"><?= isset($createCategoryErrors['title']) ? $createCategoryErrors['title'] : '' ?></span>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mô tả</label>
                        <textarea name="description" class="form-control" rows="3" placeholder="Enter ..."><?= isset($createCategoryErrors) ? $createCategoryErrors['old_data']['description'] : '' ?></textarea>
                        <span class="text-danger"><?= isset($createCategoryErrors['description']) ? $createCategoryErrors['description'] : '' ?></span>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Danh mục cha</label>
                        <select class="form-control" name="menu_id">
                            <option value="">Chọn</option>
                            <?php foreach($menus as $menu): ?>
                                <option value="<?= $menu['id'] ?>" <?= isset($createCategoryErrors) && $createCategoryErrors['old_data']['menu_id'] == $menu['id'] ? 'selected' : '' ?> ><?= $menu['title'] ?></option>
                            <?php endforeach ?>
                        </select>
                        <span class="text-danger"><?= isset($createCategoryErrors['menu_id']) ? $createCategoryErrors['menu_id'] : '' ?></span>
                    </div>
                    <div class="form-group">
                        <div class="form-check ml-2">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="active" <?= isset($createCategoryErrors['old_data']['active']) ? 'checked' : '' ?>>
                            <label class="form-check-label" for="exampleCheck1">Active</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-spinner fa-pulse js-loading-icon d-none"></i>
                        Lưu
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="update-form-category" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg">
        <form method="POST" action="<?= url('admin/category/handleUpdate') ?>" >
            <input type="hidden" name="id" value="<?= isset($updateCategoryErrors) ? $updateCategoryErrors['old_data']['id'] : '' ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật danh mục</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body row">
                    <div class="form-group col-md-12">
                        <label>Tiêu đề</label>
                        <input name="title" type="text" class="form-control" placeholder="Enter ..." id="category-title" value="<?= isset($updateCategoryErrors) ? $updateCategoryErrors['old_data']['title'] : '' ?>">
                        <span class="text-danger"><?= isset($updateCategoryErrors['title']) ? $updateCategoryErrors['title'] : '' ?></span>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mô tả</label>
                        <textarea name="description" class="form-control" rows="3" placeholder="Enter ..."><?= isset($updateCategoryErrors) ? $updateCategoryErrors['old_data']['description'] : '' ?></textarea>
                        <span class="text-danger"><?= isset($updateCategoryErrors['description']) ? $updateCategoryErrors['description'] : '' ?></span>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Danh mục cha</label>
                        <select class="form-control" name="menu_id">
                            <option value="">Chọn</option>
                            <?php foreach($menus as $menu): ?>
                                <option value="<?= $menu['id'] ?>" <?= isset($updateCategoryErrors) && $updateCategoryErrors['old_data']['menu_id'] == $menu['id'] ? 'selected' : '' ?> ><?= $menu['title'] ?></option>
                            <?php endforeach ?>
                        </select>
                        <span class="text-danger"><?= isset($updateCategoryErrors['menu_id']) ? $updateCategoryErrors['menu_id'] : '' ?></span>
                    </div>
                    <div class="form-group">
                        <div class="form-check ml-2">
                            <input type="checkbox" class="form-check-input" id="exampleCheck2" name="active" <?= isset($updateCategoryErrors['old_data']['active']) ? 'checked' : '' ?>>
                            <label class="form-check-label" for="exampleCheck2">Active</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-spinner fa-pulse js-loading-icon d-none"></i>
                        Lưu
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function changeStatus(e) {
        console.log(e)
        let id = e.getAttribute('data-id')
        let active = 1 -e.getAttribute('data-active')
        window.location.href = `<?= url('admin/category/status') ?>?id=${id}&active=${active}`
    }
    function openUpdateModal(data)
    {   
        let id = data.getAttribute('data-id')
        let title = data.getAttribute('data-title')
        let menu_id = data.getAttribute('data-menu-id')
        let description = data.getAttribute('data-description')
        let active = data.getAttribute('data-active')

        let formUpdate = document.querySelector('#update-form-category')
        formUpdate.querySelector('input[name="id"]').value = id
        formUpdate.querySelector('input[name="title"]').value = title
        formUpdate.querySelector('textarea[name="description"]').innerHTML = description
        formUpdate.querySelectorAll('#update-form-category select[name="menu_id"] option').forEach(e => {
            if (e.value == menu_id) {
                e.setAttribute('selected', '')
            }
        })
        if (active == 1) {
            formUpdate.querySelector('input[name="active"]').setAttribute('checked', '')
        }
        $('#update-form-category').modal('show');

    }
</script>
<?php endblock()?>