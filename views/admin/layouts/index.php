<?php require_once('core/Flash.php') ?>
<?php 
    if (Flash::has('createCategoryErrors')) {
        $createCategoryErrors = Flash::get('createCategoryErrors');
    }
    if (Flash::has('updateCategoryErrors')) {
        $updateCategoryErrors = Flash::get('updateCategoryErrors');
    }

    if (Flash::has('createBlogErrors')) {
        $createBlogErrors = Flash::get('createBlogErrors');
    }
    if (Flash::has('updateBlogErrors')) {
        $updateBlogErrors = Flash::get('updateBlogErrors');
    }
    
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <base href="<?php echo BASE_PATH ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="public/assets/admin/images/favicon.ico" />
    <title><?php defineblock('title') ?></title>
    <!-- Styles -->
    <link href="public/assets/admin/css/app.css" rel="stylesheet">
    <link href="public/assets/admin/libs/admin-lte/dist/css/adminlte.min.css" rel="stylesheet">
    <!-- Code Mirror - highlight code block. -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
    <link href="public/assets/admin/assets/css/editor.css" rel="stylesheet" />
    <link href="public/assets/admin/assets/css/froala_editor.pkgd.css" rel="stylesheet" />
    <link href="public/assets/admin/css/admin/custom_editor.css" rel="stylesheet">
    <link href="public/assets/admin/css/admin/admin.css" rel="stylesheet" />
    <!-- <link href="public/assets/admin/css/fontawesome.css" rel="stylesheet" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdn.tiny.cloud/1/jpauit33owsfr7h0x7ye1so78ahiho5v9l5mbnrumsog4xhn/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    
</head>
<body class="hold-transition sidebar-mini">
    <div id="app" class="wrapper">
        <?php include('views/admin/layouts/includes/navbar.php') ?>
        <?php include('views/admin/layouts/includes/aside.php') ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <?php defineblock('header') ?>
            <!-- Main content -->
            <?php defineblock('content') ?>
        </div>
        <?php include('views/admin/layouts/includes/footer.php') ?>
    </div>
    <!-- Scripts -->
    <script src="public/assets/admin/js/app.js"></script>
    <script src="public/assets/admin/libs/admin-lte/dist/js/adminlte.min.js"></script>
    <script>
        window.config = {
            image_upload_url: 'public/assets/admin/images',
        };
    </script>
    <!-- Code Mirror - highlight code block. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="public/assets/admin/assets/js/editor.pkgd.js"></script>
    <script src="public/assets/admin/assets/js/editor.js"></script>
    <script src="public/assets/admin/js/admin/admin.js"></script>
    <script src="public/assets/admin//messages.js"></script>
    <script src="public/assets/admin/js/admin/users.js"></script>
    <script>
        document.querySelectorAll('.nav-link').forEach(e => {
            
            if (window.location.pathname.split("/").splice(2).join("/") == e.getAttribute("href")) {
                console.log('ahihi')
                e.classList.add('active')
                if (e.parentElement.parentElement.nodeName == 'UL') {
                    e.parentElement.parentElement.parentElement.classList.add('menu-open')
                    e.parentElement.parentElement.parentElement.firstElementChild.classList.add('active')
                }
            }
        })
    </script>
    <script>
        <?php if (isset($createCategoryErrors)):?>
            $('#create-form-category').modal('show');
        <?php endif ?>

        <?php if (isset($updateCategoryErrors)):?>
            $('#update-form-category').modal('show');
        <?php endif ?>
    </script>
    <?php defineblock('script') ?>
</body>
</html>