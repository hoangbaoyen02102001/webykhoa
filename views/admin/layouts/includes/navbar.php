<nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link user-panel" data-toggle="dropdown" href="#">
                        <div class="img-circle elevation-2 avatar-user">
                            <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100"><circle cx="50" cy="50" r="49.5" stroke="#3F51B5" stroke-width="1" fill="#3F51B5" /><text font-size="48" fill="#FFFFFF" x="50%" y="50%" dy=".1em" style="line-height:1" alignment-baseline="middle" text-anchor="middle" dominant-baseline="central"><?= substr(Auth::getUser('admin')['name'], 0, 1)[0] ?></text></svg>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                        <a href="javascript:void(0)" class="dropdown-item">
                            <i class="fas fa-info-circle mr-2"></i>
                            <?= Auth::getUser('admin')['name'] ?>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="admin/auth/logout" class="dropdown-item">
                            <i class="fas fa-sign-out-alt mr-2"></i> Đăng xuất
                        </a>
                    </div>
                </li>
            </ul>
        </nav>