<aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="https://webykhoa.vn/admin/dashboard" class="brand-link">
                <img src="public/assets/admin/images/logo.png" alt="Tin tức sức khỏe" class="brand-image" style="opacity: .8">
                <span class="brand-text font-weight-light">Tin tức sức khỏe</span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="" class="nav-link" target="_blank">
                                <i class="nav-icon fas fa-external-link-alt"></i>
                                <p>
                                    Xem trang web
                                </p>
                            </a>
                        </li>
                        <li class="nav-header">Chính</li>
                        <li class="nav-item">
                            <a href="admin/category" class="nav-link ">
                                <i class="far fa-list-alt"></i>
                                <p>
                                    Quản lý danh mục
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:void(0)" class="nav-link ">
                        <i class="fa fa-free-code-camp"></i>
                        <p>
                            Danh mục nổi bật
                        </p>
                    </a>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="javascript:void(0)" class="nav-link ">
                                <i class="far fa-edit"></i>
                                <p>
                                    Quản lý bài viết
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="admin/blog" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tất cả bài viết</p>
                            </a>
                                </li>
                                <li class="nav-item">
                                    <a href="admin/blog/create" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Thêm mới bài viết</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:void(0)" class="nav-link ">
                                <i class="fas fa-address-book"></i>
                                <p>
                                    Quản lý mục lục
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="javascript:void(0)" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Quản lý người dùng
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="admin/user" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Tất cả người dùng</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="admin/user/create" class="nav-link ">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Thêm người dùng mới</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="admin/question" class="nav-link ">
                                <i class="nav-icon fas fa-question"></i>
                                <p>
                                    Quản lý hỏi đáp
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>